import { User } from "./user.model";

export class SalarySlip {
  public id: number;
  public serial_no: string;
  public employee_id: string;
  public bonus: number;
  public leave_days: number;
  public amount: number;
  public paid_date: string;
  public status: string;
  public details: SalarySlipDetails;
  public employee: User;

  constructor(data: any) {
    this.id = data.id ? data.id : null,
    this.serial_no = data.serial_no ? data.serial_no : null,
    this.employee_id = data.employee_id ? data.employee_id : null,
    this.bonus = data.bonus ? +data.bonus : null,
    this.leave_days = data.leave_days != null ? data.leave_days : null,
    this.paid_date = data.paid_date ? data.paid_date : null,
    this.status = data.status ? data.status : null,
    this.amount = data.amount != null ? +data.amount : null,
    this.details = data.details ? new SalarySlipDetails(data.details) : new SalarySlipDetails({}),
    this.employee = data.employee ? new User(data.employee) : new User({})
  }
}

export class SalarySlipDetails {
  public gpf: string;
  public hra: boolean;
  public base_salary: boolean;
  public transport_allowance: boolean;

  constructor(data: any) {
    this.gpf = data.gpf ? data.gpf : null,
    this.hra = data.hra ? data.hra : null,
    this.base_salary = data.base_salary ? data.base_salary : null,
    this.transport_allowance = data.transport_allowance ? data.transport_allowance : null
  }
}