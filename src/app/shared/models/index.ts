export * from './user.model';
export * from './device.model';
export * from './vehicle.model';
export * from './certificate.model';
export * from './inventory.model';
export * from './purchase-order.model';
export * from './receive-note.model';
export * from './requisition-order.model';
export * from './salary-slip.model';
export * from './transaction.model';
export * from './cost.model';
export * from './feedback.model';

export class PageData {
  public total: number;
  public per_page: number;
  constructor(data: any) {
    this.total = data.total != null ? +data.total : null;
    this.per_page = data.per_page != null ? +data.per_page : null;
  }
}

export class IcatData {
  public url: string;
  public data: string;

  constructor(data: any) {
    this.url = data.url ? data.url : null;
    this.data = data.data ? data.data : null;
  }
}