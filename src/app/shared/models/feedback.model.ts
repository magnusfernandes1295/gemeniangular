import { User } from './user.model';

export class Feedback {
  public id: number;
  public category: string;
  public subject: string;
  public body: string;
  public status: string;
  public creator: User;
  constructor(data: any) {
    this.id = data.id ? data.id : null;
    this.category = data.category ? data.category : null;
    this.subject = data.subject ? data.subject : null;
    this.body = data.body ? data.body : null;
    this.status = data.status ? data.status : null;
    this.creator = data.creator ? new User(data.creator) : new User({});
  }
}