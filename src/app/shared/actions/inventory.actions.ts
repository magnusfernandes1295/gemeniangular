import { Action } from '@ngrx/store';
import swal from 'sweetalert2';

export const CREATE_NEW_INVENTORY = '[Inventory] Create New Inventory';
export const CREATE_NEW_INVENTORY_COMPLETE = '[Inventory] Create New Inventory Complete';
export const CREATE_NEW_INVENTORY_FAILED = '[Inventory] Create New Inventory Failed';

export const UPDATE_INVENTORY = '[Inventory] Update Inventory';
export const UPDATE_INVENTORY_COMPLETE = '[Inventory] Update Inventory Complete';
export const UPDATE_INVENTORY_FAILED = '[Inventory] Update Inventory Failed';

export const DELETE_INVENTORY = '[Inventory] Delete Inventory';
export const DELETE_INVENTORY_COMPLETE = '[Inventory] Delete Inventory Complete';
export const DELETE_INVENTORY_FAILED = '[Inventory] Delete Inventory Failed';

export const FETCH_ALL_INVENTORY = '[Inventory] Fetch All Inventory';
export const FETCH_ALL_INVENTORY_COMPLETE = '[Inventory] Fetch All Inventory Complete';
export const FETCH_ALL_INVENTORY_FAILED = '[Inventory] Fetch All Inventory Failed';

export const FETCH_INVENTORY = '[Inventory] Fetch Inventory';
export const FETCH_INVENTORY_COMPLETE = '[Inventory] Fetch Inventory Complete';
export const FETCH_INVENTORY_FAILED = '[Inventory] Fetch Inventory Failed';

export const FILTER_INVENTORY = '[Inventory] Filter Inventory';
export const FILTER_INVENTORY_COMPLETE = '[Inventory] Filter Inventory Complete';
export const FILTER_INVENTORY_FAILED = '[Inventory] Filter Inventory Failed';

export const FETCH_INVENTORY_HISTORY = '[Inventory] Fetch Inventory History';
export const FETCH_INVENTORY_HISTORY_COMPLETE = '[Inventory] Fetch Inventory History Complete';
export const FETCH_INVENTORY_HISTORY_FAILED = '[Inventory] Fetch Inventory History Failed';

export const OPEN_INVENTORY_MODAL_ACTION = '[Inventory] Open Inventory Modal Action';
export const CLOSE_INVENTORY_MODAL_ACTION = '[Inventory] Close Inventory Modal Action';

export const CLEAR_INVENTORY_STATE_ACTION = "[Inventory] Clear Inventory State Action";

export class CreateInventoryAction implements Action {
  readonly type = CREATE_NEW_INVENTORY;
  constructor(public payload: any) { }
}

export class CreateNewInventoryCompleteAction implements Action {
  readonly type = CREATE_NEW_INVENTORY_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Inventory Added!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class CreateNewInventoryFailedAction implements Action {
  readonly type = CREATE_NEW_INVENTORY_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", payload, "error");
  }
}

export class UpdateInventoryAction implements Action {
  readonly type = UPDATE_INVENTORY;
  constructor(public payload: any) { }
}

export class UpdateInventoryCompleteAction implements Action {
  readonly type = UPDATE_INVENTORY_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Inventory Updated!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class UpdateInventoryFailedAction implements Action {
  readonly type = UPDATE_INVENTORY_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", payload, "error");
  }
}

export class DeleteInventoryAction implements Action {
  readonly type = DELETE_INVENTORY;
  constructor(public payload: any) { }
}

export class DeleteInventoryCompleteAction implements Action {
  readonly type = DELETE_INVENTORY_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Inventory Deleted!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class DeleteInventoryFailedAction implements Action {
  readonly type = DELETE_INVENTORY_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", payload, "error");
  }
}

export class FetchAllInventoryAction implements Action {
  readonly type = FETCH_ALL_INVENTORY;
  constructor(public payload?: any) { }
}

export class FetchAllInventoryCompleteAction implements Action {
  readonly type = FETCH_ALL_INVENTORY_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchAllInventoryFailedAction implements Action {
  readonly type = FETCH_ALL_INVENTORY_FAILED;
  constructor(public payload?: any) { }
}

export class FetchInventoryAction implements Action {
  readonly type = FETCH_INVENTORY;
  constructor(public payload?: any) { }
}

export class FetchInventoryCompleteAction implements Action {
  readonly type = FETCH_INVENTORY_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchInventoryFailedAction implements Action {
  readonly type = FETCH_INVENTORY_FAILED;
  constructor(public payload?: any) { }
}

export class FilterInventoryAction implements Action {
  readonly type = FILTER_INVENTORY;
  constructor(public payload: any) { }
}

export class FilterInventoryCompleteAction implements Action {
  readonly type = FILTER_INVENTORY_COMPLETE;
  constructor(public payload: any) { }
}

export class FilterInventoryFailedAction implements Action {
  readonly type = FILTER_INVENTORY_FAILED;
  constructor(public payload?: any) { }
}

export class FetchInventoryHistoryAction implements Action {
  readonly type = FETCH_INVENTORY_HISTORY;
  constructor(public payload?: any) { }
}

export class FetchInventoryHistoryCompleteAction implements Action {
  readonly type = FETCH_INVENTORY_HISTORY_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchInventoryHistoryFailedAction implements Action {
  readonly type = FETCH_INVENTORY_HISTORY_FAILED;
  constructor(public payload?: any) { }
}

export class OpenInventoryModalAction implements Action {
  readonly type = OPEN_INVENTORY_MODAL_ACTION;
}

export class CloseInventoryModalAction implements Action {
  readonly type = CLOSE_INVENTORY_MODAL_ACTION;
}

export class ClearInventoryStateAction implements Action {
  readonly type = CLEAR_INVENTORY_STATE_ACTION;
}

export type Actions =
  CreateInventoryAction
  | CreateNewInventoryCompleteAction
  | CreateNewInventoryFailedAction
  | UpdateInventoryAction
  | UpdateInventoryCompleteAction
  | UpdateInventoryFailedAction
  | DeleteInventoryAction
  | DeleteInventoryCompleteAction
  | DeleteInventoryFailedAction
  | FetchAllInventoryAction
  | FetchAllInventoryCompleteAction
  | FetchAllInventoryFailedAction
  | FetchInventoryAction
  | FetchInventoryCompleteAction
  | FetchInventoryFailedAction
  | FilterInventoryAction
  | FilterInventoryCompleteAction
  | FilterInventoryFailedAction
  | FetchInventoryHistoryAction
  | FetchInventoryHistoryCompleteAction
  | FetchInventoryHistoryFailedAction
  | OpenInventoryModalAction
  | CloseInventoryModalAction
  | ClearInventoryStateAction;