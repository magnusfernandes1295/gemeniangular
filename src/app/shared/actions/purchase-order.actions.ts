import { Action } from '@ngrx/store';
import swal from 'sweetalert2';

export const CREATE_PURCHASE_ORDER = '[Purchase Order] Create Purchase Order';
export const CREATE_PURCHASE_ORDER_COMPLETE = '[Purchase Order] Create Purchase Order Complete';
export const CREATE_PURCHASE_ORDER_FAILED = '[Purchase Order] Create Purchase Order Failed';

export const FETCH_PURCHASE_ORDER = '[Purchase Order] Fetch Purchase Order';
export const FETCH_PURCHASE_ORDER_COMPLETE = '[Purchase Order] Fetch Purchase Order Complete';
export const FETCH_PURCHASE_ORDER_FAILED = '[Purchase Order] Fetch Purchase Order Failed';

export const FETCH_ALL_PURCHASE_ORDERS = '[Purchase Order] Fetch All Purchase Orders';
export const FETCH_ALL_PURCHASE_ORDERS_COMPLETE = '[Purchase Order] Fetch All Purchase Orders Complete';
export const FETCH_ALL_PURCHASE_ORDERS_FAILED = '[Purchase Order] Fetch All Purchase Orders Failed';

export const FILTER_PURCHASE_ORDER = '[Purchase Order] Filter Purchase Order';
export const FILTER_PURCHASE_ORDER_COMPLETE = '[Purchase Order] Filter Purchase Order Complete';
export const FILTER_PURCHASE_ORDER_FAILED = '[Purchase Order] Filter Purchase Order Failed';

export const UPDATE_PURCHASE_ORDER = '[Purchase Order] Update Purchase Order';
export const UPDATE_PURCHASE_ORDER_COMPLETE = '[Purchase Order] Update Purchase Order Complete';
export const UPDATE_PURCHASE_ORDER_FAILED = '[Purchase Order] Update Purchase Order Failed';

export const DELETE_PURCHASE_ORDER = '[Purchase Order] Delete Purchase Order';
export const DELETE_PURCHASE_ORDER_COMPLETE = '[Purchase Order] Delete Purchase Order Complete';
export const DELETE_PURCHASE_ORDER_FAILED = '[Purchase Order] Delete Purchase Order Failed';

export const OPEN_PURCHASE_ORDER = '[Purchase Order] Open Purchase Order';
export const OPEN_PURCHASE_ORDER_COMPLETE = '[Purchase Order] Open Purchase Order Complete';
export const OPEN_PURCHASE_ORDER_FAILED = '[Purchase Order] Open Purchase Order Failed';

export const CONFIRM_PURCHASE_ORDER = '[Purchase Order] Confirm Purchase Order';
export const CONFIRM_PURCHASE_ORDER_COMPLETE = '[Purchase Order] Confirm Purchase Order Complete';
export const CONFIRM_PURCHASE_ORDER_FAILED = '[Purchase Order] Confirm Purchase Order Failed';

export const DISPATCH_PURCHASE_ORDER = '[Purchase Order] Dispatch Purchase Order';
export const DISPATCH_PURCHASE_ORDER_COMPLETE = '[Purchase Order] Dispatch Purchase Order Complete';
export const DISPATCH_PURCHASE_ORDER_FAILED = '[Purchase Order] Dispatch Purchase Order Failed';

export const CLOSE_PURCHASE_ORDER = '[Purchase Order] Close Purchase Order';
export const CLOSE_PURCHASE_ORDER_COMPLETE = '[Purchase Order] Close Purchase Order Complete';
export const CLOSE_PURCHASE_ORDER_FAILED = '[Purchase Order] Close Purchase Order Failed';

export const OPEN_PURCHASE_ORDER_MODAL = '[Purchase Order] Open Purchase Order Modal';
export const OPEN_PURCHASE_ORDER_DETAIL_MODAL = '[Purchase Order] Open Purchase Order Detail Modal';
export const OPEN_PURCHASE_ORDER_CONFIRM_MODAL = '[Purchase Order] Open Purchase Order Confirm Modal';
export const OPEN_PURCHASE_ORDER_DISPATCH_MODAL = '[Purchase Order] Open Purchase Order Dispatch Modal';
export const OPEN_PURCHASE_ORDER_CLOSE_MODAL = '[Purchase Order] Open Purchase Order Close Modal';
export const CLOSE_PURCHASE_ORDER_MODAL = '[Purchase Order] Close Purchase Order Modal';

export const CLEAR_PURCHASE_ORDER_STATE_ACTION = "[PurchaseOrder] Clear PurchaseOrder State Action";

export class CreatePurchaseOrderAction implements Action {
  readonly type = CREATE_PURCHASE_ORDER;
  constructor(public payload: any) { }
}

export class CreatePurchaseOrderCompleteAction implements Action {
  readonly type = CREATE_PURCHASE_ORDER_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Purchase order created!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class CreatePurchaseOrderFailedAction implements Action {
  readonly type = CREATE_PURCHASE_ORDER_FAILED;
  constructor(public payload: any) {
    swal("Creating P.O. failed!", this.payload.message, "error");
  }
}

export class FetchPurchaseOrderAction implements Action {
  readonly type = FETCH_PURCHASE_ORDER;
  constructor(public payload?: any) { }
}

export class FetchPurchaseOrderCompleteAction implements Action {
  readonly type = FETCH_PURCHASE_ORDER_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchPurchaseOrderFailedAction implements Action {
  readonly type = FETCH_PURCHASE_ORDER_FAILED;
  constructor(public payload?: any) { }
}

export class FetchAllPurchaseOrdersAction implements Action {
  readonly type = FETCH_ALL_PURCHASE_ORDERS;
  constructor(public payload?: any) { }
}

export class FetchAllPurchaseOrdersCompleteAction implements Action {
  readonly type = FETCH_ALL_PURCHASE_ORDERS_COMPLETE;
  constructor(public payload?: any) { }
}

export class FetchAllPurchaseOrdersFailedAction implements Action {
  readonly type = FETCH_ALL_PURCHASE_ORDERS_FAILED;
  constructor(public payload?: any) { }
}

export class FilterPurchaseOrderAction implements Action {
  readonly type = FILTER_PURCHASE_ORDER;
  constructor(public payload: any) { }
}

export class FilterPurchaseOrderCompleteAction implements Action {
  readonly type = FILTER_PURCHASE_ORDER_COMPLETE;
  constructor(public payload: any) { }
}

export class FilterPurchaseOrderFailedAction implements Action {
  readonly type = FILTER_PURCHASE_ORDER_FAILED;
  constructor(public payload?: any) { }
}

export class UpdatePurchaseOrderAction implements Action {
  readonly type = UPDATE_PURCHASE_ORDER;
  constructor(public payload?: any) { }
}

export class UpdatePurchaseOrderCompleteAction implements Action {
  readonly type = UPDATE_PURCHASE_ORDER_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Purchase order updated!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class UpdatePurchaseOrderFailedAction implements Action {
  readonly type = UPDATE_PURCHASE_ORDER_FAILED;
  constructor(public payload: any) {
    swal("Update P.O. Failed!", this.payload.message, "error");
  }
}

export class DeletePurchaseOrderAction implements Action {
  readonly type = DELETE_PURCHASE_ORDER;
  constructor(public payload: any) { }
}

export class DeletePurchaseOrderCompleteAction implements Action {
  readonly type = DELETE_PURCHASE_ORDER_COMPLETE;
  constructor(public payload?: any) {
    swal({
      title: "Purchase order deleted!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class DeletePurchaseOrderFailedAction implements Action {
  readonly type = DELETE_PURCHASE_ORDER_FAILED;
  constructor(public payload: any) {
    swal("Delete P.O. failed!", payload, "error");
  }
}

export class OpenPurchaseOrderAction implements Action {
  readonly type = OPEN_PURCHASE_ORDER;
  constructor(public payload: any) { }
}

export class OpenPurchaseOrderCompleteAction implements Action {
  readonly type = OPEN_PURCHASE_ORDER_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Purchase order opened!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class OpenPurchaseOrderFailedAction implements Action {
  readonly type = OPEN_PURCHASE_ORDER_FAILED;
  constructor(public payload: any) {
    swal("Open P.O. failed!", this.payload.message, "error");
  }
}

export class ConfirmPurchaseOrderAction implements Action {
  readonly type = CONFIRM_PURCHASE_ORDER;
  constructor(public payload: any) { }
}

export class ConfirmPurchaseOrderCompleteAction implements Action {
  readonly type = CONFIRM_PURCHASE_ORDER_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Purchase order confirmed!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class ConfirmPurchaseOrderFailedAction implements Action {
  readonly type = CONFIRM_PURCHASE_ORDER_FAILED;
  constructor(public payload: any) {
    swal("Confirm P.O. failed!", this.payload.message, "error");
  }
}

export class DispatchPurchaseOrderAction implements Action {
  readonly type = DISPATCH_PURCHASE_ORDER;
  constructor(public payload: any) { }
}

export class DispatchPurchaseOrderCompleteAction implements Action {
  readonly type = DISPATCH_PURCHASE_ORDER_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Purchase order dispatched!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class DispatchPurchaseOrderFailedAction implements Action {
  readonly type = DISPATCH_PURCHASE_ORDER_FAILED;
  constructor(public payload: any) {
    swal("Dispatch P.O. failed!", this.payload.message, "error");
  }
}

export class ClosePurchaseOrderAction implements Action {
  readonly type = CLOSE_PURCHASE_ORDER;
  constructor(public payload: any) { }
}

export class ClosePurchaseOrderCompleteAction implements Action {
  readonly type = CLOSE_PURCHASE_ORDER_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Purchase order closed!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class ClosePurchaseOrderFailedAction implements Action {
  readonly type = CLOSE_PURCHASE_ORDER_FAILED;
  constructor(public payload: any) {
    swal("Close P.O. failed!", this.payload.message, "error");
  }
}

export class OpenPurchaseOrderModalAction implements Action {
  readonly type = OPEN_PURCHASE_ORDER_MODAL;
  constructor(public payload?: any) { }
}

export class OpenPurchaseOrderDetailModalAction implements Action {
  readonly type = OPEN_PURCHASE_ORDER_DETAIL_MODAL;
  constructor(public payload: any) { }
}

export class OpenPurchaseOrderConfirmModalAction implements Action {
  readonly type = OPEN_PURCHASE_ORDER_CONFIRM_MODAL;
}

export class OpenPurchaseOrderDispatchModalAction implements Action {
  readonly type = OPEN_PURCHASE_ORDER_DISPATCH_MODAL;
  constructor(public payload: any) { }
}

export class OpenPurchaseOrderCloseModalAction implements Action {
  readonly type = OPEN_PURCHASE_ORDER_CLOSE_MODAL;
  constructor(public payload?: any) { }
}

export class ClosePurchaseOrderModalAction implements Action {
  readonly type = CLOSE_PURCHASE_ORDER_MODAL;
  constructor(public payload?: any) { }
}

export class ClearPurchaseOrderStateAction implements Action {
  readonly type = CLEAR_PURCHASE_ORDER_STATE_ACTION;
}

export type Actions =
  CreatePurchaseOrderAction
  | CreatePurchaseOrderCompleteAction
  | CreatePurchaseOrderFailedAction
  | FetchPurchaseOrderAction
  | FetchPurchaseOrderCompleteAction
  | FetchPurchaseOrderFailedAction
  | FetchAllPurchaseOrdersAction
  | FetchAllPurchaseOrdersCompleteAction
  | FetchAllPurchaseOrdersFailedAction
  | FilterPurchaseOrderAction
  | FilterPurchaseOrderCompleteAction
  | FilterPurchaseOrderFailedAction
  | UpdatePurchaseOrderAction
  | UpdatePurchaseOrderCompleteAction
  | UpdatePurchaseOrderFailedAction
  | DeletePurchaseOrderAction
  | DeletePurchaseOrderCompleteAction
  | DeletePurchaseOrderFailedAction
  | OpenPurchaseOrderAction
  | OpenPurchaseOrderCompleteAction
  | OpenPurchaseOrderFailedAction
  | ConfirmPurchaseOrderAction
  | ConfirmPurchaseOrderCompleteAction
  | ConfirmPurchaseOrderFailedAction
  | DispatchPurchaseOrderAction
  | DispatchPurchaseOrderCompleteAction
  | DispatchPurchaseOrderFailedAction
  | ClosePurchaseOrderAction
  | ClosePurchaseOrderCompleteAction
  | ClosePurchaseOrderFailedAction
  | OpenPurchaseOrderModalAction
  | OpenPurchaseOrderDetailModalAction
  | OpenPurchaseOrderConfirmModalAction
  | OpenPurchaseOrderDispatchModalAction
  | OpenPurchaseOrderCloseModalAction
  | ClosePurchaseOrderModalAction
  | ClearPurchaseOrderStateAction;