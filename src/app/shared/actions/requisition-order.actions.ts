import { Action } from '@ngrx/store';
import swal from 'sweetalert2';

export const CREATE_NEW_REQUISITION_ORDER = '[RequisitionOrder] Create New Requisition Order';
export const CREATE_NEW_REQUISITION_ORDER_COMPLETE = '[RequisitionOrder] Create New Requisition Order Complete';
export const CREATE_NEW_REQUISITION_ORDER_FAILED = '[RequisitionOrder] Create New Requisition Order Failed';

export const UPDATE_REQUISITION_ORDER = '[RequisitionOrder] Update Requisition Order';
export const UPDATE_REQUISITION_ORDER_COMPLETE = '[RequisitionOrder] Update Requisition Order Complete';
export const UPDATE_REQUISITION_ORDER_FAILED = '[RequisitionOrder] Update Requisition Order Failed';

export const DELETE_REQUISITION_ORDER = '[RequisitionOrder] Delete Requisition Order';
export const DELETE_REQUISITION_ORDER_COMPLETE = '[RequisitionOrder] Delete Requisition Order Complete';
export const DELETE_REQUISITION_ORDER_FAILED = '[RequisitionOrder] Delete Requisition Order Failed';

export const OPEN_REQUISITION_ORDER = '[RequisitionOrder] Open Requisition Order';
export const OPEN_REQUISITION_ORDER_COMPLETE = '[RequisitionOrder] Open Requisition Order Complete';
export const OPEN_REQUISITION_ORDER_FAILED = '[RequisitionOrder] Open Requisition Order Failed';

export const CLOSE_REQUISITION_ORDER = '[RequisitionOrder] Close Requisition Order';
export const CLOSE_REQUISITION_ORDER_COMPLETE = '[RequisitionOrder] Close Requisition Order Complete';
export const CLOSE_REQUISITION_ORDER_FAILED = '[RequisitionOrder] Close Requisition Order Failed';

export const FETCH_ALL_REQUISITION_ORDERS = '[RequisitionOrder] Fetch All Requisition Order';
export const FETCH_ALL_REQUISITION_ORDERS_COMPLETE = '[RequisitionOrder] Fetch All Requisition Order Complete';
export const FETCH_ALL_REQUISITION_ORDERS_FAILED = '[RequisitionOrder] Fetch All Requisition Order Failed';

export const FETCH_REQUISITION_ORDER = '[RequisitionOrder] Fetch Requisition Order';
export const FETCH_REQUISITION_ORDER_COMPLETE = '[RequisitionOrder] Fetch Requisition Order Complete';
export const FETCH_REQUISITION_ORDER_FAILED = '[RequisitionOrder] Fetch Requisition Order Failed';

export const FILTER_REQUISITION_ORDER = '[RequisitionOrder] Filter Requisition Order';
export const FILTER_REQUISITION_ORDER_COMPLETE = '[RequisitionOrder] Filter Requisition Order Complete';
export const FILTER_REQUISITION_ORDER_FAILED = '[RequisitionOrder] Filter Requisition Order Failed';

export const OPEN_REQUISITION_ORDER_MODAL = '[RequisitionOrder] Open Requisition Order Modal';
export const CLOSE_REQUISITION_ORDER_MODAL = '[RequisitionOrder] Close Requisition Order Modal';

export const OPEN_VIEW_REQUISITION_ORDER_MODAL = '[RequisitionOrder] Open View Requisition Order Modal';
export const CLOSE_VIEW_REQUISITION_ORDER_MODAL = '[RequisitionOrder] Close View Requisition Order Modal';

export const CLEAR_REQUISITION_ORDER_STATE_ACTION = "[RequisitionOrder] Clear RequisitionOrder State Action";

export class CreateNewRequisitionOrderAction implements Action {
  readonly type = CREATE_NEW_REQUISITION_ORDER;
  constructor(public payload: any) { }
}

export class CreateNewRequisitionOrderCompleteAction implements Action {
  readonly type = CREATE_NEW_REQUISITION_ORDER_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Requisition Order Added!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class CreateNewRequisitionOrderFailedAction implements Action {
  readonly type = CREATE_NEW_REQUISITION_ORDER_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", payload, "error");
  }
}

export class UpdateRequisitionOrderAction implements Action {
  readonly type = UPDATE_REQUISITION_ORDER;
  constructor(public payload: any) { }
}

export class UpdateRequisitionOrderCompleteAction implements Action {
  readonly type = UPDATE_REQUISITION_ORDER_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Requisition Order Updated!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class UpdateRequisitionOrderFailedAction implements Action {
  readonly type = UPDATE_REQUISITION_ORDER_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", payload, "error");
  }
}

export class DeleteRequisitionOrderAction implements Action {
  readonly type = DELETE_REQUISITION_ORDER;
  constructor(public payload: any) { }
}

export class DeleteRequisitionOrderCompleteAction implements Action {
  readonly type = DELETE_REQUISITION_ORDER_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Requisition Order Deleted!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class DeleteRequisitionOrderFailedAction implements Action {
  readonly type = DELETE_REQUISITION_ORDER_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", payload, "error");
  }
}


export class OpenRequisitionOrderAction implements Action {
  readonly type = OPEN_REQUISITION_ORDER;
  constructor(public payload: any) { }
}

export class OpenRequisitionOrderCompleteAction implements Action {
  readonly type = OPEN_REQUISITION_ORDER_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Requisition Order Opened!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class OpenRequisitionOrderFailedAction implements Action {
  readonly type = OPEN_REQUISITION_ORDER_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", payload, "error");
  }
}


export class CloseRequisitionOrderAction implements Action {
  readonly type = CLOSE_REQUISITION_ORDER;
  constructor(public payload: any) { }
}

export class CloseRequisitionOrderCompleteAction implements Action {
  readonly type = CLOSE_REQUISITION_ORDER_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Requisition Order Closed!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class CloseRequisitionOrderFailedAction implements Action {
  readonly type = CLOSE_REQUISITION_ORDER_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", payload, "error");
  }
}

export class FetchAllRequisitionOrdersAction implements Action {
  readonly type = FETCH_ALL_REQUISITION_ORDERS;
  constructor(public payload?: any) { }
}

export class FetchAllRequisitionOrdersCompleteAction implements Action {
  readonly type = FETCH_ALL_REQUISITION_ORDERS_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchAllRequisitionOrdersFailedAction implements Action {
  readonly type = FETCH_ALL_REQUISITION_ORDERS_FAILED;
  constructor(public payload?: any) { }
}

export class FetchRequisitionOrderAction implements Action {
  readonly type = FETCH_REQUISITION_ORDER;
  constructor(public payload: any) { }
}

export class FetchRequisitionOrderCompleteAction implements Action {
  readonly type = FETCH_REQUISITION_ORDER_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchRequisitionOrderFailedAction implements Action {
  readonly type = FETCH_REQUISITION_ORDER_FAILED;
  constructor(public payload?: any) { }
}

export class FilterRequisitionOrderAction implements Action {
  readonly type = FILTER_REQUISITION_ORDER;
  constructor(public payload: any) { }
}

export class FilterRequisitionOrderCompleteAction implements Action {
  readonly type = FILTER_REQUISITION_ORDER_COMPLETE;
  constructor(public payload: any) { }
}

export class FilterRequisitionOrderFailedAction implements Action {
  readonly type = FILTER_REQUISITION_ORDER_FAILED;
  constructor(public payload?: any) { }
}

export class OpenRequisitionOrderModal implements Action {
  readonly type = OPEN_REQUISITION_ORDER_MODAL;
  constructor(public payload?: any) { }
}

export class CloseRequisitionOrderModal implements Action {
  readonly type = CLOSE_REQUISITION_ORDER_MODAL;
  constructor(public payload?: any) { }
}

export class OpenViewRequisitionOrderModal implements Action {
  readonly type = OPEN_VIEW_REQUISITION_ORDER_MODAL;
  constructor(public payload?: any) { }
}

export class CloseViewRequisitionOrderModal implements Action {
  readonly type = CLOSE_VIEW_REQUISITION_ORDER_MODAL;
  constructor(public payload?: any) { }
}

export class ClearRequisitionOrderStateAction implements Action {
  readonly type = CLEAR_REQUISITION_ORDER_STATE_ACTION;
}

export type Actions =
  CreateNewRequisitionOrderAction
  | CreateNewRequisitionOrderCompleteAction
  | CreateNewRequisitionOrderFailedAction
  | UpdateRequisitionOrderAction
  | UpdateRequisitionOrderCompleteAction
  | UpdateRequisitionOrderFailedAction
  | DeleteRequisitionOrderAction
  | DeleteRequisitionOrderCompleteAction
  | DeleteRequisitionOrderFailedAction
  | OpenRequisitionOrderAction
  | OpenRequisitionOrderCompleteAction
  | OpenRequisitionOrderFailedAction
  | CloseRequisitionOrderAction
  | CloseRequisitionOrderCompleteAction
  | CloseRequisitionOrderFailedAction
  | FetchAllRequisitionOrdersAction
  | FetchAllRequisitionOrdersCompleteAction
  | FetchAllRequisitionOrdersFailedAction
  | FetchRequisitionOrderAction
  | FetchRequisitionOrderCompleteAction
  | FetchRequisitionOrderFailedAction
  | FilterRequisitionOrderAction
  | FilterRequisitionOrderCompleteAction
  | FilterRequisitionOrderFailedAction
  | OpenRequisitionOrderModal
  | CloseRequisitionOrderModal
  | OpenViewRequisitionOrderModal
  | CloseViewRequisitionOrderModal
  | ClearRequisitionOrderStateAction;