import { Action } from "@ngrx/store";
import swal from "sweetalert2";
import { User } from "../models/index";

export const LOGIN = "[User] Login User";
export const LOGIN_COMPLETE = "[User] Login Complete";
export const LOGIN_FAILURE = "[User] Login Failure";

export const VALIDATE_TOKEN = "[User] Validate User Token";
export const VALIDATE_TOKEN_COMPLETE = "[User] Validate User Token Complete";
export const VALIDATE_TOKEN_FAILED = "[User] Validate User Token Failed";

export const SIGNOUT = "[User] Signout User";
export const SIGNOUT_COMPLETE = "[User] Signout User Complete";
export const SIGNOUT_FAILED = "[User] Signout User Failed";

export const CREATE_NEW_USER = "[User] Create New User";
export const CREATE_NEW_USER_COMPLETE = "[User] Create New User Complete";
export const CREATE_NEW_USER_FAILED = "[User] Create New User Failed";

export const UPDATE_USER = "[User] Update User";
export const CHANGE_PASSWORD = "[User] Change Password";
export const UPDATE_USER_COMPLETE = "[User] Update User Complete";
export const UPDATE_USER_FAILED = "[User] Update User Failed";

export const DELETE_USER = "[User] Delete User";
export const DELETE_USER_COMPLETE = "[User] Delete User Complete";
export const DELETE_USER_FAILED = "[User] Delete User Failed";

export const FETCH_ALL_USERS = "[User] Fetch All Users";
export const FETCH_ALL_USERS_COMPLETE = "[User] Fetch All Users Complete";
export const FETCH_ALL_USERS_FAILED = "[User] Fetch All Users Failed";

export const FETCH_USER = "[User] Fetch User";
export const FETCH_USER_COMPLETE = "[User] Fetch User Complete";
export const FETCH_USER_FAILED = "[User] Fetch User Failed";

export const OPEN_USER_MODAL = "[User] Open User Modal";
export const OPEN_USER_PASSWORD_MODAL = "[User] Open User Password Modal";
export const CLOSE_USER_MODAL = "[User] Close User Modal";

export const CLEAR_USER_STATE_ACTION = "[User] Clear User State Action";

export class LoginAction implements Action {
  readonly type = LOGIN;
  constructor(public payload?: any) {}
}

export class LoginCompleteAction implements Action {
  readonly type = LOGIN_COMPLETE;
  constructor(public payload: User) {}
}

export class LoginFailureAction implements Action {
  readonly type = LOGIN_FAILURE;
  constructor(public payload: any) {
    swal("Login failed.", "error");
  }
}

export class ValidateTokenAction implements Action {
  readonly type = VALIDATE_TOKEN;
  constructor(public payload?: any) {}
}

export class ValidateTokenCompleteAction implements Action {
  readonly type = VALIDATE_TOKEN_COMPLETE;
  constructor(public payload?: any) {}
}

export class ValidateTokenFailedAction implements Action {
  readonly type = VALIDATE_TOKEN_FAILED;
  constructor(public payload: any) {
    swal("User signed out!", "There was an error connecting to the server.", "info");
  }
}

export class SignoutUserAction implements Action {
  readonly type = SIGNOUT;
  constructor(public payload?: any) {}
}

export class SignoutUserCompleteAction implements Action {
  readonly type = SIGNOUT_COMPLETE;
  constructor(public payload?: any) {
    swal({
      title: "Signout Successful",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class SignoutUserFailedAction implements Action {
  readonly type = SIGNOUT_FAILED;
  constructor(public payload: any) {
    swal("Signout failed!", "error");
  }
}

export class CreateNewUserAction implements Action {
  readonly type = CREATE_NEW_USER;
  constructor(public payload: any) { }
}

export class CreateNewUserCompleteAction implements Action {
  readonly type = CREATE_NEW_USER_COMPLETE;
  constructor(public payload: any) {
    swal('Great!', 'User was created!', 'success');
  }
}

export class CreateNewUserFailedAction implements Action {
  readonly type = CREATE_NEW_USER_FAILED;
  constructor(public payload: any) {
    swal("Oops!!!", 'New user could not be created', 'error');
  }
}

export class UpdateUserAction implements Action {
  readonly type = UPDATE_USER;
  constructor(public payload: any) { }
}

export class UpdateUserCompleteAction implements Action {
  readonly type = UPDATE_USER_COMPLETE;
  constructor(public payload: any) {
    swal('Great!', 'User was updated.', 'success');
  }
}

export class UpdateUserFailedAction implements Action {
  readonly type = UPDATE_USER_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", payload, 'error');
  }
}

export class ChangePasswordAction implements Action {
  readonly type = CHANGE_PASSWORD;
  constructor(public payload: any) { }
}

export class DeleteUserAction implements Action {
  readonly type = DELETE_USER;
  constructor(public payload: any) { }
}

export class DeleteUserCompleteAction implements Action {
  readonly type = DELETE_USER_COMPLETE;
  constructor(public payload: any) {
    swal('Great!', 'User was deleted.', 'success');
  }
}

export class DeleteUserFailedAction implements Action {
  readonly type = DELETE_USER_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", payload, 'error');
  }
}

export class FetchAllUsersAction implements Action {
  readonly type = FETCH_ALL_USERS;
  constructor(public payload?: any) { }
}

export class FetchAllUsersCompleteAction implements Action {
  readonly type = FETCH_ALL_USERS_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchAllUsersFailedAction implements Action {
  readonly type = FETCH_ALL_USERS_FAILED;
  constructor(public payload: any) { }
}

export class FetchUserAction implements Action {
  readonly type = FETCH_USER;
  constructor(public payload: any) { }
}

export class FetchUserCompleteAction implements Action {
  readonly type = FETCH_USER_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchUserFailedAction implements Action {
  readonly type = FETCH_USER_FAILED;
  constructor(public payload: any) { }
}

export class OpenUserModalAction implements Action {
  readonly type = OPEN_USER_MODAL;
}

export class OpenUserPasswordModalAction implements Action {
  readonly type = OPEN_USER_PASSWORD_MODAL;
}

export class CloseUserModalAction implements Action {
  readonly type = CLOSE_USER_MODAL;
}

export class ClearUserStateAction implements Action {
  readonly type = CLEAR_USER_STATE_ACTION;
}

export type Actions =
  LoginAction
  | LoginCompleteAction
  | LoginFailureAction
  | ValidateTokenAction
  | ValidateTokenCompleteAction
  | ValidateTokenFailedAction
  | SignoutUserAction
  | SignoutUserCompleteAction
  | SignoutUserFailedAction
  | CreateNewUserAction
  | CreateNewUserCompleteAction
  | CreateNewUserFailedAction
  | UpdateUserAction
  | UpdateUserCompleteAction
  | UpdateUserFailedAction
  | ChangePasswordAction
  | DeleteUserAction
  | DeleteUserCompleteAction
  | DeleteUserFailedAction
  | FetchAllUsersAction
  | FetchAllUsersCompleteAction
  | FetchAllUsersFailedAction
  | FetchUserAction
  | FetchUserCompleteAction
  | FetchUserFailedAction
  | OpenUserModalAction
  | OpenUserPasswordModalAction
  | CloseUserModalAction
  | ClearUserStateAction;
