import { Action } from '@ngrx/store';
import swal from 'sweetalert2';

export const FETCH_ALL_FEEDBACK = '[Feedback] Fetch All Feedback';
export const FETCH_ALL_FEEDBACK_COMPLETE = '[Feedback] Fetch All Feedback Complete';
export const FETCH_ALL_FEEDBACK_FAILED = '[Feedback] Fetch All Feedback Failed';

export const FETCH_FEEDBACK = '[Feedback] Fetch Feedback';
export const FETCH_FEEDBACK_COMPLETE = '[Feedback] Fetch Feedback Complete';
export const FETCH_FEEDBACK_FAILED = '[Feedback] Fetch Feedback Failed';

export const FILTER_FEEDBACK = '[Feedback] Filter Feedback';
export const FILTER_FEEDBACK_COMPLETE = '[Feedback] Filter Feedback Complete';
export const FILTER_FEEDBACK_FAILED = '[Feedback] Filter Feedback Failed';

export const CREATE_FEEDBACK = '[Feedback] Create Feedback';
export const CREATE_FEEDBACK_COMPLETE = '[Feedback] Create Feedback Complete';
export const CREATE_FEEDBACK_FAILED = '[Feedback] Create Feedback Failed';

export const UPDATE_FEEDBACK = '[Feedback] Update Feedback';
export const UPDATE_FEEDBACK_COMPLETE = '[Feedback] Update Feedback Complete';
export const UPDATE_FEEDBACK_FAILED = '[Feedback] Update Feedback Failed';

export const DELETE_FEEDBACK = '[Feedback] Delete Feedback';
export const DELETE_FEEDBACK_COMPLETE = '[Feedback] Delete Feedback Complete';
export const DELETE_FEEDBACK_FAILED = '[Feedback] Delete Feedback Failed';

export const MARK_READ_FEEDBACK = '[Feedback] Mark Read Feedback';
export const MARK_READ_FEEDBACK_COMPLETE = '[Feedback] Mark Read Feedback Complete';
export const MARK_READ_FEEDBACK_FAILED = '[Feedback] Mark Read Feedback Failed';

export const OPEN_FEEDBACK_MODAL = '[Feedback] Open Feedback Modal';
export const CLOSE_FEEDBACK_MODAL = '[Feedback] Close Feedback Modal';

export const CLEAR_FEEDBACK_STATE_ACTION = "[Feedback] Clear Feedback State Action";

export class FetchAllFeedbackAction implements Action {
  readonly type = FETCH_ALL_FEEDBACK;
}

export class FetchAllFeedbackCompleteAction implements Action {
  readonly type = FETCH_ALL_FEEDBACK_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchAllFeedbackFailedAction implements Action {
  readonly type = FETCH_ALL_FEEDBACK_FAILED;
  constructor(public payload: any) { }
}

export class FetchFeedbackAction implements Action {
  readonly type = FETCH_FEEDBACK;
  constructor(public payload: any) { }
}

export class FetchFeedbackCompleteAction implements Action {
  readonly type = FETCH_FEEDBACK_COMPLETE;
  constructor(public payload?: any) { }
}

export class FetchFeedbackFailedAction implements Action {
  readonly type = FETCH_FEEDBACK_FAILED;
  constructor(public payload?: any) { }
}

export class FilterFeedbackAction implements Action {
  readonly type = FILTER_FEEDBACK;
  constructor(public payload: any) { }
}

export class FilterFeedbackCompleteAction implements Action {
  readonly type = FILTER_FEEDBACK_COMPLETE;
  constructor(public payload?: any) { }
}

export class FilterFeedbackFailedAction implements Action {
  readonly type = FILTER_FEEDBACK_FAILED;
  constructor(public payload?: any) { }
}

export class CreateFeedbackAction implements Action {
  readonly type = CREATE_FEEDBACK;
  constructor(public payload:any) { }
}

export class CreateFeedbackCompleteAction implements Action {
  readonly type = CREATE_FEEDBACK_COMPLETE;
  constructor(public payload:any) {
    swal("Great!", "Feedback recorded!", "success");
  }
}

export class CreateFeedbackFailedAction implements Action {
  readonly type = CREATE_FEEDBACK_FAILED;
  constructor(public payload:any) {
    swal("There was an error creating the feedback.", payload, "error");
  }
}

export class UpdateFeedbackAction implements Action {
  readonly type = UPDATE_FEEDBACK;
  constructor(public payload: any) { }
}

export class UpdateFeedbackCompleteAction implements Action {
  readonly type = UPDATE_FEEDBACK_COMPLETE;
  constructor(public payload?: any) {
    swal("Great!", "Your feedback was updated!", "success");
  }
}

export class UpdateFeedbackFailedAction implements Action {
  readonly type = UPDATE_FEEDBACK_FAILED;
  constructor(public payload?: any) {
    swal("There was an error updating the feedback.", payload, "error");
  }
}

export class DeleteFeedbackAction implements Action {
  readonly type = DELETE_FEEDBACK;
  constructor(public payload: any) { }
}

export class DeleteFeedbackCompleteAction implements Action {
  readonly type = DELETE_FEEDBACK_COMPLETE;
  constructor(public payload?: any) {
    swal("Great!", "Your feedback was deleted!", "success");
  }
}

export class DeleteFeedbackFailedAction implements Action {
  readonly type = DELETE_FEEDBACK_FAILED;
  constructor(public payload?: any) {
    swal("There was an error deleting the feedback.", payload, "error");
  }
}

export class MarkReadFeedbackAction implements Action {
  readonly type = MARK_READ_FEEDBACK;
  constructor(public payload:any) { }
}

export class MarkReadFeedbackCompleteAction implements Action {
  readonly type = MARK_READ_FEEDBACK_COMPLETE;
  constructor(public payload:any) {
  }
}

export class MarkReadFeedbackFailedAction implements Action {
  readonly type = MARK_READ_FEEDBACK_FAILED;
  constructor(public payload:any) {
    swal("There was an error marking the feedback.", payload, "error");
  }
}

export class OpenFeedbackModal implements Action {
  readonly type = OPEN_FEEDBACK_MODAL;
}

export class CloseFeedbackModal implements Action {
  readonly type = CLOSE_FEEDBACK_MODAL;
}

export class ClearFeedbackStateAction implements Action {
  readonly type = CLEAR_FEEDBACK_STATE_ACTION;
}

export type Actions =
  FetchAllFeedbackAction
  | FetchAllFeedbackCompleteAction
  | FetchAllFeedbackFailedAction
  | FetchFeedbackAction
  | FetchFeedbackCompleteAction
  | FetchFeedbackFailedAction
  | FilterFeedbackAction
  | FilterFeedbackCompleteAction
  | FilterFeedbackFailedAction
  | CreateFeedbackAction
  | CreateFeedbackCompleteAction
  | CreateFeedbackFailedAction
  | UpdateFeedbackAction
  | UpdateFeedbackCompleteAction
  | UpdateFeedbackFailedAction
  | DeleteFeedbackAction
  | DeleteFeedbackCompleteAction
  | DeleteFeedbackFailedAction
  | MarkReadFeedbackAction
  | MarkReadFeedbackCompleteAction
  | MarkReadFeedbackFailedAction
  | OpenFeedbackModal
  | CloseFeedbackModal
  | ClearFeedbackStateAction;