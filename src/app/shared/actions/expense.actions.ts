import { Action } from '@ngrx/store';
import swal from 'sweetalert2';

export const FETCH_ALL_EXPENSES = '[Expense] Fetch All Expenses';
export const FETCH_ALL_EXPENSES_COMPLETE = '[Expense] Fetch All Expenses Complete';
export const FETCH_ALL_EXPENSES_FAILED = '[Expense] Fetch All Expenses Failed';

export const FETCH_EXPENSE = '[Expense] Fetch Expense';
export const FETCH_EXPENSE_COMPLETE = '[Expense] Fetch Expense Complete';
export const FETCH_EXPENSE_FAILED = '[Expense] Fetch Expense Failed';

export const CREATE_EXPENSE = '[Expense] Create Expense';
export const CREATE_EXPENSE_COMPLETE = '[Expense] Create Expense Complete';
export const CREATE_EXPENSE_FAILED = '[Expense] Create Expense Failed';

export const UPDATE_EXPENSE = '[Expense] Update Expense';
export const UPDATE_EXPENSE_COMPLETE = '[Expense] Update Expense Complete';
export const UPDATE_EXPENSE_FAILED = '[Expense] Update Expense Failed';

export const DELETE_EXPENSE = '[Expense] Delete Expense';
export const DELETE_EXPENSE_COMPLETE = '[Expense] Delete Expense Complete';
export const DELETE_EXPENSE_FAILED = '[Expense] Delete Expense Failed';

export const OPEN_EXPENSE_MODAL_ACTION = '[Expense] Open Expense Modal Action';
export const CLOSE_EXPENSE_MODAL_ACTION = '[Expense] Close Expense Modal Action';

export const CLEAR_EXPENSE_STATE_ACTION = "[Expense] Clear Expense State Action";

export class FetchAllExpensesAction implements Action {
  readonly type = FETCH_ALL_EXPENSES;
  constructor(public payload?: any) { }
}

export class FetchAllExpensesCompleteAction implements Action {
  readonly type = FETCH_ALL_EXPENSES_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchAllExpensesFailedAction implements Action {
  readonly type = FETCH_ALL_EXPENSES_FAILED;
  constructor(public payload?: any) { }
}

export class FetchExpenseAction implements Action {
  readonly type = FETCH_EXPENSE;
  constructor(public payload?: any) { }
}

export class FetchExpenseCompleteAction implements Action {
  readonly type = FETCH_EXPENSE_COMPLETE;
  constructor(public payload?: any) { }
}

export class FetchExpenseFailedAction implements Action {
  readonly type = FETCH_EXPENSE_FAILED;
  constructor(public payload?: any) { }
}

export class CreateExpenseAction implements Action {
  readonly type = CREATE_EXPENSE;
  constructor(public payload:any) { }
}

export class CreateExpenseCompleteAction implements Action {
  readonly type = CREATE_EXPENSE_COMPLETE;
  constructor(public payload:any) {
    swal("Great!", "Expense was recorded.","success");
  }
}

export class CreateExpenseFailedAction implements Action {
  readonly type = CREATE_EXPENSE_FAILED;
  constructor(public payload:any) {
    swal("There was an error.", payload, "error");
  }
}

export class UpdateExpenseAction implements Action {
  readonly type = UPDATE_EXPENSE;
  constructor(public payload: any) { }
}

export class UpdateExpenseCompleteAction implements Action {
  readonly type = UPDATE_EXPENSE_COMPLETE;
  constructor(public payload: any) {
    swal("Great!", "Expense was updated.","success");
  }
}

export class UpdateExpenseFailedAction implements Action {
  readonly type = UPDATE_EXPENSE_FAILED;
  constructor(public payload?: any) {
    swal("There was an error.", payload, "error");
  }
}

export class DeleteExpenseAction implements Action {
  readonly type = DELETE_EXPENSE;
  constructor(public payload: any) { }
}

export class DeleteExpenseCompleteAction implements Action {
  readonly type = DELETE_EXPENSE_COMPLETE;
  constructor(public payload: any) {
    swal("Great!", "Expense was deleted.","success");
  }
}

export class DeleteExpenseFailedAction implements Action {
  readonly type = DELETE_EXPENSE_FAILED;
  constructor(public payload?: any) {
    swal("There was an error.", payload, "error");
  }
}

export class OpenExpenseModalAction implements Action {
  readonly type = OPEN_EXPENSE_MODAL_ACTION;
}

export class CloseExpenseModalAction implements Action {
  readonly type = CLOSE_EXPENSE_MODAL_ACTION;
}

export class ClearExpenseStateAction implements Action {
  readonly type = CLEAR_EXPENSE_STATE_ACTION;
}

export type Actions =
  FetchAllExpensesAction
  | FetchAllExpensesCompleteAction
  | FetchAllExpensesFailedAction
  | FetchExpenseAction
  | FetchExpenseCompleteAction
  | FetchExpenseFailedAction
  | CreateExpenseAction
  | CreateExpenseCompleteAction
  | CreateExpenseFailedAction
  | UpdateExpenseAction
  | UpdateExpenseCompleteAction
  | UpdateExpenseFailedAction
  | DeleteExpenseAction
  | DeleteExpenseCompleteAction
  | DeleteExpenseFailedAction
  | OpenExpenseModalAction
  | CloseExpenseModalAction
  | ClearExpenseStateAction;