import { Action } from '@ngrx/store';
import swal from 'sweetalert2';

export const FETCH_ALL_TRANSACTIONS = '[Transaction] Fetch All Transactions';
export const FETCH_ALL_TRANSACTIONS_COMPLETE = '[Transaction] Fetch All Transactions Complete';
export const FETCH_ALL_TRANSACTIONS_FAILED = '[Transaction] Fetch All Transactions Failed';

export const FETCH_TRANSACTION = '[Transaction] Fetch Transaction';
export const FETCH_TRANSACTION_COMPLETE = '[Transaction] Fetch Transaction Complete';
export const FETCH_TRANSACTION_FAILED = '[Transaction] Fetch Transaction Failed';

export const OPEN_TRANSACTION_MODAL_ACTION = '[Transaction] Open Transaction Modal Action';
export const CLOSE_TRANSACTION_MODAL_ACTION = '[Transaction] Close Transaction Modal Action';

export const CLEAR_TRANSACTION_STATE_ACTION = "[Transaction] Clear Transaction State Action";

export class FetchAllTransactionsAction implements Action {
  readonly type = FETCH_ALL_TRANSACTIONS;
  constructor(public payload?: any) { }
}

export class FetchAllTransactionsCompleteAction implements Action {
  readonly type = FETCH_ALL_TRANSACTIONS_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchAllTransactionsFailedAction implements Action {
  readonly type = FETCH_ALL_TRANSACTIONS_FAILED;
  constructor(public payload?: any) { }
}

export class FetchTransactionAction implements Action {
  readonly type = FETCH_TRANSACTION;
  constructor(public payload: any) { }
}

export class FetchTransactionCompleteAction implements Action {
  readonly type = FETCH_TRANSACTION_COMPLETE;
  constructor(public payload?: any) { }
}

export class FetchTransactionFailedAction implements Action {
  readonly type = FETCH_TRANSACTION_FAILED;
  constructor(public payload?: any) { }
}

export class OpenTransactionModalAction implements Action {
  readonly type = OPEN_TRANSACTION_MODAL_ACTION;
}

export class CloseTransactionModalAction implements Action {
  readonly type = CLOSE_TRANSACTION_MODAL_ACTION;
}

export class ClearTransactionStateAction implements Action {
  readonly type = CLEAR_TRANSACTION_STATE_ACTION;
}

export type Actions =
  FetchAllTransactionsAction
  | FetchAllTransactionsCompleteAction
  | FetchAllTransactionsFailedAction
  | FetchTransactionAction
  | FetchTransactionCompleteAction
  | FetchTransactionFailedAction
  | OpenTransactionModalAction
  | CloseTransactionModalAction
  | ClearTransactionStateAction;