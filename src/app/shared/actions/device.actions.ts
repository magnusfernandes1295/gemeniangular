import { Action } from '@ngrx/store';
import swal from 'sweetalert2';

export const FETCH_ALL_DEVICES = '[Device] Fetch All Devices';
export const FETCH_ALL_DEVICES_COMPLETE = '[Device] Fetch All Devices Complete';
export const FETCH_ALL_DEVICES_FAILED = '[Device] Fetch All Devices Failed';

export const FETCH_DEVICE = '[Device] Fetch Device';
export const FETCH_DEVICE_COMPLETE = '[Device] Fetch Device Complete';
export const FETCH_DEVICE_FAILED = '[Device] Fetch Device Failed';

export const CREATE_DEVICE = '[Device] Create Device';
export const CREATE_DEVICE_COMPLETE = '[Device] Create Device Complete';
export const CREATE_DEVICE_FAILED = '[Device] Create Device Failed';

export const UPDATE_DEVICE = '[Device] Update Device';
export const UPDATE_DEVICE_COMPLETE = '[Device] Update Device Complete';
export const UPDATE_DEVICE_FAILED = '[Device] Update Device Failed';

export const DELETE_DEVICE = '[Device] Delete Device';
export const DELETE_DEVICE_COMPLETE = '[Device] Delete Device Complete';
export const DELETE_DEVICE_FAILED = '[Device] Delete Device Failed';

export const TRANSFER_DEVICE = '[Device] Transfer Device';
export const TRANSFER_DEVICE_COMPLETE = '[Device] Transfer Device Complete';
export const TRANSFER_DEVICE_FAILED = '[Device] Transfer Device Failed';

export const OPEN_DEVICE_MODAL_ACTION = '[Device] Open Device Modal Action';
export const CLOSE_DEVICE_MODAL_ACTION = '[Device] Close Device Modal Action';

export const CLEAR_DEVICE_STATE_ACTION = "[Device] Clear Device State Action";

export class FetchAllDevicesAction implements Action {
  readonly type = FETCH_ALL_DEVICES;
  constructor(public payload?: any) {
    swal({
      title: 'Fetching devices...'
    });
    swal.showLoading();
  }
}

export class FetchAllDevicesCompleteAction implements Action {
  readonly type = FETCH_ALL_DEVICES_COMPLETE;
  constructor(public payload: any) {
    swal.close();
  }
}

export class FetchAllDevicesFailedAction implements Action {
  readonly type = FETCH_ALL_DEVICES_FAILED;
  constructor(public payload?: any) { }
}

export class FetchDeviceAction implements Action {
  readonly type = FETCH_DEVICE;
  constructor(public payload?: any) { }
}

export class FetchDeviceCompleteAction implements Action {
  readonly type = FETCH_DEVICE_COMPLETE;
  constructor(public payload?: any) { }
}

export class FetchDeviceFailedAction implements Action {
  readonly type = FETCH_DEVICE_FAILED;
  constructor(public payload?: any) { }
}

export class CreateDeviceAction implements Action {
  readonly type = CREATE_DEVICE;
  constructor(public payload:any) { }
}

export class CreateDeviceCompleteAction implements Action {
  readonly type = CREATE_DEVICE_COMPLETE;
  constructor(public payload:any) {
    swal({
      title: "Device Created!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class CreateDeviceFailedAction implements Action {
  readonly type = CREATE_DEVICE_FAILED;
  constructor(public payload:any) {
    swal("There was an error.", payload, "error");
  }
}

export class UpdateDeviceAction implements Action {
  readonly type = UPDATE_DEVICE;
  constructor(public payload?: any) { }
}

export class UpdateDeviceCompleteAction implements Action {
  readonly type = UPDATE_DEVICE_COMPLETE;
  constructor(public payload?: any) { }
}

export class UpdateDeviceFailedAction implements Action {
  readonly type = UPDATE_DEVICE_FAILED;
  constructor(public payload?: any) { }
}

export class DeleteDeviceAction implements Action {
  readonly type = DELETE_DEVICE;
  constructor(public payload?: any) { }
}

export class DeleteDeviceCompleteAction implements Action {
  readonly type = DELETE_DEVICE_COMPLETE;
  constructor(public payload?: any) { }
}

export class DeleteDeviceFailedAction implements Action {
  readonly type = DELETE_DEVICE_FAILED;
  constructor(public payload?: any) { }
}

export class TransferDeviceAction implements Action {
  readonly type = TRANSFER_DEVICE;
  constructor(public payload:any) { }
}

export class TransferDeviceCompleteAction implements Action {
  readonly type = TRANSFER_DEVICE_COMPLETE;
  constructor(public payload:any) {
    swal({
      title: "Devices are transferred!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class TransferDeviceFailedAction implements Action {
  readonly type = TRANSFER_DEVICE_FAILED;
  constructor(public payload:any) {
    swal("There was an error transferring the device.", payload, "error");
  }
}

export class OpenDeviceModalAction implements Action {
  readonly type = OPEN_DEVICE_MODAL_ACTION;
}

export class CloseDeviceModalAction implements Action {
  readonly type = CLOSE_DEVICE_MODAL_ACTION;
}

export class ClearDeviceStateAction implements Action {
  readonly type = CLEAR_DEVICE_STATE_ACTION;
}

export type Actions =
  FetchAllDevicesAction
  | FetchAllDevicesCompleteAction
  | FetchAllDevicesFailedAction
  | FetchDeviceAction
  | FetchDeviceCompleteAction
  | FetchDeviceFailedAction
  | CreateDeviceAction
  | CreateDeviceCompleteAction
  | CreateDeviceFailedAction
  | UpdateDeviceAction
  | UpdateDeviceCompleteAction
  | UpdateDeviceFailedAction
  | DeleteDeviceAction
  | DeleteDeviceCompleteAction
  | DeleteDeviceFailedAction
  | TransferDeviceAction
  | TransferDeviceCompleteAction
  | TransferDeviceFailedAction
  | OpenDeviceModalAction
  | CloseDeviceModalAction
  | ClearDeviceStateAction;