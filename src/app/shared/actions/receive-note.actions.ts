import { Action } from '@ngrx/store';
import swal from 'sweetalert2';

export const CREATE_NEW_RECEIVE_NOTE = '[ReceiveNote] Create New Receive Note';
export const CREATE_NEW_RECEIVE_NOTE_COMPLETE = '[ReceiveNote] Create New Receive Note Complete';
export const CREATE_NEW_RECEIVE_NOTE_FAILED = '[ReceiveNote] Create New Receive Note Failed';

export const UPDATE_RECEIVE_NOTE = '[ReceiveNote] Update Receive Note';
export const UPDATE_RECEIVE_NOTE_COMPLETE = '[ReceiveNote] Update Receive Note Complete';
export const UPDATE_RECEIVE_NOTE_FAILED = '[ReceiveNote] Update Receive Note Failed';

export const DELETE_RECEIVE_NOTE = '[ReceiveNote] Delete Receive Note';
export const DELETE_RECEIVE_NOTE_COMPLETE = '[ReceiveNote] Delete Receive Note Complete';
export const DELETE_RECEIVE_NOTE_FAILED = '[ReceiveNote] Delete Receive Note Failed';

export const CONFIRM_RECEIVE_NOTE = '[ReceiveNote] Confirm Receive Note';
export const CONFIRM_RECEIVE_NOTE_COMPLETE = '[ReceiveNote] Confirm Receive Note Complete';
export const CONFIRM_RECEIVE_NOTE_FAILED = '[ReceiveNote] Confirm Receive Note Failed';

export const FETCH_ALL_RECEIVE_NOTE = '[ReceiveNote] Fetch All Receive Note';
export const FETCH_ALL_RECEIVE_NOTE_COMPLETE = '[ReceiveNote] Fetch All Receive Note Complete';
export const FETCH_ALL_RECEIVE_NOTE_FAILED = '[ReceiveNote] Fetch All Receive Note Failed';

export const FETCH_RECEIVE_NOTE = '[ReceiveNote] Fetch Receive Note';
export const FETCH_RECEIVE_NOTE_COMPLETE = '[ReceiveNote] Fetch Receive Note Complete';
export const FETCH_RECEIVE_NOTE_FAILED = '[ReceiveNote] Fetch Receive Note Failed';

export const FILTER_RECEIVE_NOTE = '[ReceiveNote] Filter Receive Note';
export const FILTER_RECEIVE_NOTE_COMPLETE = '[ReceiveNote] Filter Receive Note Complete';
export const FILTER_RECEIVE_NOTE_FAILED = '[ReceiveNote] Filter Receive Note Failed';

export const OPEN_RECEIVE_NOTE_MODAL = '[ReceiveNote] Open Receive Note Modal';
export const OPEN_RECEIVE_NOTE_DETAIL_MODAL = '[ReceiveNote] Open Receive Note Detail Modal';
export const CLOSE_RECEIVE_NOTE_MODAL = '[ReceiveNote] Close Receive Note Modal';

export const CLEAR_RECEIVE_NOTE_STATE_ACTION = "[ReceiveNote] Clear ReceiveNote State Action";

export class CreateNewReceiveNoteAction implements Action {
  readonly type = CREATE_NEW_RECEIVE_NOTE;
  constructor(public payload: any) { }
}

export class CreateNewReceiveNoteCompleteAction implements Action {
  readonly type = CREATE_NEW_RECEIVE_NOTE_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Receive Note Created!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class CreateNewReceiveNoteFailedAction implements Action {
  readonly type = CREATE_NEW_RECEIVE_NOTE_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", this.payload.message, "error");
  }
}

export class UpdateReceiveNoteAction implements Action {
  readonly type = UPDATE_RECEIVE_NOTE;
  constructor(public payload: any) { }
}

export class UpdateReceiveNoteCompleteAction implements Action {
  readonly type = UPDATE_RECEIVE_NOTE_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Receive Note Updated!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class UpdateReceiveNoteFailedAction implements Action {
  readonly type = UPDATE_RECEIVE_NOTE_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", this.payload.message, "error");
  }
}

export class DeleteReceiveNoteAction implements Action {
  readonly type = DELETE_RECEIVE_NOTE;
  constructor(public payload: any) { }
}

export class DeleteReceiveNoteCompleteAction implements Action {
  readonly type = DELETE_RECEIVE_NOTE_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Receive Note Deleted!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class DeleteReceiveNoteFailedAction implements Action {
  readonly type = DELETE_RECEIVE_NOTE_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", this.payload.message, "error");
  }
}

export class ConfirmReceiveNoteAction implements Action {
  readonly type = CONFIRM_RECEIVE_NOTE;
  constructor(public payload: any) { }
}

export class ConfirmReceiveNoteCompleteAction implements Action {
  readonly type = CONFIRM_RECEIVE_NOTE_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Receive Note Confirmed!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class ConfirmReceiveNoteFailedAction implements Action {
  readonly type = CONFIRM_RECEIVE_NOTE_FAILED;
  constructor(public payload: any) {
    swal("There was an error.", this.payload.message, "error");
  }
}

export class FetchAllReceiveNoteAction implements Action {
  readonly type = FETCH_ALL_RECEIVE_NOTE;
  constructor(public payload?: any) { }
}

export class FetchAllReceiveNoteCompleteAction implements Action {
  readonly type = FETCH_ALL_RECEIVE_NOTE_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchAllReceiveNoteFailedAction implements Action {
  readonly type = FETCH_ALL_RECEIVE_NOTE_FAILED;
  constructor(public payload?: any) { }
}

export class FetchReceiveNoteAction implements Action {
  readonly type = FETCH_RECEIVE_NOTE;
  constructor(public payload: any) { }
}

export class FetchReceiveNoteCompleteAction implements Action {
  readonly type = FETCH_RECEIVE_NOTE_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchReceiveNoteFailedAction implements Action {
  readonly type = FETCH_RECEIVE_NOTE_FAILED;
  constructor(public payload?: any) { }
}

export class FilterReceiveNoteAction implements Action {
  readonly type = FILTER_RECEIVE_NOTE;
  constructor(public payload?: any) { }
}

export class FilterReceiveNoteCompleteAction implements Action {
  readonly type = FILTER_RECEIVE_NOTE_COMPLETE;
  constructor(public payload: any) { }
}

export class FilterReceiveNoteFailedAction implements Action {
  readonly type = FILTER_RECEIVE_NOTE_FAILED;
  constructor(public payload?: any) { }
}

export class OpenReceiveNoteModalAction implements Action {
  readonly type = OPEN_RECEIVE_NOTE_MODAL;
}

export class OpenReceiveNoteDetailModalAction implements Action {
  readonly type = OPEN_RECEIVE_NOTE_DETAIL_MODAL;
}

export class CloseReceiveNoteModalAction implements Action {
  readonly type = CLOSE_RECEIVE_NOTE_MODAL;
}

export class ClearReceiveNoteStateAction implements Action {
  readonly type = CLEAR_RECEIVE_NOTE_STATE_ACTION;
}

export type Actions =
  CreateNewReceiveNoteAction
  | CreateNewReceiveNoteCompleteAction
  | CreateNewReceiveNoteFailedAction
  | UpdateReceiveNoteAction
  | UpdateReceiveNoteCompleteAction
  | UpdateReceiveNoteFailedAction
  | DeleteReceiveNoteAction
  | DeleteReceiveNoteCompleteAction
  | DeleteReceiveNoteFailedAction
  | ConfirmReceiveNoteAction
  | ConfirmReceiveNoteCompleteAction
  | ConfirmReceiveNoteFailedAction
  | FetchAllReceiveNoteAction
  | FetchAllReceiveNoteCompleteAction
  | FetchAllReceiveNoteFailedAction
  | FetchReceiveNoteAction
  | FetchReceiveNoteCompleteAction
  | FetchReceiveNoteFailedAction
  | FilterReceiveNoteAction
  | FilterReceiveNoteCompleteAction
  | FilterReceiveNoteFailedAction
  | OpenReceiveNoteModalAction
  | OpenReceiveNoteDetailModalAction
  | CloseReceiveNoteModalAction
  | ClearReceiveNoteStateAction;
