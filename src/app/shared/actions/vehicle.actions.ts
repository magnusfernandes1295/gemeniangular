import { Action } from "@ngrx/store";
import swal from "sweetalert2";

export const CREATE_VEHICLE = "[Vehicle] Create Vehicle";
export const CREATE_VEHICLE_COMPLETE = "[Vehicle] Create Vehicle Complete";
export const CREATE_VEHICLE_FAILED = "[Vehicle] Create Vehicle Failed";

export const FETCH_ALL_VEHICLES = "[Vehicle] Fetch All Vehicles";
export const FETCH_ALL_VEHICLES_COMPLETE = "[Vehicle] Fetch All Vehicles Complete";
export const FETCH_ALL_VEHICLES_FAILED = "[Vehicle] Fetch All Vehicles Failed";

export const FETCH_VEHICLE = "[Vehicle] Fetch Vehicle";
export const FETCH_VEHICLE_COMPLETE = "[Vehicle] Fetch Vehicle Complete";
export const FETCH_VEHICLE_FAILED = "[Vehicle] Fetch Vehicle Failed";

export const UPDATE_VEHICLE = "[Vehicle] Update Vehicle";
export const UPDATE_VEHICLE_COMPLETE = "[Vehicle] Update Vehicle Complete";
export const UPDATE_VEHICLE_FAILED = "[Vehicle] Update Vehicle Failed";

export const DELETE_VEHICLE = "[Vehicle] Delete Vehicle";
export const DELETE_VEHICLE_COMPLETE = "[Vehicle] Delete Vehicle Complete";
export const DELETE_VEHICLE_FAILED = "[Vehicle] Delete Vehicle Failed";

export const UPDATE_VEHICLE_ICAT = "[Vehicle] Update Vehicle Icat";
export const UPDATE_VEHICLE_ICAT_COMPLETE = "[Vehicle] Update Vehicle Icat Complete";
export const UPDATE_VEHICLE_ICAT_FAILED = "[Vehicle] Update Vehicle Icat Failed";

export const DELETE_VEHICLE_ICAT = "[Vehicle] Delete Vehicle Icat";
export const DELETE_VEHICLE_ICAT_COMPLETE = "[Vehicle] Delete Vehicle Icat Complete";
export const DELETE_VEHICLE_ICAT_FAILED = "[Vehicle] Delete Vehicle Icat Failed";

export const OPEN_VEHICLE_MODAL = "[Vehicle] Open Vehicle Modal";
export const CLOSE_VEHICLE_MODAL = "[Vehicle] Close Vehicle Modal";

export const CLEAR_VEHICLE_STATE_ACTION = "[Vehicle] Clear Vehicle State Action";

export class CreateVehicleAction implements Action {
  readonly type = CREATE_VEHICLE;
  constructor(public payload: any) {
    swal({
      title: 'Creating vehicle...'
    });
    swal.showLoading();
  }
}

export class CreateVehicleCompleteAction implements Action {
  readonly type = CREATE_VEHICLE_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Vehicle created!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class CreateVehicleFailedAction implements Action {
  readonly type = CREATE_VEHICLE_FAILED;
  constructor(public payload?: any) {
    swal("There was an error.", payload, "error");
  }
}

export class FetchAllVehiclesAction implements Action {
  readonly type = FETCH_ALL_VEHICLES;
}

export class FetchAllVehiclesCompleteAction implements Action {
  readonly type = FETCH_ALL_VEHICLES_COMPLETE;
  constructor(public payload?: any) { }
}

export class FetchAllVehiclesFailedAction implements Action {
  readonly type = FETCH_ALL_VEHICLES_FAILED;
  constructor(public payload?: any) { }
}

export class FetchVehicleAction implements Action {
  readonly type = FETCH_VEHICLE;
  constructor(public payload: any) { }
}

export class FetchVehicleCompleteAction implements Action {
  readonly type = FETCH_VEHICLE_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchVehicleFailedAction implements Action {
  readonly type = FETCH_VEHICLE_FAILED;
  constructor(public payload?: any) { }
}

export class UpdateVehicleAction implements Action {
  readonly type = UPDATE_VEHICLE;
  constructor(public payload: any) { }
}

export class UpdateVehicleCompleteAction implements Action {
  readonly type = UPDATE_VEHICLE_COMPLETE;
  constructor(public payload?: any) {
    swal({
      title: "Vehicle updated!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class UpdateVehicleFailedAction implements Action {
  readonly type = UPDATE_VEHICLE_FAILED;
  constructor(public payload?: any) {
    swal("There was an error.", payload, "error");
  }
}

export class DeleteVehicleAction implements Action {
  readonly type = DELETE_VEHICLE;
  constructor(public payload: any) { }
}

export class DeleteVehicleCompleteAction implements Action {
  readonly type = DELETE_VEHICLE_COMPLETE;
  constructor(public payload?: any) {
    swal({
      title: "Vehicle deleted!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class DeleteVehicleFailedAction implements Action {
  readonly type = DELETE_VEHICLE_FAILED;
  constructor(public payload?: any) {
    swal("There was an error.", payload, "error");
  }
}

export class DeleteVehicleIcatAction implements Action {
  readonly type = DELETE_VEHICLE_ICAT;
  constructor(public payload: any) { }
}

export class DeleteVehicleIcatCompleteAction implements Action {
  readonly type = DELETE_VEHICLE_ICAT_COMPLETE;
  constructor(public payload?: any) {
    swal({
      title: "Document deleted!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class DeleteVehicleIcatFailedAction implements Action {
  readonly type = DELETE_VEHICLE_ICAT_FAILED;
  constructor(public payload?: any) {
    swal("There was an error.", payload, "error");
  }
}

export class UpdateVehicleIcatAction implements Action {
  readonly type = UPDATE_VEHICLE_ICAT;
  constructor(public payload: any) {
    swal({
      title: 'Creating vehicle...'
    });
    swal.showLoading();
  }
}

export class UpdateVehicleIcatCompleteAction implements Action {
  readonly type = UPDATE_VEHICLE_ICAT_COMPLETE;
  constructor(public payload?: any) { }
}

export class UpdateVehicleIcatFailedAction implements Action {
  readonly type = UPDATE_VEHICLE_ICAT_FAILED;
  constructor(public payload?: any) {
    swal("There was an error.", payload, "error");
  }
}

export class OpenVehicleModal implements Action {
  readonly type = OPEN_VEHICLE_MODAL;
}

export class CloseVehicleModal implements Action {
  readonly type = CLOSE_VEHICLE_MODAL;
}

export class ClearVehicleStateAction implements Action {
  readonly type = CLEAR_VEHICLE_STATE_ACTION;
}

export type Actions =
  | CreateVehicleAction
  | CreateVehicleCompleteAction
  | CreateVehicleFailedAction
  | FetchAllVehiclesAction
  | FetchAllVehiclesCompleteAction
  | FetchAllVehiclesFailedAction
  | FetchVehicleAction
  | FetchVehicleCompleteAction
  | FetchVehicleFailedAction
  | UpdateVehicleAction
  | UpdateVehicleCompleteAction
  | UpdateVehicleFailedAction
  | DeleteVehicleAction
  | DeleteVehicleCompleteAction
  | DeleteVehicleFailedAction
  | DeleteVehicleIcatAction
  | DeleteVehicleIcatCompleteAction
  | DeleteVehicleIcatFailedAction
  | UpdateVehicleIcatAction
  | UpdateVehicleIcatCompleteAction
  | UpdateVehicleIcatFailedAction
  | OpenVehicleModal
  | CloseVehicleModal
  | ClearVehicleStateAction;
