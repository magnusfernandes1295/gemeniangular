import { Action } from '@ngrx/store';
import swal from 'sweetalert2';

export const FETCH_ALL_SALARY_SLIPS = '[SalarySlip] Fetch All Salary Slips';
export const FETCH_ALL_SALARY_SLIPS_COMPLETE = '[SalarySlip] Fetch All Salary Slips Complete';
export const FETCH_ALL_SALARY_SLIPS_FAILED = '[SalarySlip] Fetch All Salary Slips Failed';

export const FETCH_SALARY_SLIP = '[SalarySlip] Fetch Salary Slip';
export const FETCH_SALARY_SLIP_COMPLETE = '[SalarySlip] Fetch Salary Slip Complete';
export const FETCH_SALARY_SLIP_FAILED = '[SalarySlip] Fetch Salary Slip Failed';

export const FILTER_SALARY_SLIPS = '[SalarySlip] Filter Salary Slips';
export const FILTER_SALARY_SLIPS_COMPLETE = '[SalarySlip] Filter Salary Slips Complete';
export const FILTER_SALARY_SLIPS_FAILED = '[SalarySlip] Filter Salary Slips Failed';

export const CREATE_SALARY_SLIP = '[SalarySlip] Create Salary Slip';
export const CREATE_SALARY_SLIP_COMPLETE = '[SalarySlip] Create Salary Slip Complete';
export const CREATE_SALARY_SLIP_FAILED = '[SalarySlip] Create Salary Slip Failed';

export const UPDATE_SALARY_SLIP = '[SalarySlip] Update Salary Slip';
export const UPDATE_SALARY_SLIP_COMPLETE = '[SalarySlip] Update Salary Slip Complete';
export const UPDATE_SALARY_SLIP_FAILED = '[SalarySlip] Update Salary Slip Failed';

export const DELETE_SALARY_SLIP = '[SalarySlip] Delete Salary Slip';
export const DELETE_SALARY_SLIP_COMPLETE = '[SalarySlip] Delete Salary Slip Complete';
export const DELETE_SALARY_SLIP_FAILED = '[SalarySlip] Delete Salary Slip Failed';

export const PAY_SALARY_SLIP = '[SalarySlip] Pay Salary Slip';
export const PAY_SALARY_SLIP_COMPLETE = '[SalarySlip] Pay Salary Slip Complete';
export const PAY_SALARY_SLIP_FAILED = '[SalarySlip] Pay Salary Slip Failed';

export const CONFIRM_SALARY_SLIP = '[SalarySlip] Confirm Salary Slip';
export const CONFIRM_SALARY_SLIP_COMPLETE = '[SalarySlip] Confirm Salary Slip Complete';
export const CONFIRM_SALARY_SLIP_FAILED = '[SalarySlip] Confirm Salary Slip Failed';

export const OPEN_SALARY_SLIP_MODAL = '[SalarySlip] Open Salary Slip Modal';
export const CLOSE_SALARY_SLIP_MODAL = '[SalarySlip] Close Salary Slip Modal';

export const CLEAR_SALARY_SLIP_STATE_ACTION = "[SalarySlip] Clear SalarySlip State Action";

export class FetchAllSalarySlipsAction implements Action {
  readonly type = FETCH_ALL_SALARY_SLIPS;
}

export class FetchAllSalarySlipsCompleteAction implements Action {
  readonly type = FETCH_ALL_SALARY_SLIPS_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchAllSalarySlipsFailedAction implements Action {
  readonly type = FETCH_ALL_SALARY_SLIPS_FAILED;
  constructor(public payload?: any) { }
}

export class FetchSalarySlipAction implements Action {
  readonly type = FETCH_SALARY_SLIP;
  constructor(public payload: any) { }
}

export class FetchSalarySlipCompleteAction implements Action {
  readonly type = FETCH_SALARY_SLIP_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchSalarySlipFailedAction implements Action {
  readonly type = FETCH_SALARY_SLIP_FAILED;
  constructor(public payload?: any) { }
}

export class FilterSalarySlipsAction implements Action {
  readonly type = FILTER_SALARY_SLIPS;
  constructor(public payload: any) { }
}

export class FilterSalarySlipsCompleteAction implements Action {
  readonly type = FILTER_SALARY_SLIPS_COMPLETE;
  constructor(public payload: any) { }
}

export class FilterSalarySlipsFailedAction implements Action {
  readonly type = FILTER_SALARY_SLIPS_FAILED;
  constructor(public payload?: any) {
    swal("There was an error.", payload, "error");
  }
}

export class CreateSalarySlipAction implements Action {
  readonly type = CREATE_SALARY_SLIP;
  constructor(public payload:any) { }
}

export class CreateSalarySlipCompleteAction implements Action {
  readonly type = CREATE_SALARY_SLIP_COMPLETE;
  constructor(public payload:any) {
    swal({
      title: "Salary slip created!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class CreateSalarySlipFailedAction implements Action {
  readonly type = CREATE_SALARY_SLIP_FAILED;
  constructor(public payload:any) {
    swal("There was an error.", payload, "error");
  }
}

export class UpdateSalarySlipAction implements Action {
  readonly type = UPDATE_SALARY_SLIP;
  constructor(public payload: any) { }
}

export class UpdateSalarySlipCompleteAction implements Action {
  readonly type = UPDATE_SALARY_SLIP_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Salary slip updated!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class UpdateSalarySlipFailedAction implements Action {
  readonly type = UPDATE_SALARY_SLIP_FAILED;
  constructor(public payload?: any) {
    swal("There was an error.", payload, "error");
  }
}

export class DeleteSalarySlipAction implements Action {
  readonly type = DELETE_SALARY_SLIP;
  constructor(public payload: any) { }
}

export class DeleteSalarySlipCompleteAction implements Action {
  readonly type = DELETE_SALARY_SLIP_COMPLETE;
  constructor(public payload: any) {
    swal({
      title: "Salary slip deleted!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class DeleteSalarySlipFailedAction implements Action {
  readonly type = DELETE_SALARY_SLIP_FAILED;
  constructor(public payload?: any) {
    swal("There was an error.", payload, "error");
  }
}

export class ConfirmSalarySlipAction implements Action {
  readonly type = CONFIRM_SALARY_SLIP;
  constructor(public payload:any) { }
}

export class ConfirmSalarySlipCompleteAction implements Action {
  readonly type = CONFIRM_SALARY_SLIP_COMPLETE;
  constructor(public payload:any) {
    swal({
      title: "Salary slip confirmed!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class ConfirmSalarySlipFailedAction implements Action {
  readonly type = CONFIRM_SALARY_SLIP_FAILED;
  constructor(public payload:any) {
    swal("There was an error.", payload, "error");
  }
}

export class PaySalarySlipAction implements Action {
  readonly type = PAY_SALARY_SLIP;
  constructor(public payload:any) { }
}

export class PaySalarySlipCompleteAction implements Action {
  readonly type = PAY_SALARY_SLIP_COMPLETE;
  constructor(public payload:any) {
    swal({
      title: "Salary slip was cleared!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class PaySalarySlipFailedAction implements Action {
  readonly type = PAY_SALARY_SLIP_FAILED;
  constructor(public payload:any) {
    swal("There was an error.", payload, "error");
  }
}

export class OpenSalarySlipModalAction implements Action {
  readonly type = OPEN_SALARY_SLIP_MODAL;
}

export class CloseSalarySlipModalAction implements Action {
  readonly type = CLOSE_SALARY_SLIP_MODAL;
}

export class ClearSalarySlipStateAction implements Action {
  readonly type = CLEAR_SALARY_SLIP_STATE_ACTION;
}

export type Actions =
  FetchAllSalarySlipsAction
  | FetchAllSalarySlipsCompleteAction
  | FetchAllSalarySlipsFailedAction
  | FetchSalarySlipAction
  | FetchSalarySlipCompleteAction
  | FetchSalarySlipFailedAction
  | FilterSalarySlipsAction
  | FilterSalarySlipsCompleteAction
  | FilterSalarySlipsFailedAction
  | CreateSalarySlipAction
  | CreateSalarySlipCompleteAction
  | CreateSalarySlipFailedAction
  | UpdateSalarySlipAction
  | UpdateSalarySlipCompleteAction
  | UpdateSalarySlipFailedAction
  | DeleteSalarySlipAction
  | DeleteSalarySlipCompleteAction
  | DeleteSalarySlipFailedAction
  | ConfirmSalarySlipAction
  | ConfirmSalarySlipCompleteAction
  | ConfirmSalarySlipFailedAction
  | PaySalarySlipAction
  | PaySalarySlipCompleteAction
  | PaySalarySlipFailedAction
  | OpenSalarySlipModalAction
  | CloseSalarySlipModalAction
  | ClearSalarySlipStateAction;