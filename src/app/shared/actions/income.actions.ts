import { Action } from '@ngrx/store';
import swal from 'sweetalert2';

export const FETCH_ALL_INCOMES = '[Income] Fetch All Incomes';
export const FETCH_ALL_INCOMES_COMPLETE = '[Income] Fetch All Incomes Complete';
export const FETCH_ALL_INCOMES_FAILED = '[Income] Fetch All Incomes Failed';

export const FETCH_INCOME = '[Income] Fetch Income';
export const FETCH_INCOME_COMPLETE = '[Income] Fetch Income Complete';
export const FETCH_INCOME_FAILED = '[Income] Fetch Income Failed';

export const CREATE_INCOME = '[Income] Create Income';
export const CREATE_INCOME_COMPLETE = '[Income] Create Income Complete';
export const CREATE_INCOME_FAILED = '[Income] Create Income Failed';

export const UPDATE_INCOME = '[Income] Update Income';
export const UPDATE_INCOME_COMPLETE = '[Income] Update Income Complete';
export const UPDATE_INCOME_FAILED = '[Income] Update Income Failed';

export const DELETE_INCOME = '[Income] Delete Income';
export const DELETE_INCOME_COMPLETE = '[Income] Delete Income Complete';
export const DELETE_INCOME_FAILED = '[Income] Delete Income Failed';

export const CLEAR_INCOME_STATE_ACTION = "[Income] Clear Income State Action";

export class FetchAllIncomesAction implements Action {
  readonly type = FETCH_ALL_INCOMES;
  constructor(public payload?: any) { }
}

export class FetchAllIncomesCompleteAction implements Action {
  readonly type = FETCH_ALL_INCOMES_COMPLETE;
  constructor(public payload: any) { }
}

export class FetchAllIncomesFailedAction implements Action {
  readonly type = FETCH_ALL_INCOMES_FAILED;
  constructor(public payload?: any) { }
}

export class FetchIncomeAction implements Action {
  readonly type = FETCH_INCOME;
  constructor(public payload?: any) { }
}

export class FetchIncomeCompleteAction implements Action {
  readonly type = FETCH_INCOME_COMPLETE;
  constructor(public payload?: any) { }
}

export class FetchIncomeFailedAction implements Action {
  readonly type = FETCH_INCOME_FAILED;
  constructor(public payload?: any) { }
}

export class CreateIncomeAction implements Action {
  readonly type = CREATE_INCOME;
  constructor(public payload:any) { }
}

export class CreateIncomeCompleteAction implements Action {
  readonly type = CREATE_INCOME_COMPLETE;
  constructor(public payload:any) {
    swal("Great!", "Income was recorded.","success");
  }
}

export class CreateIncomeFailedAction implements Action {
  readonly type = CREATE_INCOME_FAILED;
  constructor(public payload:any) {
    swal("There was an error.", payload, "error");
  }
}

export class UpdateIncomeAction implements Action {
  readonly type = UPDATE_INCOME;
  constructor(public payload: any) { }
}

export class UpdateIncomeCompleteAction implements Action {
  readonly type = UPDATE_INCOME_COMPLETE;
  constructor(public payload: any) {
    swal("Great!", "Income was updated.","success");
  }
}

export class UpdateIncomeFailedAction implements Action {
  readonly type = UPDATE_INCOME_FAILED;
  constructor(public payload?: any) {
    swal("There was an error.", payload, "error");
  }
}

export class DeleteIncomeAction implements Action {
  readonly type = DELETE_INCOME;
  constructor(public payload: any) { }
}

export class DeleteIncomeCompleteAction implements Action {
  readonly type = DELETE_INCOME_COMPLETE;
  constructor(public payload: any) {
    swal("Great!", "Income was deleted.","success");
  }
}

export class DeleteIncomeFailedAction implements Action {
  readonly type = DELETE_INCOME_FAILED;
  constructor(public payload?: any) {
    swal("There was an error.", payload, "error");
  }
}

export class ClearIncomeStateAction implements Action {
  readonly type = CLEAR_INCOME_STATE_ACTION;
}

export type Actions =
  FetchAllIncomesAction
  | FetchAllIncomesCompleteAction
  | FetchAllIncomesFailedAction
  | FetchIncomeAction
  | FetchIncomeCompleteAction
  | FetchIncomeFailedAction
  | CreateIncomeAction
  | CreateIncomeCompleteAction
  | CreateIncomeFailedAction
  | UpdateIncomeAction
  | UpdateIncomeCompleteAction
  | UpdateIncomeFailedAction
  | DeleteIncomeAction
  | DeleteIncomeCompleteAction
  | DeleteIncomeFailedAction
  | ClearIncomeStateAction;