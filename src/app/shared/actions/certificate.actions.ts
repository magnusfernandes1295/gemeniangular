import { Action } from '@ngrx/store';
import swal from 'sweetalert2';

export const CREATE_CERTIFICATE = '[Certificate] Create Certificate';
export const CREATE_CERTIFICATE_COMPLETE = '[Certificate] Create Certificate Complete';
export const CREATE_CERTIFICATE_FAILED = '[Certificate] Create Certificate Failed';

export const FETCH_ALL_CERTIFICATES = '[Certificate] Fetch All Certificates';
export const FETCH_ALL_CERTIFICATES_COMPLETE = '[Certificate] Fetch All Certificates Complete';
export const FETCH_ALL_CERTIFICATES_FAILED = '[Certificate] Fetch All Certificates Failed';

export const FETCH_CERTIFICATE = '[Certificate] Fetch Certificate';
export const FETCH_CERTIFICATE_COMPLETE = '[Certificate] Fetch Certificate Complete';
export const FETCH_CERTIFICATE_FAILED = '[Certificate] Fetch Certificate Failed';

export const FILTER_CERTIFICATES = '[Certificate] Filter Certificates';
export const FILTER_CERTIFICATES_COMPLETE = '[Certificate] Filter Certificates Complete';
export const FILTER_CERTIFICATES_FAILED = '[Certificate] Filter Certificates Failed';

export const DELETE_CERTIFICATE = '[Certificate] Delete Certificate';
export const DELETE_CERTIFICATE_COMPLETE = '[Certificate] Delete Certificate Complete';
export const DELETE_CERTIFICATE_FAILED = '[Certificate] Delete Certificate Failed';

export const UPDATE_CERTIFICATE = '[Certificate] Update Certificate';
export const UPDATE_CERTIFICATE_COMPLETE = '[Certificate] Update Certificate Complete';
export const UPDATE_CERTIFICATE_FAILED = '[Certificate] Update Certificate Failed';

export const ISSUE_CERTIFICATE = '[Certificate] Issue Certificate';
export const ISSUE_CERTIFICATE_COMPLETE = '[Certificate] Issue Certificate Complete';
export const ISSUE_CERTIFICATE_FAILED = '[Certificate] Issue Certificate Failed';

export const RENEW_CERTIFICATE = '[Certificate] Renew Certificate';
export const RENEW_CERTIFICATE_COMPLETE = '[Certificate] Renew Certificate Complete';
export const RENEW_CERTIFICATE_FAILED = '[Certificate] Renew Certificate Failed';

export const OPEN_CERTIFICATE_MODAL_ACTION = '[Certificate] Open Certificate Modal Action';
export const CLOSE_CERTIFICATE_MODAL_ACTION = '[Certificate] Close Certificate Modal Action';

export const CLEAR_CERTIFICATE_STATE_ACTION = "[Certificate] Clear Certificate State Action";

export class CreateCertificateAction implements Action {
  readonly type = CREATE_CERTIFICATE;
  constructor(public payload:any) { }
}

export class CreateCertificateCompleteAction implements Action {
  readonly type = CREATE_CERTIFICATE_COMPLETE;
  constructor(public payload:any) {
    swal({
      title: "Certificate Created!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class CreateCertificateFailedAction implements Action {
  readonly type = CREATE_CERTIFICATE_FAILED;
  constructor(public payload:any) {
    swal("There was an error.", payload, "error");
  }
}

export class FetchAllCertificatesAction implements Action {
  readonly type = FETCH_ALL_CERTIFICATES;
  constructor(public payload?:any) { }
}

export class FetchAllCertificatesCompleteAction implements Action {
  readonly type = FETCH_ALL_CERTIFICATES_COMPLETE;
  constructor(public payload?:any) { }
}

export class FetchAllCertificatesFailedAction implements Action {
  readonly type = FETCH_ALL_CERTIFICATES_FAILED;
  constructor(public payload?:any) { }
}

export class FetchCertificateAction implements Action {
  readonly type = FETCH_CERTIFICATE;
  constructor(public payload:any) { }
}

export class FetchCertificateCompleteAction implements Action {
  readonly type = FETCH_CERTIFICATE_COMPLETE;
  constructor(public payload:any) { }
}

export class FetchCertificateFailedAction implements Action {
  readonly type = FETCH_CERTIFICATE_FAILED;
  constructor(public payload:any) { }
}

export class FilterCertificatesAction implements Action {
  readonly type = FILTER_CERTIFICATES;
  constructor(public payload:any) { }
}

export class FilterCertificatesCompleteAction implements Action {
  readonly type = FILTER_CERTIFICATES_COMPLETE;
  constructor(public payload:any) { }
}

export class FilterCertificatesFailedAction implements Action {
  readonly type = FILTER_CERTIFICATES_FAILED;
  constructor(public payload:any) { }
}

export class UpdateCertificateAction implements Action {
  readonly type = UPDATE_CERTIFICATE;
  constructor(public payload:any) { }
}

export class UpdateCertificateCompleteAction implements Action {
  readonly type = UPDATE_CERTIFICATE_COMPLETE;
  constructor(public payload:any) {
    swal({
      title: "Certificate Updated!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class UpdateCertificateFailedAction implements Action {
  readonly type = UPDATE_CERTIFICATE_FAILED;
  constructor(public payload:any) {
    swal("There was an error.", payload, "error");
  }
}

export class DeleteCertificateAction implements Action {
  readonly type = DELETE_CERTIFICATE;
  constructor(public payload:any) { }
}

export class DeleteCertificateCompleteAction implements Action {
  readonly type = DELETE_CERTIFICATE_COMPLETE;
  constructor(public payload:any) {
    swal({
      title: "Certificate Deleted!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class DeleteCertificateFailedAction implements Action {
  readonly type = DELETE_CERTIFICATE_FAILED;
  constructor(public payload:any) {
    swal("There was an error.", payload, "error");
  }
}

export class IssueCertificateAction implements Action {
  readonly type = ISSUE_CERTIFICATE;
  constructor(public payload:any) { }
}

export class IssueCertificateCompleteAction implements Action {
  readonly type = ISSUE_CERTIFICATE_COMPLETE;
  constructor(public payload:any) { }
}

export class IssueCertificateFailedAction implements Action {
  readonly type = ISSUE_CERTIFICATE_FAILED;
  constructor(public payload:any) {
    swal("There was an error.", payload, "error");
  }
}

export class RenewCertificateAction implements Action {
  readonly type = RENEW_CERTIFICATE;
  constructor(public payload:any) { }
}

export class RenewCertificateCompleteAction implements Action {
  readonly type = RENEW_CERTIFICATE_COMPLETE;
  constructor(public payload:any) {
    swal({
      title: "Certificate Renewed!",
      type: "success",
      timer: 3000,
      showConfirmButton: false
    });
  }
}

export class RenewCertificateFailedAction implements Action {
  readonly type = RENEW_CERTIFICATE_FAILED;
  constructor(public payload:any) {
    swal("There was an error.", payload, "error");
  }
}

export class OpenCertificateModalAction implements Action {
  readonly type = OPEN_CERTIFICATE_MODAL_ACTION;
}

export class CloseCertificateModalAction implements Action {
  readonly type = CLOSE_CERTIFICATE_MODAL_ACTION;
}

export class ClearCertificateStateAction implements Action {
  readonly type = CLEAR_CERTIFICATE_STATE_ACTION;
}

export type Actions =
  CreateCertificateAction
  | CreateCertificateCompleteAction
  | CreateCertificateFailedAction
  | FetchAllCertificatesAction
  | FetchAllCertificatesCompleteAction
  | FetchAllCertificatesFailedAction
  | FetchCertificateAction
  | FetchCertificateCompleteAction
  | FetchCertificateFailedAction
  | FilterCertificatesAction
  | FilterCertificatesCompleteAction
  | FilterCertificatesFailedAction
  | UpdateCertificateAction
  | UpdateCertificateCompleteAction
  | UpdateCertificateFailedAction
  | DeleteCertificateAction
  | DeleteCertificateCompleteAction
  | DeleteCertificateFailedAction
  | IssueCertificateAction
  | IssueCertificateCompleteAction
  | IssueCertificateFailedAction
  | RenewCertificateAction
  | RenewCertificateCompleteAction
  | RenewCertificateFailedAction
  | OpenCertificateModalAction
  | CloseCertificateModalAction
  | ClearCertificateStateAction;