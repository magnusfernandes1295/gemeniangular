import { Observable } from 'rxjs/Observable';
import { Angular2TokenService } from 'angular2-token';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class DashboardService {

  public dashboardData: Subject<any> = new Subject<any>();

  constructor(
    private _http: HttpClient,
    private _tokenService: Angular2TokenService
  ) { }

  getDashboardData(): Observable<any> {
    this._tokenService.get('dashboard_data').subscribe((data) => {
      let response: any = data;
      if (response.json() != null) this.dashboardData.next(response.json().message);
    });
    return this.dashboardData.asObservable();
  }

}
