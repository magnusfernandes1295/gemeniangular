import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Angular2TokenService } from 'angular2-token';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/catch';

@Injectable()
export class DeviceService {
  public exists: Subject<boolean> = new Subject();

  constructor(
    private _tokenService: Angular2TokenService
  ) { }

  checkIfDeviceExists(sld_number: string) : Observable<any> {
    this._tokenService.get(`devices/exists/${sld_number}`).subscribe(res => {
      this.exists.next(true);
    }, err => {
      this.exists.next(false);      
    });
    return this.exists.asObservable();
  }

}