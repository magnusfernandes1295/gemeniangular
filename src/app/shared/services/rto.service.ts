import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { rto } from '../../../assets/js/rto';

@Injectable()
export class RtoService {

  constructor(private _http: HttpClient) { }

  getStates() {
    let states: any[] = [];
    rto.states.map(state => {
      states.push({
        code: state.code,
        name: state.name
      });
    });
    return states;
  }

  getRto(name: string) {
    if (name) {
      return rto.states.find(state => state.name == name).offices.map(office => new Office(office));
    }
  }
}

class Office {
  public id: string;
  public office: string;
  public full: string;

  constructor(data: any) {
    this.id = data.id;
    this.office = data.office;
    this.full = data.id + ": " + data.office;
  }
}
