import { PurchaseOrder, PageData } from "../models";

import * as purchaseOrderActions from "../actions/purchase-order.actions";

export interface State {
  allPurchaseOrders: PurchaseOrder[];
  currentPurchaseOrder: PurchaseOrder;
  currentPurchaseOrderId: number;
  showPOModal: boolean;
  showDetailPO: boolean;
  showConfirmPO: boolean;
  showDispatchPO: boolean;
  showClosePO: boolean;
  currentPurchaseOrderPageStatus: PageData;
}

const initialState: State = {
  allPurchaseOrders: [],
  currentPurchaseOrder: new PurchaseOrder({}),
  currentPurchaseOrderId: null,
  showPOModal: false,
  showDetailPO: false,
  showConfirmPO: false,
  showDispatchPO: false,
  showClosePO: false,
  currentPurchaseOrderPageStatus: new PageData({})
};

export function reducer(state = initialState, action: purchaseOrderActions.Actions): State {
  switch (action.type) {
    case purchaseOrderActions.CREATE_PURCHASE_ORDER_COMPLETE:
      return Object.assign({}, state, {
        allPurchaseOrders: [new PurchaseOrder(action.payload), ...state.allPurchaseOrders]
      });
    case purchaseOrderActions.FETCH_PURCHASE_ORDER:
      return Object.assign({}, state, {
        currentPurchaseOrder: null
      });
    case purchaseOrderActions.FILTER_PURCHASE_ORDER_COMPLETE:
      return Object.assign({}, state, {
        allPurchaseOrders: [...action.payload.data.map(purchaseOrders => new PurchaseOrder(purchaseOrders))],
        currentPurchaseOrderPageStatus: new PageData({
          total: action.payload.total,
          per_page: action.payload.per_page,
        })
      });
    case purchaseOrderActions.FETCH_PURCHASE_ORDER_COMPLETE:
      return Object.assign({}, state, {
        currentPurchaseOrder: new PurchaseOrder(action.payload)
      });
    case purchaseOrderActions.FETCH_ALL_PURCHASE_ORDERS_COMPLETE:
      return Object.assign({}, state, {
        allPurchaseOrders: [...action.payload.map(purchaseOrders => new PurchaseOrder(purchaseOrders))]
      });
    case purchaseOrderActions.UPDATE_PURCHASE_ORDER_COMPLETE:
      return Object.assign({}, state, {
        allPurchaseOrders: state.allPurchaseOrders.map(po => po.id === action.payload.id ? new PurchaseOrder(action.payload) : po)
      });
    case purchaseOrderActions.DELETE_PURCHASE_ORDER_COMPLETE:
      return Object.assign({}, state, {
        allPurchaseOrders: state.allPurchaseOrders.filter(po => po.id != state.currentPurchaseOrderId)
      });
    case purchaseOrderActions.OPEN_PURCHASE_ORDER_COMPLETE:
      return Object.assign({}, state, {
        allPurchaseOrders: state.allPurchaseOrders.map(po => po.id == action.payload.id ? new PurchaseOrder(action.payload) : po)
      });
    case purchaseOrderActions.CONFIRM_PURCHASE_ORDER_COMPLETE:
      return Object.assign({}, state, {
        allPurchaseOrders: state.allPurchaseOrders.map(po => po.id == action.payload.id ? new PurchaseOrder(action.payload) : po)
      });
    case purchaseOrderActions.DISPATCH_PURCHASE_ORDER_COMPLETE:
      return Object.assign({}, state, {
        allPurchaseOrders: state.allPurchaseOrders.map(po => po.id == action.payload.id ? new PurchaseOrder(action.payload) : po)
      });
    case purchaseOrderActions.CLOSE_PURCHASE_ORDER_COMPLETE:
      return Object.assign({}, state, {
        allPurchaseOrders: state.allPurchaseOrders.map(po => po.id == action.payload.id ? new PurchaseOrder(action.payload) : po)
      });

    case purchaseOrderActions.OPEN_PURCHASE_ORDER_MODAL:
      return Object.assign({}, state, {
        showPOModal: true
      });
    case purchaseOrderActions.CLOSE_PURCHASE_ORDER_MODAL:
      return Object.assign({}, state, {
        showPOModal: false,
        showClosePO: false,
        showDetailPO: false,
        showConfirmPO: false,
        showDispatchPO: false,
        currentPurchaseOrder: new PurchaseOrder({})
      });
    case purchaseOrderActions.OPEN_PURCHASE_ORDER_DETAIL_MODAL:
      return Object.assign({}, state, {
        showDetailPO: true
      });
    case purchaseOrderActions.OPEN_PURCHASE_ORDER_CONFIRM_MODAL:
      return Object.assign({}, state, {
        showConfirmPO: true
      });
    case purchaseOrderActions.OPEN_PURCHASE_ORDER_DISPATCH_MODAL:
      return Object.assign({}, state, {
        currentPurchaseOrder: state.allPurchaseOrders.find(po => po.id == action.payload),
        showDispatchPO: true
      });
    case purchaseOrderActions.OPEN_PURCHASE_ORDER_CLOSE_MODAL:
      return Object.assign({}, state, {
        showClosePO: true
      });

    case purchaseOrderActions.FETCH_PURCHASE_ORDER:
      return Object.assign({}, state, {
        currentPurchaseOrder: new PurchaseOrder({})
      });
    case purchaseOrderActions.DELETE_PURCHASE_ORDER:
      return Object.assign({}, state, {
        currentPurchaseOrderId: action.payload
      });
    case purchaseOrderActions.CLEAR_PURCHASE_ORDER_STATE_ACTION:
      return Object.assign({}, state, {
        allPurchaseOrders: [],
        currentPurchaseOrder: new PurchaseOrder({}),
        currentPurchaseOrderId: null,
        showPOModal: false,
        showDetailPO: false,  
        showConfirmPO: false,
        showDispatchPO: false,
        showClosePO: false
      });

    default:
      return state;
  }
}
