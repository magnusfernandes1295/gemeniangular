import { Certificate, PageData } from "../models";

import * as certificateActions from "../actions/certificate.actions";

export interface State {
  allCertificates: Certificate[];
  certificateModal: boolean;
  certificateId: number;
  currentCertificate: Certificate;
  currentCertificatePageStatus: PageData;
}

const initialState: State = {
  allCertificates: [],
  certificateModal: false,
  certificateId: null,
  currentCertificate: null,
  currentCertificatePageStatus: new PageData({})
};

export function reducer(state = initialState, action: certificateActions.Actions): State {
  let certificates: Certificate[] = [];
  switch (action.type) {
    case certificateActions.FETCH_ALL_CERTIFICATES_COMPLETE:
      certificates = action.payload.map(certificate => new Certificate(certificate))
      return Object.assign({}, state, {
        allCertificates: [...certificates]
      });
    case certificateActions.FETCH_CERTIFICATE_COMPLETE:
      certificates = state.allCertificates.map(certificate => certificate.id === action.payload.id ? new Certificate(action.payload) : certificate);
      return Object.assign({}, state, {
        currentCertificate: new Certificate(action.payload),
        allCertificates: [...certificates]
      });
    case certificateActions.FILTER_CERTIFICATES_COMPLETE:
      certificates = action.payload.data.map(certificate => new Certificate(certificate))
      return Object.assign({}, state, {
        allCertificates: [...certificates],
        currentCertificatePageStatus: new PageData({
          total: action.payload.total,
          per_page: action.payload.per_page,
        })
      });
    case certificateActions.UPDATE_CERTIFICATE_COMPLETE:
      certificates = state.allCertificates.map(certificate => certificate.id === action.payload.id ? new Certificate(action.payload) : certificate);
      return Object.assign({}, state, {
        allCertificates: [...certificates]
      });
    case certificateActions.ISSUE_CERTIFICATE_COMPLETE:
      certificates = state.allCertificates.map(certificate => certificate.id === action.payload.id ? new Certificate(action.payload) : certificate);
      return Object.assign({}, state, {
        allCertificates: [...certificates],
        currentCertificate: new Certificate(action.payload)
      });
    case certificateActions.DELETE_CERTIFICATE:
      return Object.assign({}, state, {
        certificateId: action.payload
      });
    case certificateActions.DELETE_CERTIFICATE_COMPLETE:
      certificates = state.allCertificates.filter(certificate => certificate.id != state.certificateId);
      return Object.assign({}, state, {
        allCertificates: [...certificates]
      });
    case certificateActions.ISSUE_CERTIFICATE_COMPLETE:
      certificates = state.allCertificates.filter(certificate => certificate.id == action.payload.id ? new Certificate(action.payload) : certificate)
      return Object.assign({}, state, {
        allCertificates: [...certificates],
        currentCertificate: new Certificate(action.payload)
      });
    case certificateActions.RENEW_CERTIFICATE_COMPLETE:
      certificates = state.allCertificates.filter(certificate => certificate.id == action.payload.id ? new Certificate(action.payload) : certificate)
      return Object.assign({}, state, {
        allCertificates: [...certificates]
      });
    case certificateActions.OPEN_CERTIFICATE_MODAL_ACTION:
      return Object.assign({}, state, {
        certificateModal: true
      });
    case certificateActions.CLOSE_CERTIFICATE_MODAL_ACTION:
      return Object.assign({}, state, {
        certificateModal: false
      });
    case certificateActions.CREATE_CERTIFICATE_COMPLETE:
      return Object.assign({}, state, {
        allCertificates: [...state.allCertificates, new Certificate(action.payload)]
      });
    case certificateActions.CLEAR_CERTIFICATE_STATE_ACTION:
      return Object.assign({}, state, {
        allCertificates: [],
        certificateModal: false,
        currentCertificate: null
      });

    default:
      return state;
  }
}
