import { SalarySlip, PageData } from "../models";
import * as salarySlipActions from "../actions/salary-slip.actions";

export interface State {
  allSlips: SalarySlip[];
  showSlipModal: boolean;
  currentSlipId: number;
  currentSalarySlipPageStatus: PageData;
}

const initialState: State = {
  allSlips: [],
  showSlipModal: false,
  currentSlipId: null,
  currentSalarySlipPageStatus: new PageData({})
};

export function reducer(state = initialState, action: salarySlipActions.Actions): State {
  let salarySlips: SalarySlip[] = [];
  switch (action.type) {
    case salarySlipActions.FETCH_ALL_SALARY_SLIPS_COMPLETE:
      salarySlips = action.payload.map(salarySlip => new SalarySlip(salarySlip));
      return Object.assign({}, state, {
        allSlips: [...salarySlips]
      });
    case salarySlipActions.FETCH_SALARY_SLIP_COMPLETE:
      return Object.assign({}, state, {
        allSlips: state.allSlips.map(slip => slip.id === action.payload.id ? new SalarySlip(action.payload) : slip)
      });
    case salarySlipActions.FILTER_SALARY_SLIPS_COMPLETE:
      salarySlips = action.payload.data.map(salarySlip => new SalarySlip(salarySlip));
      return Object.assign({}, state, {
        allSlips: [...salarySlips],
        currentSalarySlipPageStatus: new PageData({
          total: action.payload.total,
          per_page: action.payload.per_page,
        })
      });
    case salarySlipActions.CREATE_SALARY_SLIP_COMPLETE:
      return Object.assign({}, state, {
        allSlips: [...state.allSlips, new SalarySlip(action.payload)]
      });
    case salarySlipActions.UPDATE_SALARY_SLIP_COMPLETE:
      return Object.assign({}, state, {
        allSlips: state.allSlips.map(slip => slip.id === action.payload.id ? new SalarySlip(action.payload) : slip)
      });
    case salarySlipActions.DELETE_SALARY_SLIP_COMPLETE:
      return Object.assign({}, state, {
        allSlips: state.allSlips.filter(slip => slip.id != state.currentSlipId)
      });
    case salarySlipActions.CONFIRM_SALARY_SLIP_COMPLETE:
      return Object.assign({}, state, {
        allSlips: state.allSlips.map(slip => slip.id == action.payload.id ? new SalarySlip(action.payload) : slip)
      });
    case salarySlipActions.PAY_SALARY_SLIP_COMPLETE:
      return Object.assign({}, state, {
        allSlips: state.allSlips.map(slip => slip.id == action.payload.id ? new SalarySlip(action.payload) : slip)
      });
    case salarySlipActions.OPEN_SALARY_SLIP_MODAL:
      return Object.assign({}, state, {
        showSlipModal: true
      });
    case salarySlipActions.CLOSE_SALARY_SLIP_MODAL:
      return Object.assign({}, state, {
        showSlipModal: false
      });
    case salarySlipActions.DELETE_SALARY_SLIP:
      return Object.assign({}, state, {
        currentSlipId: action.payload
      });
    case salarySlipActions.CLEAR_SALARY_SLIP_STATE_ACTION:
      return Object.assign({}, state, {
        allSlips: [],
        showSlipModal: false,
        currentSlipId: null
      });

    default:
      return state;
  }
}
