import { Vehicle } from '../models';

import * as vehicleActions from '../actions/vehicle.actions';

export interface State {
  allVehicles: Vehicle[];
  currentVehicleId: number;
  currentVehicleIcatId: number;
  showVehicleModal: boolean;
}

const initialState: State = {
  allVehicles: [],
  currentVehicleId: null,
  currentVehicleIcatId: null,
  showVehicleModal: false
};

export function reducer(state = initialState, action: vehicleActions.Actions): State {
  let vehicles :Vehicle[] = [];
  switch (action.type) {

    case vehicleActions.FETCH_ALL_VEHICLES_COMPLETE:
      vehicles = action.payload.map(vehicle => new Vehicle(vehicle))
      return Object.assign({}, state, {
        allVehicles: [...vehicles]
      });
    case vehicleActions.CREATE_VEHICLE_COMPLETE:
      return Object.assign({}, state, {
        allVehicles: [...state.allVehicles, new Vehicle(action.payload)]
      });
    case vehicleActions.UPDATE_VEHICLE_COMPLETE:
      return Object.assign({}, state, {
        allVehicles: [...state.allVehicles.map(vehicle => vehicle.id === action.payload.id ? new Vehicle(action.payload) : vehicle)]
      });
    case vehicleActions.DELETE_VEHICLE_COMPLETE:
      vehicles = state.allVehicles.filter(vehicle => vehicle.id != state.currentVehicleId);
      return Object.assign({}, state, {
        allVehicles: vehicles
      });
    case vehicleActions.OPEN_VEHICLE_MODAL:
      return Object.assign({}, state, {
        showVehicleModal: true
      });
    case vehicleActions.CLOSE_VEHICLE_MODAL:
      return Object.assign({}, state, {
        showVehicleModal: false
      });
    
    case vehicleActions.DELETE_VEHICLE:
      return Object.assign({}, state, {
        currentVehicleId: action.payload
      });
    case vehicleActions.DELETE_VEHICLE_ICAT:
      return Object.assign({}, state, {
        currentVehicleId: action.payload.vehicle_id,
        currentVehicleIcatId: action.payload.icat_id
      });
    case vehicleActions.DELETE_VEHICLE_ICAT_COMPLETE:
      let vehicle: Vehicle = state.allVehicles.find(vehicle => vehicle.id == state.currentVehicleId);
      vehicle.icats = vehicle.icats.filter(icat => icat.id !== state.currentVehicleIcatId);
      return Object.assign({}, state, {
        allVehicles: [...state.allVehicles.map(data => data.id === vehicle.id ? vehicle : data)]
      });
    case vehicleActions.UPDATE_VEHICLE_ICAT_COMPLETE:
      return Object.assign({}, state, {
        allVehicles: [...state.allVehicles.map(vehicle => vehicle.id === action.payload.id ? new Vehicle(action.payload) : vehicle)]
      });
    case vehicleActions.CLEAR_VEHICLE_STATE_ACTION:
      return Object.assign({}, state, {
        allVehicles: [],
        currentVehicleId: null,
        showVehicleModal: false
      });
    
    default:
      return state;
  }
}
