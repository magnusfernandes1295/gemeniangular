import { ReceiveNote, PageData } from "../models";

import * as receiveNoteActions from "../actions/receive-note.actions";

export interface State {
  allReceiveNotes: ReceiveNote[];
  currentReceiveNote: ReceiveNote;
  currentID: number
  showNoteModal: boolean;
  showNoteDetailModal: boolean;
  currentReceiveNotePageStatus: PageData;
}

const initialState: State = {
  allReceiveNotes: [],
  currentReceiveNote: new ReceiveNote({}),
  currentID: null,
  showNoteModal: false,
  showNoteDetailModal: false,
  currentReceiveNotePageStatus: new PageData({})
};

export function reducer(state = initialState, action: receiveNoteActions.Actions): State {
  let receiveNotes: ReceiveNote[] = [];
  switch (action.type) {
    case receiveNoteActions.CREATE_NEW_RECEIVE_NOTE_COMPLETE:
      return Object.assign({}, state, {
        allReceiveNotes: [...state.allReceiveNotes, new ReceiveNote(action.payload)]
      });
    case receiveNoteActions.UPDATE_RECEIVE_NOTE_COMPLETE:
      receiveNotes = state.allReceiveNotes.map(receiveNote => receiveNote.id === action.payload.id ? new ReceiveNote(action.payload) : receiveNote);
      return Object.assign({}, state, {
        allReceiveNotes: [...receiveNotes]
      });
    case receiveNoteActions.DELETE_RECEIVE_NOTE_COMPLETE:
      receiveNotes = state.allReceiveNotes.filter(receiveNote => receiveNote.id === state.currentID ? null : receiveNote);
      return Object.assign({}, state, {
        allReceiveNotes: [...receiveNotes]
      });

    case receiveNoteActions.CONFIRM_RECEIVE_NOTE_COMPLETE:
      receiveNotes = state.allReceiveNotes.map(receiveNote => receiveNote.id === action.payload.id ? new ReceiveNote(action.payload) : receiveNote);
      return Object.assign({}, state, {
        allReceiveNotes: [...receiveNotes]
      });

    case receiveNoteActions.FETCH_ALL_RECEIVE_NOTE_COMPLETE:
      receiveNotes = action.payload.map(receiveNote => new ReceiveNote(receiveNote));
      return Object.assign({}, state, {
        allReceiveNotes: [...receiveNotes]
      });
    case receiveNoteActions.FILTER_RECEIVE_NOTE_COMPLETE:
      receiveNotes = action.payload.data.map(receiveNote => new ReceiveNote(receiveNote));
      return Object.assign({}, state, {
        allReceiveNotes: [...receiveNotes],
        currentReceiveNotePageStatus: new PageData({
          total: action.payload.total,
          per_page: action.payload.per_page,
        })
      });
    case receiveNoteActions.FETCH_RECEIVE_NOTE_COMPLETE:
      return Object.assign({}, state, {
        currentReceiveNote: new ReceiveNote(action.payload)
      });

    case receiveNoteActions.OPEN_RECEIVE_NOTE_MODAL:
      return Object.assign({}, state, {
        showNoteModal: true
      });
    case receiveNoteActions.OPEN_RECEIVE_NOTE_DETAIL_MODAL:
      return Object.assign({}, state, {
        showNoteDetailModal: true,
      });
    case receiveNoteActions.CLOSE_RECEIVE_NOTE_MODAL:
      return Object.assign({}, state, {
        showNoteModal: false,
        showNoteDetailModal: false
      });

    case receiveNoteActions.DELETE_RECEIVE_NOTE:
      return Object.assign({}, state, {
        currentID: action.payload
      });
    case receiveNoteActions.CLEAR_RECEIVE_NOTE_STATE_ACTION:
      return Object.assign({}, state, {
        allReceiveNotes: [],
        currentReceiveNote: new ReceiveNote({}),
        currentID: null,
        showRNModal: false,
        showRNDetailModal: false
      });

    default:
      return state;
  }
}
