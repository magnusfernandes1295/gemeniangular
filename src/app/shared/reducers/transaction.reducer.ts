import { Transaction, PageData } from "../models";

import * as transactionActions from "../actions/transaction.actions";

export interface State {
  allTransactions: Transaction[];
  currentTransaction: Transaction;
  showTransactionModal: boolean;
  currentTransactionPageStatus: PageData;
}

const initialState: State = {
  allTransactions: [],
  currentTransaction: new Transaction({}),
  showTransactionModal: false,
  currentTransactionPageStatus: new PageData({})
};

export function reducer(state = initialState, action: transactionActions.Actions): State {
  let transactions: Transaction[] = [];
  switch (action.type) {
    case transactionActions.FETCH_ALL_TRANSACTIONS_COMPLETE:
      transactions = action.payload.data.map(transaction => new Transaction(transaction))
      return Object.assign({}, state, {
        allTransactions: [...transactions],
        currentTransactionPageStatus: new PageData({
          total: action.payload.total,
          per_page: action.payload.per_page,
        })
      });
    case transactionActions.FETCH_TRANSACTION_COMPLETE:
      return Object.assign({}, state, {
        currentTransaction: new Transaction(action.payload)
      });

    case transactionActions.OPEN_TRANSACTION_MODAL_ACTION:
      return Object.assign({}, state, {
        showTransactionModal: true
      });
    case transactionActions.CLOSE_TRANSACTION_MODAL_ACTION:
      return Object.assign({}, state, {
        showTransactionModal: false,
        currentTransaction: new Transaction({})
      });
    case transactionActions.CLEAR_TRANSACTION_STATE_ACTION:
      return Object.assign({}, state, {
        allTransactions: [],
        currentTransaction: new Transaction({}),
        showTransactionModal: false
      });

    default:
      return state;
  }
}
