import { Cost } from "../models";

import * as expenseActions from "../actions/expense.actions";

export interface State {
  allExpenses: Cost[];
  currentExpenseId: number;
  showExpenseModal: boolean;
}

const initialState: State = {
  allExpenses: [],
  currentExpenseId: null,
  showExpenseModal: false
};

export function reducer(state = initialState, action: expenseActions.Actions): State {
  let expenses: Cost[] = [];
  switch (action.type) {
    case expenseActions.FETCH_ALL_EXPENSES_COMPLETE:
      expenses = action.payload.map(expense => new Cost(expense))
      return Object.assign({}, state, {
        allExpenses: [...expenses]
      });
    case expenseActions.FETCH_EXPENSE_COMPLETE:
      expenses = state.allExpenses.map(expense => expense.id == action.payload.id ? new Cost(action.payload) : expense);
      return Object.assign({}, state, {
        allExpenses: [...expenses]
      });
    case expenseActions.CREATE_EXPENSE_COMPLETE:
      return Object.assign({}, state, {
        allExpenses: [...state.allExpenses, new Cost(action.payload)]
      });
    case expenseActions.UPDATE_EXPENSE_COMPLETE:
      expenses = state.allExpenses.map(expense => expense.id == action.payload.id ? new Cost(action.payload) : expense);
      return Object.assign({}, state, {
        allExpenses: [...expenses]
      });
    case expenseActions.DELETE_EXPENSE_COMPLETE:
      expenses = state.allExpenses.map(expense => expense.id == state.currentExpenseId ? null : expense);
      return Object.assign({}, state, {
        allExpenses: [...expenses]
      });
    case expenseActions.OPEN_EXPENSE_MODAL_ACTION:
      return Object.assign({}, state, {
        showExpenseModal: true
      });
    case expenseActions.CLOSE_EXPENSE_MODAL_ACTION:
      return Object.assign({}, state, {
        showExpenseModal: false
      });

    case expenseActions.DELETE_EXPENSE:
      return Object.assign({}, state, {
        currentExpenseId: action.payload
      });
    case expenseActions.CLEAR_EXPENSE_STATE_ACTION:
      return Object.assign({}, state, {
        allExpenses: [],
        currentExpenseId: null,
        showExpenseModal: false
      });

    default:
      return state;
  }
}
