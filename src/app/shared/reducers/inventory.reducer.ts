import { Inventory } from "../models";

import * as inventoryActions from "../actions/inventory.actions";

export interface State {
  allInventory: Inventory[];
  currentInventoryId: number;
  showInventoryModal: boolean;
}

const initialState: State = {
  allInventory: [],
  currentInventoryId: null,  
  showInventoryModal: false
};

export function reducer(state = initialState, action: inventoryActions.Actions): State {
  let inventories: Inventory[] = [];
  switch (action.type) {
    case inventoryActions.CREATE_NEW_INVENTORY_COMPLETE:
      return Object.assign({}, state, {
        allInventory: [...state.allInventory, new Inventory(action.payload)]
      });
    case inventoryActions.UPDATE_INVENTORY_COMPLETE:
      inventories = state.allInventory.map(inventory => inventory.id === action.payload.id ? new Inventory(action.payload) : inventory);
      return Object.assign({}, state, {
        allInventory: [...inventories]
      });
    case inventoryActions.DELETE_INVENTORY_COMPLETE:
      return Object.assign({}, state, {
        allInventory: state.allInventory.filter(inventory => inventory.id != state.currentInventoryId)
      });
    case inventoryActions.FETCH_ALL_INVENTORY_COMPLETE:
      inventories = action.payload.map(inventory => new Inventory(inventory));
      return Object.assign({}, state, {
        allInventory: [...inventories]
      });
    case inventoryActions.FILTER_INVENTORY_COMPLETE:
      inventories = action.payload.map(inventory => new Inventory(inventory));
      return Object.assign({}, state, {
        allInventory: [...inventories]
      });
    case inventoryActions.OPEN_INVENTORY_MODAL_ACTION:
      return Object.assign({}, state, {
        showInventoryModal: true
      });
    case inventoryActions.CLOSE_INVENTORY_MODAL_ACTION:
      return Object.assign({}, state, {
        showInventoryModal: false
      });
    case inventoryActions.DELETE_INVENTORY:
      return Object.assign({}, state, {
        currentInventoryId: action.payload
      });
    case inventoryActions.CLEAR_INVENTORY_STATE_ACTION:
      return Object.assign({}, state, {
        allInventory: [],
        currentInventoryId: null,  
        showInventoryModal: false
      });

    default:
      return state;
  }
}
