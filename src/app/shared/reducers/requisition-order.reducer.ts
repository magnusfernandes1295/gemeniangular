import { RequisitionOrder, PageData } from "../models";

import * as requisitionOrderActions from "../actions/requisition-order.actions";

export interface State {
  allRequisitionOrders: RequisitionOrder[];
  currentRequisitionOrder: RequisitionOrder;
  requisitionOrderId: number;
  reqModal: boolean;
  reqViewModal: boolean;
  currentRequisitionOrderPageStatus: PageData;
}

const initialState: State = {
  allRequisitionOrders: [],
  currentRequisitionOrder: new RequisitionOrder({}),
  requisitionOrderId: null,
  reqModal: false,
  reqViewModal: false,
  currentRequisitionOrderPageStatus: new PageData({})
};

export function reducer(state = initialState, action: requisitionOrderActions.Actions): State {
  let requisitionOrders: RequisitionOrder[] = [];
  switch (action.type) {
    case requisitionOrderActions.CREATE_NEW_REQUISITION_ORDER_COMPLETE:
      return Object.assign({}, state, {
        allRequisitionOrders: [...state.allRequisitionOrders, new RequisitionOrder(action.payload)]
      });
    case requisitionOrderActions.UPDATE_REQUISITION_ORDER_COMPLETE:
      requisitionOrders = state.allRequisitionOrders.map(requisitionOrder => requisitionOrder.id === action.payload.id ? action.payload : requisitionOrder);
      return Object.assign({}, state, {
        allRequisitionOrders: [...requisitionOrders]
      });
    case requisitionOrderActions.DELETE_REQUISITION_ORDER_COMPLETE:
      requisitionOrders = state.allRequisitionOrders.filter(requisitionOrder => requisitionOrder.id === state.requisitionOrderId ? null : requisitionOrder);
      return Object.assign({}, state, {
        allRequisitionOrders: [...requisitionOrders]
      });
    case requisitionOrderActions.OPEN_REQUISITION_ORDER_COMPLETE:
      requisitionOrders = state.allRequisitionOrders.map(requisitionOrder => requisitionOrder.id == action.payload.id ? new RequisitionOrder(action.payload) : requisitionOrder);
      return Object.assign({}, state, {
        allRequisitionOrders: [...requisitionOrders]
      });
    case requisitionOrderActions.CLOSE_REQUISITION_ORDER_COMPLETE:
      requisitionOrders = state.allRequisitionOrders.map(requisitionOrder => requisitionOrder.id == action.payload.id ? new RequisitionOrder(action.payload) : requisitionOrder);
      return Object.assign({}, state, {
        allRequisitionOrders: [...requisitionOrders]
      });
    case requisitionOrderActions.FETCH_ALL_REQUISITION_ORDERS_COMPLETE:
      return Object.assign({}, state, {
        allRequisitionOrders: [...action.payload.map(requisitionOrder => new RequisitionOrder(requisitionOrder))]
      });
    case requisitionOrderActions.FETCH_REQUISITION_ORDER_COMPLETE:
      return Object.assign({}, state, {
        currentRequisitionOrder: new RequisitionOrder(action.payload)
      });
    case requisitionOrderActions.FILTER_REQUISITION_ORDER_COMPLETE:
      return Object.assign({}, state, {
        allRequisitionOrders: [...action.payload.data.map(requisitionOrder => new RequisitionOrder(requisitionOrder))],
        currentRequisitionOrderPageStatus: new PageData({
          total: action.payload.total,
          per_page: action.payload.per_page,
        })
      });

    case requisitionOrderActions.OPEN_REQUISITION_ORDER_MODAL:
      return Object.assign({}, state, {
        reqModal: true
      });
    case requisitionOrderActions.CLOSE_REQUISITION_ORDER_MODAL:
      return Object.assign({}, state, {
        reqModal: false
      });
    case requisitionOrderActions.OPEN_VIEW_REQUISITION_ORDER_MODAL:
      return Object.assign({}, state, {
        reqViewModal: true
      });
    case requisitionOrderActions.CLOSE_VIEW_REQUISITION_ORDER_MODAL:
      return Object.assign({}, state, {
        reqViewModal: false,
        currentRequisitionOrder: new RequisitionOrder({})
      });
    case requisitionOrderActions.DELETE_REQUISITION_ORDER:
      return Object.assign({}, state, {
        requisitionOrderId: action.payload
      });
    case requisitionOrderActions.CLEAR_REQUISITION_ORDER_STATE_ACTION:
      return Object.assign({}, state, {
        allRequisitionOrders: [],
        currentRequisitionOrder: new RequisitionOrder({}),
        requisitionOrderId: null,  
        reqModal: false,
        reqViewModal: false
      });

    default:
      return state;
  }
}
