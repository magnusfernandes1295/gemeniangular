import { Cost } from "../models";

import * as incomeActions from "../actions/income.actions";

export interface State {
  allIncomes: Cost[];
  currentIncomeId: number;
}

const initialState: State = {
  allIncomes: [],
  currentIncomeId: null
};

export function reducer(state = initialState, action: incomeActions.Actions): State {
  let incomes: Cost[] = [];
  switch (action.type) {
    case incomeActions.FETCH_ALL_INCOMES_COMPLETE:
      incomes = action.payload.map(income => new Cost(income))
      return Object.assign({}, state, {
        allIncomes: [...incomes]
      });
    case incomeActions.FETCH_INCOME_COMPLETE:
      incomes = state.allIncomes.map(income => income.id == action.payload.id ? new Cost(action.payload) : income);
      return Object.assign({}, state, {
        allIncomes: [...incomes]
      });
    case incomeActions.CREATE_INCOME_COMPLETE:
      return Object.assign({}, state, {
        allIncomes: [...state.allIncomes, new Cost(action.payload)]
      });
    case incomeActions.UPDATE_INCOME_COMPLETE:
      incomes = state.allIncomes.map(income => income.id == action.payload.id ? new Cost(action.payload) : income);
      return Object.assign({}, state, {
        allIncomes: [...incomes]
      });
    case incomeActions.DELETE_INCOME_COMPLETE:
      incomes = state.allIncomes.map(income => income.id == state.currentIncomeId ? null : income);
      return Object.assign({}, state, {
        allIncomes: [...incomes]
      });

    case incomeActions.DELETE_INCOME:
      return Object.assign({}, state, {
        currentIncomeId: action.payload
      });
    case incomeActions.CLEAR_INCOME_STATE_ACTION:
      return Object.assign({}, state, {
        allIncomes: [],
        currentIncomeId: null
      });

    default:
      return state;
  }
}
