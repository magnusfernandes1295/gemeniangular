import { User } from '../models';

import * as userActions from '../actions/user.actions';

export interface State {
  loggedUser: User;
  currentUser: User;
  currentUserId: number;
  userModal: boolean;
  userPasswordModal: boolean;
  allUsers: User[];
}

const initialState: State = {
  loggedUser: new User({}),
  currentUser: new User({}),
  currentUserId: null,
  userPasswordModal: false,
  userModal: false,
  allUsers: []
};

export function reducer(state = initialState, action: userActions.Actions): State {
  let users: User[] = [];
  switch (action.type) {
    case userActions.LOGIN_COMPLETE:
      return Object.assign({}, state, {
        loggedUser: new User(action.payload)
      })
    case userActions.VALIDATE_TOKEN_COMPLETE:
      return Object.assign({}, state, {
        loggedUser: new User(action.payload)
      })
    case userActions.FETCH_ALL_USERS_COMPLETE:
      users = action.payload.map(user => new User(user))
      return Object.assign({}, state, {
        allUsers: users
      })
    case userActions.FETCH_USER_COMPLETE:
      users = state.allUsers.map(user => user.id === action.payload.id ? action.payload : user);
      return Object.assign({}, state, {
        allUsers: [...users]
      });
    case userActions.UPDATE_USER_COMPLETE:
      users = state.allUsers.map(user => user.id === action.payload.id ? new User(action.payload) : user);
      return Object.assign({}, state, {
        allUsers: [...users]
      });
    case userActions.OPEN_USER_MODAL:
      return Object.assign({}, state, {
        userModal: true
      });
    case userActions.OPEN_USER_PASSWORD_MODAL:
      return Object.assign({}, state, {
        userPasswordModal: true
      });
    case userActions.CLOSE_USER_MODAL:
      return Object.assign({}, state, {
        userPasswordModal: false,
        userModal: false
      });
    case userActions.CREATE_NEW_USER_COMPLETE:
      return Object.assign({}, state, {
        allUsers: [...state.allUsers, new User(action.payload)]
      });
    case userActions.DELETE_USER_COMPLETE:
      return Object.assign({}, state, {
        allUsers: state.allUsers.filter(user => user.id != state.currentUserId)
      });
    case userActions.DELETE_USER:
      return Object.assign({}, state, {
        currentUserId: action.payload
      });
    case userActions.CLEAR_USER_STATE_ACTION:
      return Object.assign({}, state, {
        loggedUser: new User({}),
        currentUser: new User({}),
        currentUserId: null,
        userPasswordModal: false,
        userModal: false,
        allUsers: []
      });

    default:
      return state;
  }
}
