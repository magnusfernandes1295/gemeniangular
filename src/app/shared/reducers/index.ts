import * as fromUser from './user.reducer';
import * as fromDevice from './device.reducer';
import * as fromVehicle from './vehicle.reducer';
import * as fromCertificate from './certificate.reducer';
import * as fromInventory from './inventory.reducer';
import * as fromPurchaseOrder from './purchase-order.reducer';
import * as fromReceiveNote from './receive-note.reducer';
import * as fromRequisitionOrder from './requisition-order.reducer';
import * as fromSalarySlip from './salary-slip.reducer';
import * as fromTransaction from './transaction.reducer';
import * as fromExpense from './expense.reducer';
import * as fromIncome from './income.reducer';
import * as fromFeedback from './feedback.reducer';

import { ActionReducerMap } from '@ngrx/store';

export interface State {
  users: fromUser.State;
  devices: fromDevice.State;
  vehicles: fromVehicle.State;
  certificates: fromCertificate.State;
  inventories: fromInventory.State;
  purchaseOrders: fromPurchaseOrder.State;
  receiveNotes: fromReceiveNote.State;
  requisitionOrders: fromRequisitionOrder.State;
  salarySlips: fromSalarySlip.State;
  transactions: fromTransaction.State;
  expenses: fromExpense.State;
  incomes: fromIncome.State;
  feedback: fromFeedback.State;
}

export const reducers: ActionReducerMap<State> = {
  users: fromUser.reducer,
  devices: fromDevice.reducer,
  vehicles: fromVehicle.reducer,
  certificates: fromCertificate.reducer,
  inventories: fromInventory.reducer,
  purchaseOrders: fromPurchaseOrder.reducer,
  receiveNotes: fromReceiveNote.reducer,
  requisitionOrders: fromRequisitionOrder.reducer,
  salarySlips: fromSalarySlip.reducer,
  transactions: fromTransaction.reducer,
  expenses: fromExpense.reducer,
  incomes: fromIncome.reducer,
  feedback: fromFeedback.reducer
};

export const loggedUser = (state: State) => state.users.loggedUser;
export const currentUser = (state: State) => state.users.currentUser;
export const getUsers = (state: State) => state.users.allUsers;
export const getEmployees = (state: State) => state.users.allUsers.filter(user => user.role != 'dealer').filter(user => user.role != 'distributor').filter(user => user.role != 'manufacturer');
export const showUserModal = (state: State) => state.users.userModal;
export const showUserPasswordModal = (state: State) => state.users.userPasswordModal;
export const getDistributors = (state: State) => state.users.allUsers.filter(user => user.role === "distributor");
export const getUserEmails = (state: State) => state.users.allUsers.map(user => user.email);
export const getDealersDistributors = (state: State) => state.users.allUsers.filter(user => user.role === "dealer" || user.role === "distributor");

export const getAllDevices = (state: State) => state.devices.allDevices.reverse();
export const showDeviceModal = (state: State) => state.devices.showDeviceModal;

export const getVehicles = (state: State) => state.vehicles.allVehicles;
export const showVehicleModal = (state: State) => state.vehicles.showVehicleModal;
export const getVehicleMakes = (state: State) => state.vehicles.allVehicles.map(vehicle => vehicle.make);

export const getCertificates = (state: State) => state.certificates.allCertificates;
export const getCurrentCertificate = (state: State) => state.certificates.currentCertificate;
export const showCertificateModal = (state: State) => state.certificates.certificateModal;
export const getCertificatePageStatus = (state: State) => state.certificates.currentCertificatePageStatus;

export const getAllInventory = (state: State) => state.inventories.allInventory;
export const showInventoryModal = (state: State) => state.inventories.showInventoryModal;

export const getPurchaseOrders = (state: State) => state.purchaseOrders.allPurchaseOrders;
export const getCurrentPurchaseOrder = (state: State) => state.purchaseOrders.currentPurchaseOrder;
export const showPOModal = (state: State) => state.purchaseOrders.showPOModal;
export const showConfirmPO = (state: State) => state.purchaseOrders.showConfirmPO;
export const showDetailPO = (state: State) => state.purchaseOrders.showDetailPO;
export const showDispatchPO = (state: State) => state.purchaseOrders.showDispatchPO;
export const showClosePO = (state: State) => state.purchaseOrders.showClosePO;
export const getPurchaseOrderPageStatus = (state: State) => state.purchaseOrders.currentPurchaseOrderPageStatus;

export const getReceiveNotes = (state: State) => state.receiveNotes.allReceiveNotes;
export const getCurrentReceiveNote = (state: State) => state.receiveNotes.currentReceiveNote;
export const showNoteModal = (state: State) => state.receiveNotes.showNoteModal;
export const showNoteDetailModal = (state: State) => state.receiveNotes.showNoteDetailModal;
export const getReceiveNotePageStatus = (state: State) => state.receiveNotes.currentReceiveNotePageStatus;

export const getRequisitionOrders = (state: State) => state.requisitionOrders.allRequisitionOrders;
export const getCurrentRequisitionOrder = (state: State) => state.requisitionOrders.currentRequisitionOrder;
export const showReqModal = (state: State) => state.requisitionOrders.reqModal;
export const showViewReqModal = (state: State) => state.requisitionOrders.reqViewModal;
export const getRequisitionOrderPageStatus = (state: State) => state.requisitionOrders.currentRequisitionOrderPageStatus;

export const getSalarySlips = (state: State) => state.salarySlips.allSlips;
export const showSlipModal = (state: State) => state.salarySlips.showSlipModal;
export const getSalarySlipPageStatus = (state: State) => state.salarySlips.currentSalarySlipPageStatus;

export const getAllTransactions = (state: State) => state.transactions.allTransactions;
export const getCurrentTransaction = (state: State) => state.transactions.currentTransaction;
export const showTransactionModal = (state: State) => state.transactions.showTransactionModal;
export const getTransactionPageStatus = (state: State) => state.transactions.currentTransactionPageStatus;

export const getAllExpenses = (state: State) => state.expenses.allExpenses;
export const showExpenseModal = (state: State) => state.expenses.showExpenseModal;
export const getAllIncomes = (state: State) => state.incomes.allIncomes;

export const getAllFeedbacks = (state: State) => state.feedback.allFeedbacks;
export const showFeedbackModal = (state: State) => state.feedback.feedbackModal;
export const getFeedbackPageStatus = (state: State) => state.feedback.currentFeedbackPageStatus;