import { Device } from "../models";

import * as deviceActions from "../actions/device.actions";

export interface State {
  allDevices: Device[];
  currentDevice: Device;
  currentDeviceId: number;
  currentDeviceIds: number[];
  showDeviceModal: boolean
}

const initialState: State = {
  allDevices: [],
  currentDevice: null,
  currentDeviceId: null,
  currentDeviceIds: null,
  showDeviceModal: false
};

export function reducer(state = initialState, action: deviceActions.Actions): State {
  let devices: Device[] = [];
  switch (action.type) {
    case deviceActions.FETCH_ALL_DEVICES:
      devices = [];
      return Object.assign({}, state, {
        allDevices: [...devices]
      });
    case deviceActions.FETCH_ALL_DEVICES_COMPLETE:
      devices = action.payload.map(device => new Device(device))
      return Object.assign({}, state, {
        allDevices: [...devices]
      });
    case deviceActions.FETCH_DEVICE_COMPLETE:
      devices = state.allDevices.map(device => device.id == action.payload.id ? new Device(action.payload) : device);
      return Object.assign({}, state, {
        allDevices: [...devices],
        currentDevice: new Device(action.payload)
      });
    case deviceActions.CREATE_DEVICE_COMPLETE:
      devices = state.allDevices;
      action.payload.map(device => devices.push(new Device(device)));
      return Object.assign({}, state, {
        allDevices: [...devices]
      });
    case deviceActions.UPDATE_DEVICE_COMPLETE:
      devices = state.allDevices.map(device => device.id == action.payload.id ? new Device(action.payload) : device);
      return Object.assign({}, state, {
        allDevices: [...devices]
      });
    case deviceActions.DELETE_DEVICE_COMPLETE:
      devices = state.allDevices.map(device => device.id == state.currentDeviceId ? null : device);
      return Object.assign({}, state, {
        allDevices: [...devices]
      });
    case deviceActions.TRANSFER_DEVICE_COMPLETE:
      state.currentDeviceIds.map(id => devices = state.allDevices.filter(device => device.id != id));
      return Object.assign({}, state, {
        allDevices: [...devices]
      });

    case deviceActions.DELETE_DEVICE:
      return Object.assign({}, state, {
        currentDeviceId: action.payload
      });
    case deviceActions.TRANSFER_DEVICE:
      return Object.assign({}, state, {
        currentDeviceIds: action.payload.device_ids
      });
    case deviceActions.OPEN_DEVICE_MODAL_ACTION:
      return Object.assign({}, state, {
        showDeviceModal: true
      });
    case deviceActions.CLOSE_DEVICE_MODAL_ACTION:
      return Object.assign({}, state, {
        showDeviceModal: false
      });
    case deviceActions.CLEAR_DEVICE_STATE_ACTION:
      return Object.assign({}, state, {
        allDevices: [],
        currentDevice: null,
        currentDeviceId: null,
        currentDeviceIds: null
      });

    default:
      return state;
  }
}
