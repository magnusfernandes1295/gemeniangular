import { Feedback, PageData } from "../models";

import * as feedbackActions from "../actions/feedback.actions";

export interface State {
  allFeedbacks: Feedback[];
  currentFeedbackId: number;
  feedbackModal: boolean;
  currentFeedbackPageStatus: PageData;
}

const initialState: State = {
  allFeedbacks: [],
  currentFeedbackId: null,
  feedbackModal: false,
  currentFeedbackPageStatus: new PageData({})
};

export function reducer(state = initialState, action: feedbackActions.Actions): State {
  let feedback: Feedback[] = [];
  switch (action.type) {
    case feedbackActions.FETCH_ALL_FEEDBACK_COMPLETE:
      feedback = action.payload.map(feedback => new Feedback(feedback))
      return Object.assign({}, state, {
        allFeedbacks: [...feedback]
      });
    case feedbackActions.FETCH_FEEDBACK_COMPLETE:
      feedback = state.allFeedbacks.map(feedback => feedback.id == action.payload.id ? new Feedback(action.payload) : feedback);
      return Object.assign({}, state, {
        allFeedbacks: [...feedback],
        currentFeedback: new Feedback(action.payload)
      });
    case feedbackActions.FILTER_FEEDBACK_COMPLETE:
      feedback = action.payload.data.map(feedback => new Feedback(feedback));
      return Object.assign({}, state, {
        allFeedbacks: [...feedback],
        currentFeedbackPageStatus: new PageData({
          total: action.payload.total,
          per_page: action.payload.per_page
        })
      });
    case feedbackActions.CREATE_FEEDBACK_COMPLETE:
      return Object.assign({}, state, {
        allFeedbacks: [...state.allFeedbacks, new Feedback(action.payload)]
      });
    case feedbackActions.UPDATE_FEEDBACK_COMPLETE:
      feedback = state.allFeedbacks.map(feedback => feedback.id == action.payload.id ? new Feedback(action.payload) : feedback);
      return Object.assign({}, state, {
        allFeedbacks: [...feedback]
      });
    case feedbackActions.DELETE_FEEDBACK_COMPLETE:
      feedback = state.allFeedbacks.map(feedback => feedback.id == state.currentFeedbackId ? null : feedback);
      return Object.assign({}, state, {
        allFeedbacks: [...feedback]
      });
    case feedbackActions.MARK_READ_FEEDBACK_COMPLETE:
      feedback = state.allFeedbacks.map(feedback => feedback.id == action.payload.id ? new Feedback(action.payload) : feedback);
      return Object.assign({}, state, {
        allFeedbacks: [...feedback]
      });
    case feedbackActions.OPEN_FEEDBACK_MODAL:
      return Object.assign({}, state, {
        feedbackModal: true
      });
    case feedbackActions.CLOSE_FEEDBACK_MODAL:
      return Object.assign({}, state, {
        feedbackModal: false
      });

    case feedbackActions.DELETE_FEEDBACK:
      return Object.assign({}, state, {
        currentFeedbackId: action.payload
      });
    case feedbackActions.CLEAR_FEEDBACK_STATE_ACTION:
      return Object.assign({}, state, {
        allFeedbacks: [],
        currentFeedbackId: null,
        feedbackModal: false
      });

    default:
      return state;
  }
}
