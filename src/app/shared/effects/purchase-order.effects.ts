import { OPEN_PURCHASE_ORDER, DISPATCH_PURCHASE_ORDER } from './../actions/purchase-order.actions';
import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as purchaseOrderActions from '../actions/purchase-order.actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class PurchaseOrderEffects {
  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) { }

  @Effect()
  fetchAllPurchaseOrders$: Observable<Action> = this._action$.ofType(purchaseOrderActions.FETCH_ALL_PURCHASE_ORDERS)
    .mergeMap((action: purchaseOrderActions.FetchAllPurchaseOrdersAction) => this._tokenService.get('purchase_orders')
      .map(purchaseOrders => new purchaseOrderActions.FetchAllPurchaseOrdersCompleteAction(purchaseOrders.json().message))
      .catch((error) => of(new purchaseOrderActions.FetchAllPurchaseOrdersFailedAction(error.json().message))));

  @Effect()
  fetchPurchaseOrderDetail$: Observable<Action> = this._action$.ofType(purchaseOrderActions.FETCH_PURCHASE_ORDER, purchaseOrderActions.OPEN_PURCHASE_ORDER_DETAIL_MODAL)
    .mergeMap((action: purchaseOrderActions.FetchPurchaseOrderAction) => this._tokenService.get(`purchase_orders/${action.payload}`)
      .map(purchaseOrder => new purchaseOrderActions.FetchPurchaseOrderCompleteAction(purchaseOrder.json().message))
      .catch((error) => of(new purchaseOrderActions.FetchPurchaseOrderFailedAction(error.json().message))));

  @Effect()
  filterPurchaseOrder$: Observable<Action> = this._action$.ofType(purchaseOrderActions.FILTER_PURCHASE_ORDER)
    .mergeMap((action: purchaseOrderActions.FilterPurchaseOrderAction) => this._tokenService.post('purchase_orders/list', action.payload)
      .map(response => new purchaseOrderActions.FilterPurchaseOrderCompleteAction({
        data: response.json().message,
        total: response.headers.get('total'),
        per_page: response.headers.get('per-page')
      }))
      .catch((error) => of(new purchaseOrderActions.FilterPurchaseOrderFailedAction(error.json().message))));

  @Effect()
  createPurchaseOrder$: Observable<Action> = this._action$.ofType(purchaseOrderActions.CREATE_PURCHASE_ORDER)
    .mergeMap((action: purchaseOrderActions.CreatePurchaseOrderAction) => this._tokenService.post('purchase_orders', action.payload)
      .mergeMap(purchaseOrder => [new purchaseOrderActions.CreatePurchaseOrderCompleteAction(purchaseOrder.json().message), new purchaseOrderActions.ClosePurchaseOrderModalAction])
      .catch((error) => of(new purchaseOrderActions.CreatePurchaseOrderFailedAction(error.json().message))));

  @Effect()
  updatePurchaseOrder$: Observable<Action> = this._action$.ofType(purchaseOrderActions.UPDATE_PURCHASE_ORDER)
    .mergeMap((action: purchaseOrderActions.UpdatePurchaseOrderAction) => this._tokenService.patch(`purchase_orders/${action.payload.purchase_order.id}`, action.payload)
      .mergeMap(purchaseOrder => [new purchaseOrderActions.UpdatePurchaseOrderCompleteAction(purchaseOrder.json().message), new purchaseOrderActions.ClosePurchaseOrderModalAction])
      .catch((error) => of(new purchaseOrderActions.UpdatePurchaseOrderFailedAction(error.json().message))));

  @Effect()
  deletePurchaseOrder$: Observable<Action> = this._action$.ofType(purchaseOrderActions.DELETE_PURCHASE_ORDER)
    .mergeMap((action: purchaseOrderActions.DeletePurchaseOrderAction) => this._tokenService.delete(`purchase_orders/${action.payload}`)
      .map(purchaseOrder => new purchaseOrderActions.DeletePurchaseOrderCompleteAction(purchaseOrder.json()))
      .catch((error) => of(new purchaseOrderActions.DeletePurchaseOrderFailedAction(error.json().message))));
  
  @Effect()
  openPurchaseOrder$: Observable<Action> = this._action$.ofType(purchaseOrderActions.OPEN_PURCHASE_ORDER)
    .mergeMap((action: purchaseOrderActions.OpenPurchaseOrderAction) => this._tokenService.post(`purchase_orders/${action.payload}/open`, null)
      .map(purchaseOrder => new purchaseOrderActions.OpenPurchaseOrderCompleteAction(purchaseOrder.json().message))
      .catch((error) => of(new purchaseOrderActions.OpenPurchaseOrderFailedAction(error.json().message))));

  @Effect()
  confirmPurchaseOrder$: Observable<Action> = this._action$.ofType(purchaseOrderActions.CONFIRM_PURCHASE_ORDER)
    .mergeMap((action: purchaseOrderActions.ConfirmPurchaseOrderAction) => this._tokenService.post(`purchase_orders/${action.payload.id}/processing`, action.payload.message)
      .mergeMap(purchaseOrder => [new purchaseOrderActions.ConfirmPurchaseOrderCompleteAction(purchaseOrder.json().message), new purchaseOrderActions.ClosePurchaseOrderModalAction])
      .catch((error) => of(new purchaseOrderActions.ConfirmPurchaseOrderFailedAction(error.json().message))));

  @Effect()
  dispatchPurchaseOrder$: Observable<Action> = this._action$.ofType(purchaseOrderActions.DISPATCH_PURCHASE_ORDER)
    .mergeMap((action: purchaseOrderActions.DispatchPurchaseOrderAction) => this._tokenService.post(`purchase_orders/${action.payload.id}/dispatch`, action.payload)
      .mergeMap(purchaseOrder => [new purchaseOrderActions.DispatchPurchaseOrderCompleteAction(purchaseOrder.json().message), new purchaseOrderActions.ClosePurchaseOrderModalAction])
      .catch((error) => of(new purchaseOrderActions.DispatchPurchaseOrderFailedAction(error.json().message))));

  @Effect()
  closePurchaseOrder$: Observable<Action> = this._action$.ofType(purchaseOrderActions.CLOSE_PURCHASE_ORDER)
    .mergeMap((action: purchaseOrderActions.ClosePurchaseOrderAction) => this._tokenService.post(`purchase_orders/${action.payload.id}/close`, action.payload.message)
      .mergeMap(purchaseOrder => [new purchaseOrderActions.ClosePurchaseOrderCompleteAction(purchaseOrder.json().message), new purchaseOrderActions.ClosePurchaseOrderModalAction])
      .catch((error) => of(new purchaseOrderActions.ClosePurchaseOrderFailedAction(error.json().message))));
}
