import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as receiveNoteActions from '../actions/receive-note.actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class ReceiveNoteEffects {
  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) { }

  @Effect()
  createReceiveNote$: Observable<Action> = this._action$.ofType(receiveNoteActions.CREATE_NEW_RECEIVE_NOTE)
    .mergeMap((action: receiveNoteActions.CreateNewReceiveNoteAction) => this._tokenService.post(`receive_notes`, action.payload)
      .mergeMap(response => [new receiveNoteActions.CreateNewReceiveNoteCompleteAction(response.json().message), new receiveNoteActions.CloseReceiveNoteModalAction])
      .catch((error) => of(new receiveNoteActions.CreateNewReceiveNoteFailedAction(error.json()))));

  @Effect()
  updateReceiveNote$: Observable<Action> = this._action$.ofType(receiveNoteActions.UPDATE_RECEIVE_NOTE)
    .mergeMap((action: receiveNoteActions.UpdateReceiveNoteAction) => this._tokenService.patch(`receive_notes/${action.payload.receive_note.id}`, action.payload)
      .mergeMap(response => [new receiveNoteActions.UpdateReceiveNoteCompleteAction(response.json().message), new receiveNoteActions.CloseReceiveNoteModalAction])
      .catch((error) => of(new receiveNoteActions.UpdateReceiveNoteFailedAction(error.json()))));

  @Effect()
  deleteReceiveNote$: Observable<Action> = this._action$.ofType(receiveNoteActions.DELETE_RECEIVE_NOTE)
    .mergeMap((action: receiveNoteActions.DeleteReceiveNoteAction) => this._tokenService.delete(`receive_notes/${action.payload}`)
      .map(response => new receiveNoteActions.DeleteReceiveNoteCompleteAction(response.json()))
      .catch((error) => of(new receiveNoteActions.DeleteReceiveNoteFailedAction(error.json()))));

  @Effect()
  confirmReceiveNote$: Observable<Action> = this._action$.ofType(receiveNoteActions.CONFIRM_RECEIVE_NOTE)
    .mergeMap((action: receiveNoteActions.ConfirmReceiveNoteAction) => this._tokenService.post(`receive_notes/${action.payload}/confirm`, null)
      .mergeMap(response => [new receiveNoteActions.ConfirmReceiveNoteCompleteAction(response.json().message), new receiveNoteActions.CloseReceiveNoteModalAction])
      .catch((error) => of(new receiveNoteActions.ConfirmReceiveNoteFailedAction(error.json()))));

  @Effect()
  fetchAllReceiveNotes$: Observable<Action> = this._action$.ofType(receiveNoteActions.FETCH_ALL_RECEIVE_NOTE)
    .mergeMap((action: receiveNoteActions.FetchAllReceiveNoteAction) => this._tokenService.get(`receive_notes`)
      .map(response => new receiveNoteActions.FetchAllReceiveNoteCompleteAction(response.json().message))
      .catch((error) => of(new receiveNoteActions.FetchAllReceiveNoteFailedAction(error.json()))));

  @Effect()
  fetchReceiveNote$: Observable<Action> = this._action$.ofType(receiveNoteActions.FETCH_RECEIVE_NOTE)
    .mergeMap((action: receiveNoteActions.FetchReceiveNoteAction) => this._tokenService.get(`receive_notes/${action.payload}`)
      .map(response => new receiveNoteActions.FetchReceiveNoteCompleteAction(response.json().message))
      .catch((error) => of(new receiveNoteActions.FetchReceiveNoteFailedAction(error.json()))));

  @Effect()
  filterReceiveNote$: Observable<Action> = this._action$.ofType(receiveNoteActions.FILTER_RECEIVE_NOTE)
    .mergeMap((action: receiveNoteActions.FilterReceiveNoteAction) => this._tokenService.post('receive_notes/list', action.payload)
      .map(response => new receiveNoteActions.FilterReceiveNoteCompleteAction({
        data: response.json().message,
        total: response.headers.get('total'),
        per_page: response.headers.get('per-page')
      }))
      .catch((error) => of(new receiveNoteActions.FilterReceiveNoteFailedAction(error.json()))));

}
