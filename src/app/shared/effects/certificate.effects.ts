import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as certificateActions from '../actions/certificate.actions';
import { CertificateService } from '../services/certificate.service';
import { Certificate } from '../models';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class CertificateEffects {

  public type: boolean = false;

  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router,
    private _certificateService: CertificateService
  ) { }

  @Effect()
  fetchAllCertificates$: Observable<Action> = this._action$.ofType(certificateActions.FETCH_ALL_CERTIFICATES)
    .mergeMap((action: certificateActions.FetchAllCertificatesAction) => this._tokenService.get('certificates')
      .map(certificates => new certificateActions.FetchAllCertificatesCompleteAction(certificates.json().message))
      .catch(error => of(new certificateActions.FetchAllCertificatesFailedAction(error.json().message))));

  @Effect()
  fetchCertificate$: Observable<Action> = this._action$.ofType(certificateActions.FETCH_CERTIFICATE)
    .mergeMap((action: certificateActions.FetchCertificateAction) => this._tokenService.get(`certificates/${action.payload}`)
      .map(response => new certificateActions.FetchCertificateCompleteAction(response.json().message))
      .catch(error => of(new certificateActions.FetchCertificateFailedAction(error.json().message))));

  @Effect()
  filterCertificates$: Observable<Action> = this._action$.ofType(certificateActions.FILTER_CERTIFICATES)
    .mergeMap((action: certificateActions.FilterCertificatesAction) => this._tokenService.post('certificates/list', action.payload)
      .map(response => new certificateActions.FilterCertificatesCompleteAction({
        data: response.json().message,
        total: response.headers.get('total'),
        per_page: response.headers.get('per-page')
      }))
      .catch(error => of(new certificateActions.FilterCertificatesFailedAction(error.json().message))));

  @Effect()
  createCertificate$: Observable<Action> = this._action$.ofType(certificateActions.CREATE_CERTIFICATE)
    .mergeMap((action: certificateActions.CreateCertificateAction) => this._tokenService.post('certificates', action.payload)
      .mergeMap(response => {
        this._router.navigate([`/dashboard/certificate/issue-certificate/${response.json().message.id}`])
        return [new certificateActions.CreateCertificateCompleteAction(response.json().message), new certificateActions.CloseCertificateModalAction]
      })
      .catch(error => of(new certificateActions.CreateCertificateFailedAction(error.json().message))));

  @Effect()
  updateCertificate$: Observable<Action> = this._action$.ofType(certificateActions.UPDATE_CERTIFICATE)
    .mergeMap((action: certificateActions.UpdateCertificateAction) => this._tokenService.patch(`certificates/${action.payload.id}`, action.payload)
      .map(response => new certificateActions.UpdateCertificateCompleteAction(response.json().message))
      .catch(error => of(new certificateActions.UpdateCertificateFailedAction(error.json().message))));

  @Effect()
  deleteCertificate$: Observable<Action> = this._action$.ofType(certificateActions.DELETE_CERTIFICATE)
    .mergeMap((action: certificateActions.DeleteCertificateAction) => this._tokenService.delete(`certificates/${action.payload}`)
      .map(response => new certificateActions.DeleteCertificateCompleteAction(response.json()))
      .catch(error => of(new certificateActions.DeleteCertificateFailedAction(error.json().message))));

  @Effect()
  issueCertificate$: Observable<Action> = this._action$.ofType(certificateActions.ISSUE_CERTIFICATE)
    .mergeMap((action: certificateActions.IssueCertificateAction) => {
      this.type = action.payload.type;
      return this._tokenService.post(`certificates/${action.payload.id}/issue`, null)
        .map(response => {
          if (this.type) {
            this._certificateService.process('Print', new Certificate(response.json().message));
          } else {
            this._certificateService.process('Download', new Certificate(response.json().message));
          }
          return new certificateActions.IssueCertificateCompleteAction(response.json().message);
        })
        .catch(error => of(new certificateActions.IssueCertificateFailedAction(error.json().message)))
    });

  @Effect()
  renewCertificate$: Observable<Action> = this._action$.ofType(certificateActions.RENEW_CERTIFICATE)
    .mergeMap((action: certificateActions.RenewCertificateAction) => this._tokenService.post(`certificates/${action.payload}/renew`, null)
      .map(response => new certificateActions.RenewCertificateCompleteAction(response.json().message))
      .catch(error => of(new certificateActions.RenewCertificateFailedAction(error.json().message))));
  
}
