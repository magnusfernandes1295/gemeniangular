import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as inventoryActions from '../actions/inventory.actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class InventoryEffects {
  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) { }

  @Effect()
  createInventory$: Observable<Action> = this._action$.ofType(inventoryActions.CREATE_NEW_INVENTORY)
    .mergeMap((action: inventoryActions.CreateInventoryAction) => this._tokenService.post(`inventory_items`, action.payload)
      .mergeMap(response => [new inventoryActions.CreateNewInventoryCompleteAction(response.json().message), new inventoryActions.CloseInventoryModalAction])
      .catch((error) => of(new inventoryActions.CreateNewInventoryFailedAction(error.json().message))));

  @Effect()
  updateInventory$: Observable<Action> = this._action$.ofType(inventoryActions.UPDATE_INVENTORY)
    .mergeMap((action: inventoryActions.UpdateInventoryAction) => this._tokenService.patch(`inventory_items/${action.payload.id}`, action.payload.message)
      .mergeMap(response => [new inventoryActions.UpdateInventoryCompleteAction(response.json().message), new inventoryActions.CloseInventoryModalAction])
      .catch((error) => of(new inventoryActions.UpdateInventoryFailedAction(error.json().message))));

  @Effect()
  deleteInventory$: Observable<Action> = this._action$.ofType(inventoryActions.DELETE_INVENTORY)
    .mergeMap((action: inventoryActions.DeleteInventoryAction) => this._tokenService.delete(`inventory_items/${action.payload}`)
      .map(response => new inventoryActions.DeleteInventoryCompleteAction(response.json()))
      .catch((error) => of(new inventoryActions.DeleteInventoryFailedAction(error.json().message))));

  @Effect()
  fetchAllInventory$: Observable<Action> = this._action$.ofType(inventoryActions.FETCH_ALL_INVENTORY)
    .mergeMap((action: inventoryActions.FetchAllInventoryAction) => this._tokenService.get(`inventory_items`)
      .map(response => new inventoryActions.FetchAllInventoryCompleteAction(response.json().message))
      .catch((error) => of(new inventoryActions.FetchAllInventoryFailedAction(error.json().message))));

  @Effect()
  fetchInventory$: Observable<Action> = this._action$.ofType(inventoryActions.FETCH_INVENTORY)
    .mergeMap((action: inventoryActions.FetchInventoryAction) => this._tokenService.get(`inventory_items/${action.payload}`)
      .map(response => new inventoryActions.FetchInventoryCompleteAction(response.json().message))
      .catch((error) => of(new inventoryActions.FetchInventoryFailedAction(error.json().message))));

  @Effect()
  filterInventory$: Observable<Action> = this._action$.ofType(inventoryActions.FILTER_INVENTORY)
    .mergeMap((action: inventoryActions.FilterInventoryAction) => this._tokenService.get(`inventory_items?category=${action.payload}`)
      .map(response => new inventoryActions.FilterInventoryCompleteAction(response.json().message))
      .catch((error) => of(new inventoryActions.FilterInventoryFailedAction(error.json().message))));

  @Effect()
  fetchInventoryHistory$: Observable<Action> = this._action$.ofType(inventoryActions.FETCH_INVENTORY_HISTORY)
    .mergeMap((action: inventoryActions.FetchInventoryHistoryAction) => this._tokenService.get(`inventory_items/history/${action.payload}`)
      .map(response => new inventoryActions.FetchInventoryHistoryCompleteAction(response.json().message))
      .catch((error) => of(new inventoryActions.FetchInventoryHistoryFailedAction(error.json().message))));

}
