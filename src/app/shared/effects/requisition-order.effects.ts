import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as requisitionOrderActions from '../actions/requisition-order.actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class RequisitionOrderEffects {
  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) { }

  @Effect()
  createRequisitionOrder$: Observable<Action> = this._action$.ofType(requisitionOrderActions.CREATE_NEW_REQUISITION_ORDER)
    .mergeMap((action: requisitionOrderActions.CreateNewRequisitionOrderAction) => this._tokenService.post(`req_orders`, action.payload)
      .mergeMap(response => [
        new requisitionOrderActions.CreateNewRequisitionOrderCompleteAction(response.json().message),
        new requisitionOrderActions.CloseRequisitionOrderModal
      ])
      .catch((error) => of(new requisitionOrderActions.CreateNewRequisitionOrderFailedAction(error.json().message))));

  @Effect()
  updateRequisitionOrder$: Observable<Action> = this._action$.ofType(requisitionOrderActions.UPDATE_REQUISITION_ORDER)
    .mergeMap((action: requisitionOrderActions.UpdateRequisitionOrderAction) => this._tokenService.patch(`req_orders/${action.payload.id}`, {req_order: {req_particulars: action.payload.req_particulars}})
      .mergeMap(response => [new requisitionOrderActions.UpdateRequisitionOrderCompleteAction(response.json().message), new requisitionOrderActions.CloseRequisitionOrderModal])
      .catch((error) => of(new requisitionOrderActions.UpdateRequisitionOrderFailedAction(error.json().message))));

  @Effect()
  deleteRequisitionOrder$: Observable<Action> = this._action$.ofType(requisitionOrderActions.DELETE_REQUISITION_ORDER)
    .mergeMap((action: requisitionOrderActions.DeleteRequisitionOrderAction) => this._tokenService.delete(`req_orders/${action.payload}`)
      .map(response => new requisitionOrderActions.DeleteRequisitionOrderCompleteAction(response.json()))
      .catch((error) => of(new requisitionOrderActions.DeleteRequisitionOrderFailedAction(error.json().message))));

  @Effect()
  openRequisitionOrder$: Observable<Action> = this._action$.ofType(requisitionOrderActions.OPEN_REQUISITION_ORDER)
    .mergeMap((action: requisitionOrderActions.OpenRequisitionOrderAction) => this._tokenService.post(`req_orders/${action.payload}/open`, null)
      .mergeMap(response => [new requisitionOrderActions.OpenRequisitionOrderCompleteAction(response.json().message), new requisitionOrderActions.CloseRequisitionOrderModal])
      .catch((error) => of(new requisitionOrderActions.OpenRequisitionOrderFailedAction(error.json().message))));

  @Effect()
  closeRequisitionOrder$: Observable<Action> = this._action$.ofType(requisitionOrderActions.CLOSE_REQUISITION_ORDER)
    .mergeMap((action: requisitionOrderActions.CloseRequisitionOrderAction) => this._tokenService.post(`req_orders/${action.payload}/close`, null)
      .map(response => new requisitionOrderActions.CloseRequisitionOrderCompleteAction(response.json().message))
      .catch((error) => of(new requisitionOrderActions.CloseRequisitionOrderFailedAction(error.json().message))));

  @Effect()
  fetchAllRequisitionOrders$: Observable<Action> = this._action$.ofType(requisitionOrderActions.FETCH_ALL_REQUISITION_ORDERS)
    .mergeMap((action: requisitionOrderActions.FetchAllRequisitionOrdersAction) => this._tokenService.get(`req_orders`)
      .map(response => new requisitionOrderActions.FetchAllRequisitionOrdersCompleteAction(response.json().message))
      .catch((error) => of(new requisitionOrderActions.FetchAllRequisitionOrdersFailedAction(error.json().message))));

  @Effect()
  fetchRequisitionOrder$: Observable<Action> = this._action$.ofType(requisitionOrderActions.FETCH_REQUISITION_ORDER)
    .mergeMap((action: requisitionOrderActions.FetchRequisitionOrderAction) => this._tokenService.get(`req_orders/${action.payload}`)
      .map(response => new requisitionOrderActions.FetchRequisitionOrderCompleteAction(response.json().message))
      .catch((error) => of(new requisitionOrderActions.FetchRequisitionOrderFailedAction(error.json().message))));

  @Effect()
  filterRequisitionOrder$: Observable<Action> = this._action$.ofType(requisitionOrderActions.FILTER_REQUISITION_ORDER)
    .mergeMap((action: requisitionOrderActions.FilterRequisitionOrderAction) => this._tokenService.post('req_orders/list', action.payload)
      .map(response => new requisitionOrderActions.FilterRequisitionOrderCompleteAction({
        data: response.json().message,
        total: response.headers.get('total'),
        per_page: response.headers.get('per-page')
      }))
      .catch((error) => of(new requisitionOrderActions.FilterRequisitionOrderFailedAction(error.json().message))));
}
