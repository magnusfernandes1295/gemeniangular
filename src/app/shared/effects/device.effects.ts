import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as deviceActions from '../actions/device.actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class DeviceEffects {
  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) { }

  @Effect()
  fetchAllDevices$: Observable<Action> = this._action$.ofType(deviceActions.FETCH_ALL_DEVICES)
    .mergeMap((action: deviceActions.FetchAllDevicesAction) => this._tokenService.get('devices')
      .map(devices => new deviceActions.FetchAllDevicesCompleteAction(devices.json().message))
      .catch((error) => of(new deviceActions.FetchAllDevicesFailedAction(error.json().message))));

  fetchDevice$: Observable<Action> = this._action$.ofType(deviceActions.FETCH_DEVICE)
    .mergeMap((action: deviceActions.FetchDeviceAction) => this._tokenService.get(`device/${action.payload}`)
      .map(device => new deviceActions.FetchDeviceCompleteAction(device.json().message))
      .catch((error) => of(new deviceActions.FetchDeviceFailedAction(error.json().message))));

  @Effect()
  createDevice$: Observable<Action> = this._action$.ofType(deviceActions.CREATE_DEVICE)
    .mergeMap((action: deviceActions.CreateDeviceAction) => this._tokenService.post('devices', action.payload)
      .mergeMap(device => [new deviceActions.CreateDeviceCompleteAction(device.json().message), new deviceActions.CloseDeviceModalAction])
      .catch(error => of(new deviceActions.CreateDeviceFailedAction(error.json().message))));

  @Effect()
  updateDevice$: Observable<Action> = this._action$.ofType(deviceActions.UPDATE_DEVICE)
    .mergeMap((action: deviceActions.UpdateDeviceAction) => this._tokenService.patch(`devices/${action.payload.id}`, action.payload)
      .map(device => new deviceActions.UpdateDeviceCompleteAction(device.json().message))
      .catch(error => of(new deviceActions.UpdateDeviceFailedAction(error.json().message))));

  @Effect()
  deleteDevice$: Observable<Action> = this._action$.ofType(deviceActions.DELETE_DEVICE)
    .mergeMap((action: deviceActions.DeleteDeviceAction) => this._tokenService.delete(`devices/${action.payload}`)
      .map(device => new deviceActions.DeleteDeviceCompleteAction(device.json()))
      .catch(error => of(new deviceActions.DeleteDeviceFailedAction(error.json().message))));
  
  @Effect()
  transferDevices$: Observable<Action> = this._action$.ofType(deviceActions.TRANSFER_DEVICE)
    .mergeMap((action: deviceActions.TransferDeviceAction) => this._tokenService.post('devices/transfer', action.payload)
      .mergeMap(response => [new deviceActions.TransferDeviceCompleteAction(response.json().message), new deviceActions.FetchAllDevicesAction])
      .catch(error => of(new deviceActions.TransferDeviceFailedAction(error.json().message))));

}
