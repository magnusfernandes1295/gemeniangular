import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as salarySlipActions from '../actions/salary-slip.actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class SalarySlipEffects {
  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) { }

  @Effect()
  fetchAllSalarySlips$: Observable<Action> = this._action$.ofType(salarySlipActions.FETCH_ALL_SALARY_SLIPS)
    .mergeMap((action: salarySlipActions.FetchAllSalarySlipsAction) => this._tokenService.get('salary_slips')
      .map(salarySlips => new salarySlipActions.FetchAllSalarySlipsCompleteAction(salarySlips.json().message))
      .catch((error) => of(new salarySlipActions.FetchAllSalarySlipsFailedAction(error.json().message))));

  fetchSalarySlip$: Observable<Action> = this._action$.ofType(salarySlipActions.FETCH_SALARY_SLIP)
    .mergeMap((action: salarySlipActions.FetchSalarySlipAction) => this._tokenService.get(`salary_slips/${action.payload}`)
      .map(salarySlip => new salarySlipActions.FetchSalarySlipCompleteAction(salarySlip.json().message))
      .catch((error) => of(new salarySlipActions.FetchSalarySlipFailedAction(error.json().message))));

  @Effect()
  filterSalarySlips$: Observable<Action> = this._action$.ofType(salarySlipActions.FILTER_SALARY_SLIPS)
    .mergeMap((action: salarySlipActions.FilterSalarySlipsAction) => this._tokenService.post('salary_slips/list', action.payload)
      .map(response => new salarySlipActions.FilterSalarySlipsCompleteAction({
        data: response.json().message,
        total: response.headers.get('total'),
        per_page: response.headers.get('per-page')
      }))
      .catch((error) => of(new salarySlipActions.FilterSalarySlipsFailedAction(error.json().message))));

  @Effect()
  createSalarySlip$: Observable<Action> = this._action$.ofType(salarySlipActions.CREATE_SALARY_SLIP)
    .mergeMap((action: salarySlipActions.CreateSalarySlipAction) => this._tokenService.post('salary_slips', action.payload)
      .mergeMap(salarySlip => [new salarySlipActions.CreateSalarySlipCompleteAction(salarySlip.json().message), new salarySlipActions.CloseSalarySlipModalAction])
      .catch(error => of(new salarySlipActions.CreateSalarySlipFailedAction(error.json().message))));

  @Effect()
  updateSalarySlip$: Observable<Action> = this._action$.ofType(salarySlipActions.UPDATE_SALARY_SLIP)
    .mergeMap((action: salarySlipActions.UpdateSalarySlipAction) => this._tokenService.patch(`salary_slips/${action.payload.id}`, action.payload)
      .mergeMap(salarySlip => [new salarySlipActions.UpdateSalarySlipCompleteAction(salarySlip.json().message), new salarySlipActions.CloseSalarySlipModalAction])
      .catch(error => of(new salarySlipActions.UpdateSalarySlipFailedAction(error.json().message))));

  @Effect()
  deleteSalarySlip$: Observable<Action> = this._action$.ofType(salarySlipActions.DELETE_SALARY_SLIP)
    .mergeMap((action: salarySlipActions.DeleteSalarySlipAction) => this._tokenService.delete(`salary_slips/${action.payload}`)
      .map(salarySlip => new salarySlipActions.DeleteSalarySlipCompleteAction(salarySlip.json().message))
      .catch(error => of(new salarySlipActions.DeleteSalarySlipFailedAction(error.json()))));
  
  @Effect()
  confirmSalarySlip$: Observable<Action> = this._action$.ofType(salarySlipActions.CONFIRM_SALARY_SLIP)
    .mergeMap((action: salarySlipActions.ConfirmSalarySlipAction) => this._tokenService.post(`salary_slips/${action.payload}/confirm`, action.payload)
      .map(salarySlip => new salarySlipActions.ConfirmSalarySlipCompleteAction(salarySlip.json().message))
      .catch(error => of(new salarySlipActions.ConfirmSalarySlipFailedAction(error.json().message))));

    
  @Effect()
  paySalarySlip$: Observable<Action> = this._action$.ofType(salarySlipActions.PAY_SALARY_SLIP)
    .mergeMap((action: salarySlipActions.PaySalarySlipAction) => this._tokenService.post(`salary_slips/${action.payload}/pay`, action.payload)
      .map(salarySlip => new salarySlipActions.PaySalarySlipCompleteAction(salarySlip.json().message))
      .catch(error => of(new salarySlipActions.PaySalarySlipFailedAction(error.json().message))));

}
