import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as expenseActions from '../actions/expense.actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class ExpenseEffects {
  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) { }

  @Effect()
  fetchAllExpenses$: Observable<Action> = this._action$.ofType(expenseActions.FETCH_ALL_EXPENSES)
    .mergeMap((action: expenseActions.FetchAllExpensesAction) => this._tokenService.get('expenses')
      .map(expenses => new expenseActions.FetchAllExpensesCompleteAction(expenses.json().message))
      .catch((error) => of(new expenseActions.FetchAllExpensesFailedAction(error.json().message))));

  fetchExpense$: Observable<Action> = this._action$.ofType(expenseActions.FETCH_EXPENSE)
    .mergeMap((action: expenseActions.FetchExpenseAction) => this._tokenService.get(`expenses/${action.payload}`)
      .map(expense => new expenseActions.FetchExpenseCompleteAction(expense.json().message))
      .catch((error) => of(new expenseActions.FetchExpenseFailedAction(error.json().message))));

  @Effect()
  createExpense$: Observable<Action> = this._action$.ofType(expenseActions.CREATE_EXPENSE)
    .mergeMap((action: expenseActions.CreateExpenseAction) => this._tokenService.post('expenses', action.payload)
      .mergeMap(expense => [new expenseActions.CreateExpenseCompleteAction(expense.json().message), new expenseActions.CloseExpenseModalAction])
      .catch(error => of(new expenseActions.CreateExpenseFailedAction(error.json().message))));

  @Effect()
  updateExpense$: Observable<Action> = this._action$.ofType(expenseActions.UPDATE_EXPENSE)
    .mergeMap((action: expenseActions.UpdateExpenseAction) => this._tokenService.patch(`expenses/${action.payload.id}`, action.payload.message)
      .mergeMap(expense => [new expenseActions.UpdateExpenseCompleteAction(expense.json().message), new expenseActions.CloseExpenseModalAction])
      .catch(error => of(new expenseActions.UpdateExpenseFailedAction(error.json().message))));

  @Effect()
  deleteExpense$: Observable<Action> = this._action$.ofType(expenseActions.DELETE_EXPENSE)
    .mergeMap((action: expenseActions.DeleteExpenseAction) => this._tokenService.delete(`expenses/${action.payload.id}`)
      .map(expense => new expenseActions.DeleteExpenseCompleteAction(expense.json()))
      .catch(error => of(new expenseActions.DeleteExpenseFailedAction(error.json().message))));

}
