import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as feedbackActions from '../actions/feedback.actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class FeedbackEffects {
  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) { }

  @Effect()
  fetchAllFeedback$: Observable<Action> = this._action$.ofType(feedbackActions.FETCH_ALL_FEEDBACK)
    .mergeMap((action: feedbackActions.FetchAllFeedbackAction) => this._tokenService.get('feedbacks')
      .map(feedback => new feedbackActions.FetchAllFeedbackCompleteAction(feedback.json().message))
      .catch((error) => of(new feedbackActions.FetchAllFeedbackFailedAction(error.json().message))));

  @Effect()
  fetchFeedback$: Observable<Action> = this._action$.ofType(feedbackActions.FETCH_FEEDBACK)
    .mergeMap((action: feedbackActions.FetchFeedbackAction) => this._tokenService.get(`feedbacks/${action.payload}`)
      .map(feedback => new feedbackActions.FetchFeedbackCompleteAction(feedback.json().message))
      .catch((error) => of(new feedbackActions.FetchFeedbackFailedAction(error.json().message))));

  @Effect()
  filterFeedback$: Observable<Action> = this._action$.ofType(feedbackActions.FILTER_FEEDBACK)
    .mergeMap((action: feedbackActions.FilterFeedbackAction) => this._tokenService.post('feedbacks/list', action.payload)
      .map(response => new feedbackActions.FilterFeedbackCompleteAction({
        data: response.json().message,
        total: response.headers.get('total'),
        per_page: response.headers.get('per-page')
      }))
      .catch((error) => of(new feedbackActions.FilterFeedbackFailedAction(error.json().message))));

  @Effect()
  createFeedback$: Observable<Action> = this._action$.ofType(feedbackActions.CREATE_FEEDBACK)
    .mergeMap((action: feedbackActions.CreateFeedbackAction) => this._tokenService.post('feedbacks', action.payload)
      .mergeMap(feedback => [new feedbackActions.CreateFeedbackCompleteAction(feedback.json().message), new feedbackActions.CloseFeedbackModal])
      .catch(error => of(new feedbackActions.CreateFeedbackFailedAction(error.json().message))));

  @Effect()
  updateFeedback$: Observable<Action> = this._action$.ofType(feedbackActions.UPDATE_FEEDBACK)
    .mergeMap((action: feedbackActions.UpdateFeedbackAction) => this._tokenService.patch(`feedbacks/${action.payload.id}`, action.payload)
      .map(feedback => new feedbackActions.UpdateFeedbackCompleteAction(feedback.json().message))
      .catch(error => of(new feedbackActions.UpdateFeedbackFailedAction(error.json().message))));

  @Effect()
  deleteFeedback$: Observable<Action> = this._action$.ofType(feedbackActions.DELETE_FEEDBACK)
    .mergeMap((action: feedbackActions.DeleteFeedbackAction) => this._tokenService.delete(`feedbacks/${action.payload}`)
      .map(feedback => new feedbackActions.DeleteFeedbackCompleteAction(feedback.json()))
      .catch(error => of(new feedbackActions.DeleteFeedbackFailedAction(error.json().message))));
  
  @Effect()
  markReadFeedback$: Observable<Action> = this._action$.ofType(feedbackActions.MARK_READ_FEEDBACK)
    .mergeMap((action: feedbackActions.MarkReadFeedbackAction) => this._tokenService.post(`feedbacks/${action.payload}/mark_read`, null)
      .map(response => new feedbackActions.MarkReadFeedbackCompleteAction(response.json().message))
      .catch(error => of(new feedbackActions.MarkReadFeedbackFailedAction(error.json().message))));

}
