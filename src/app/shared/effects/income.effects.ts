import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as incomeActions from '../actions/income.actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class IncomeEffects {
  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) { }

  @Effect()
  fetchAllIncomes$: Observable<Action> = this._action$.ofType(incomeActions.FETCH_ALL_INCOMES)
    .mergeMap((action: incomeActions.FetchAllIncomesAction) => this._tokenService.get('incomes')
      .map(incomes => new incomeActions.FetchAllIncomesCompleteAction(incomes.json().message))
      .catch((error) => of(new incomeActions.FetchAllIncomesFailedAction(error.json().message))));

  fetchIncome$: Observable<Action> = this._action$.ofType(incomeActions.FETCH_INCOME)
    .mergeMap((action: incomeActions.FetchIncomeAction) => this._tokenService.get(`incomes/${action.payload}`)
      .map(income => new incomeActions.FetchIncomeCompleteAction(income.json().message))
      .catch((error) => of(new incomeActions.FetchIncomeFailedAction(error.json().message))));

  @Effect()
  createIncome$: Observable<Action> = this._action$.ofType(incomeActions.CREATE_INCOME)
    .mergeMap((action: incomeActions.CreateIncomeAction) => this._tokenService.post('incomes', action.payload)
      .map(income => new incomeActions.CreateIncomeCompleteAction(income.json().message))
      .catch(error => of(new incomeActions.CreateIncomeFailedAction(error.json().message))));

  @Effect()
  updateIncome$: Observable<Action> = this._action$.ofType(incomeActions.UPDATE_INCOME)
    .mergeMap((action: incomeActions.UpdateIncomeAction) => this._tokenService.patch(`incomes/${action.payload.id}`, action.payload)
      .map(income => new incomeActions.UpdateIncomeCompleteAction(income.json().message))
      .catch(error => of(new incomeActions.UpdateIncomeFailedAction(error.json().message))));

  @Effect()
  deleteIncome$: Observable<Action> = this._action$.ofType(incomeActions.DELETE_INCOME)
    .mergeMap((action: incomeActions.DeleteIncomeAction) => this._tokenService.delete(`incomes/${action.payload.id}`)
      .map(income => new incomeActions.DeleteIncomeCompleteAction(income.json()))
      .catch(error => of(new incomeActions.DeleteIncomeFailedAction(error.json().message))));

}
