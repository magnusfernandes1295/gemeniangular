import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as transactionActions from '../actions/transaction.actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class TransactionEffects {
  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) { }

  @Effect()
  fetchAllTransactions$: Observable<Action> = this._action$.ofType(transactionActions.FETCH_ALL_TRANSACTIONS)
    .mergeMap((action: transactionActions.FetchAllTransactionsAction) => this._tokenService.post('transactions/list', action.payload)
      .map(response => new transactionActions.FetchAllTransactionsCompleteAction({
        data: response.json().message,
        total: response.headers.get('total'),
        per_page: response.headers.get('per-page')
      }))
      .catch((error) => of(new transactionActions.FetchAllTransactionsFailedAction(error.json().message))));

  @Effect()
  fetchTransaction$: Observable<Action> = this._action$.ofType(transactionActions.FETCH_TRANSACTION)
    .mergeMap((action: transactionActions.FetchTransactionAction) => this._tokenService.get(`transactions/${action.payload}`)
      .map(device => new transactionActions.FetchTransactionCompleteAction(device.json().message))
      .catch((error) => of(new transactionActions.FetchTransactionFailedAction(error.json().message))));

}
