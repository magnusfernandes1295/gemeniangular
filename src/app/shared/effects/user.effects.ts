import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as fromUser from '../actions/user.actions';
import * as fromCertificate from '../actions/certificate.actions';
import * as fromDevice from '../actions/device.actions';
import * as fromExpense from '../actions/expense.actions';
import * as fromFeedback from '../actions/feedback.actions';
import * as fromIncome from '../actions/income.actions';
import * as fromInventory from '../actions/inventory.actions';
import * as fromPurchaseOrder from '../actions/purchase-order.actions';
import * as fromReceiveNote from '../actions/receive-note.actions';
import * as fromRequisitionOrder from '../actions/requisition-order.actions';
import * as fromSalarySlip from '../actions/salary-slip.actions';
import * as fromTransaction from '../actions/transaction.actions';
import * as fromVehicle from '../actions/vehicle.actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class UserEffects {
  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) { }

  @Effect()
  loginUser$: Observable<Action> = this._action$.ofType(fromUser.LOGIN)
    .mergeMap((action: fromUser.LoginAction) => this._tokenService.signIn(action.payload)
      .map(user => {
        this._router.navigate(['/']);
        return new fromUser.LoginCompleteAction(user.json().data);
      })
      .catch(error => of(new fromUser.LoginFailureAction(error.json()))));

  @Effect()
  validateToken$: Observable<Action> = this._action$.ofType(fromUser.VALIDATE_TOKEN)
    .mergeMap((action: fromUser.ValidateTokenAction) => this._tokenService.validateToken()
      .map(user => new fromUser.ValidateTokenCompleteAction(user.json().data))
      .catch(error => {
        this._tokenService.signOut();
        fromUser.SignoutUserAction;
        this._router.navigate(['/login']);
        return of(new fromUser.ValidateTokenFailedAction(error.json()));
      }));

  @Effect()
  signoutUser$: Observable<Action> = this._action$.ofType(fromUser.SIGNOUT)
    .mergeMap((action: fromUser.SignoutUserAction) => this._tokenService.signOut()
      .mergeMap(response => {
        this._router.navigate(['/login']);
        return [
          new fromUser.SignoutUserCompleteAction,
          new fromUser.ClearUserStateAction,
          new fromCertificate.ClearCertificateStateAction,
          new fromDevice.ClearDeviceStateAction,
          new fromExpense.ClearExpenseStateAction,
          new fromFeedback.ClearFeedbackStateAction,
          new fromIncome.ClearIncomeStateAction,
          new fromInventory.ClearInventoryStateAction,
          new fromPurchaseOrder.ClearPurchaseOrderStateAction,
          new fromReceiveNote.ClearReceiveNoteStateAction,
          new fromRequisitionOrder.ClearRequisitionOrderStateAction,
          new fromSalarySlip.ClearSalarySlipStateAction,
          new fromTransaction.ClearTransactionStateAction,
          new fromVehicle.ClearVehicleStateAction
        ];
      })
      .catch(error => of(new fromUser.SignoutUserFailedAction(error.json()))));
  
  @Effect()
  createNewUser$: Observable<Action> = this._action$.ofType(fromUser.CREATE_NEW_USER)
    .mergeMap((action: fromUser.CreateNewUserAction) => this._tokenService.post('users', action.payload)
      .mergeMap(response => [new fromUser.CreateNewUserCompleteAction(response.json().message), new fromUser.CloseUserModalAction])
      .catch(error => of(new fromUser.CreateNewUserFailedAction(error.json().message))));
    
  @Effect()
  updateUser$: Observable<Action> = this._action$.ofType(fromUser.UPDATE_USER)
    .mergeMap((action: fromUser.UpdateUserAction) => this._tokenService.patch(`users/${action.payload.id}`, action.payload)
      .mergeMap(response => [new fromUser.UpdateUserCompleteAction(response.json().message), new fromUser.CloseUserModalAction])
      .catch(error => of(new fromUser.UpdateUserFailedAction(error.json().message))));
    
  @Effect()
  changePassword$: Observable<Action> = this._action$.ofType(fromUser.CHANGE_PASSWORD)
    .mergeMap((action: fromUser.ChangePasswordAction) => this._tokenService.patch(`users/${action.payload.user.id}`, action.payload)
      .mergeMap(response => [new fromUser.UpdateUserCompleteAction(response.json().message), new fromUser.CloseUserModalAction])
      .catch(error => of(new fromUser.UpdateUserFailedAction(error.json().message))));

  @Effect()
  deleteUser$: Observable<Action> = this._action$.ofType(fromUser.DELETE_USER)
    .mergeMap((action: fromUser.DeleteUserAction) => this._tokenService.delete(`users/${action.payload}`)
      .map(response => new fromUser.DeleteUserCompleteAction(response.json()))
      .catch(error => of(new fromUser.DeleteUserFailedAction(error.json().message))));
  
  @Effect()
  fetchAllUsers$: Observable<Action> = this._action$.ofType(fromUser.FETCH_ALL_USERS)
    .mergeMap((action: fromUser.FetchAllUsersAction) => this._tokenService.get('users')
      .map(response => new fromUser.FetchAllUsersCompleteAction(response.json().message))
      .catch(error => of(new fromUser.FetchAllUsersFailedAction(error.json().message))));

  @Effect()
  fetchUser$: Observable<Action> = this._action$.ofType(fromUser.FETCH_USER)
    .mergeMap((action: fromUser.FetchUserAction) => this._tokenService.get(`users/${action.payload}`)
      .map(response => new fromUser.FetchUserCompleteAction(response.json().message))
      .catch(error => of(new fromUser.FetchUserFailedAction(error.json().message))));
}
