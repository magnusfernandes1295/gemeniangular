import { Router } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { of } from 'rxjs/observable/of';

import * as fromVehicle from '../actions/vehicle.actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class VehicleEffects {
  constructor(
    private _action$: Actions,
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) { }

  @Effect()
  createVehicle$: Observable<Action> = this._action$.ofType(fromVehicle.CREATE_VEHICLE)
    .mergeMap((action: fromVehicle.CreateVehicleAction) => this._tokenService.post('vehicles', action.payload)
      .mergeMap(response => [new fromVehicle.CreateVehicleCompleteAction(response.json().message), new fromVehicle.CloseVehicleModal])
      .catch(error => of(new fromVehicle.CreateVehicleFailedAction(error.json().message))));

  @Effect()
  fetchAllVehicles$: Observable<Action> = this._action$.ofType(fromVehicle.FETCH_ALL_VEHICLES)
    .mergeMap((action: fromVehicle.FetchAllVehiclesAction) => this._tokenService.get('vehicles')
      .map(vehicles => new fromVehicle.FetchAllVehiclesCompleteAction(vehicles.json().message))
      .catch(error => of(new fromVehicle.FetchAllVehiclesFailedAction(error.json().message))));

  @Effect()
  fetchVehicle$: Observable<Action> = this._action$.ofType(fromVehicle.FETCH_VEHICLE)
    .mergeMap((action: fromVehicle.FetchVehicleAction) => this._tokenService.get(`vehicles/${action.payload}`)
      .map(vehicles => new fromVehicle.FetchVehicleCompleteAction(vehicles.json().message))
      .catch(error => of(new fromVehicle.FetchVehicleFailedAction(error.json().message))));

  @Effect()
  updateVehicle$: Observable<Action> = this._action$.ofType(fromVehicle.UPDATE_VEHICLE)
    .mergeMap((action: fromVehicle.UpdateVehicleAction) => this._tokenService.patch(`vehicles/${action.payload.vehicle.id}`, action.payload)
      .mergeMap(vehicles => [new fromVehicle.UpdateVehicleCompleteAction(vehicles.json().message), new fromVehicle.CloseVehicleModal])
      .catch(error => of(new fromVehicle.UpdateVehicleFailedAction(error.json().message))));

  @Effect()
  deleteVehicle$: Observable<Action> = this._action$.ofType(fromVehicle.DELETE_VEHICLE)
    .mergeMap((action: fromVehicle.DeleteVehicleAction) => this._tokenService.delete(`vehicles/${action.payload}`)
      .map(vehicles => new fromVehicle.DeleteVehicleCompleteAction(vehicles.json()))
      .catch(error => of(new fromVehicle.DeleteVehicleFailedAction(error.json().message))));
  
  @Effect()
  deleteVehicleIcat$: Observable<Action> = this._action$.ofType(fromVehicle.DELETE_VEHICLE_ICAT)
    .mergeMap((action: fromVehicle.DeleteVehicleIcatAction) => this._tokenService.delete(`vehicles/${action.payload.vehicle_id}/icats/${action.payload.icat_id}`)
      .mergeMap(response => [new fromVehicle.DeleteVehicleIcatCompleteAction(response.json()), new fromVehicle.CloseVehicleModal])
      .catch(error => of(new fromVehicle.DeleteVehicleIcatFailedAction(error.json().message))));

  @Effect()
  updateVehicleIcat$: Observable<Action> = this._action$.ofType(fromVehicle.UPDATE_VEHICLE_ICAT)
    .mergeMap((action: fromVehicle.UpdateVehicleIcatAction) => this._tokenService.post(`vehicles/${action.payload.id}/icats`, {vehicle: {icats: action.payload.icats}})
      .mergeMap(response => [new fromVehicle.UpdateVehicleIcatCompleteAction(response.json().message), new fromVehicle.CloseVehicleModal])
      .catch(error => of(new fromVehicle.UpdateVehicleIcatFailedAction(error.json().message))));
}
