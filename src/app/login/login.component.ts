import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { Angular2TokenService } from 'angular2-token';
import { Router } from '@angular/router';

import * as fromRoot from '../shared/reducers';
import * as userActions from '../shared/actions/user.actions';
import { Store, State } from '@ngrx/store';
import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _tokenService: Angular2TokenService,
    private _store: Store<fromRoot.State>
  ) {
  }

  buildForm() {
    this.loginForm = this._fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }
  
  get email(): FormControl {
    return this.loginForm.get('email') as FormControl;
  }
  
  get password(): FormControl {
    return this.loginForm.get('password') as FormControl;
  }
  
  ngOnInit() {
    this.buildForm()
    this._tokenService.userSignedIn() ? this._router.navigate(['/dashboard']) : null
  }

  signIn() {
    this.loginForm.invalid ?
      swal('Warning!', 'Please enter your email and password!', 'warning') :
      this._store.dispatch(new userActions.LoginAction(this.loginForm.value))
  }

}
