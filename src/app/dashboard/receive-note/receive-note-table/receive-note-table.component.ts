import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import swal from 'sweetalert2';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PaginationInstance } from 'ngx-pagination';

import * as fromRoot from '../../../shared/reducers';
import * as noteActions from '../../../shared/actions/receive-note.actions';
import { ReceiveNote, PageData } from '../../../shared/models';

declare var $:any;

@Component({
  selector: 'receive-note-table',
  templateUrl: './receive-note-table.component.html',
  styleUrls: ['./receive-note-table.component.sass']
})
export class ReceiveNoteTableComponent implements OnInit, OnChanges {

  @Input('type') type: string = '';
  @Input('start_date') start_date: any;
  @Input('end_date') end_date: any;
  @Output() openNoteViewModal: EventEmitter<any> = new EventEmitter();
  @Output() openNoteModal: EventEmitter<any> = new EventEmitter();
  public notes: ReceiveNote[] = [];

  public pageData: BehaviorSubject<PageData> = new BehaviorSubject(new PageData({}));
  public paginateConfig: PaginationInstance = {
    id: 'transactionsPaginate',
    itemsPerPage: this.pageData.value.per_page,
    currentPage: 1,
    totalItems: this.pageData.value.total
  }
  public loading: boolean = false;

  constructor(
    private _store: Store<fromRoot.State>
  ) {
    this.pageData.subscribe(data => {
      this.paginateConfig.itemsPerPage = data.per_page;
      this.paginateConfig.totalItems = data.total;
    });
  }

  ngOnInit() {
    this._store.select(fromRoot.getReceiveNotes).subscribe(notes => {
      this.loading = false;
      this.notes = notes.filter(note => note.status == this.type);
    });
    this._store.select(fromRoot.getReceiveNotePageStatus).subscribe(pageData => this.pageData.next(pageData));
  }

  ngOnChanges() {
    this.loading = true;
    let formData = {
      start: this.start_date ? this.start_date.formatted : '',
      end: this.end_date ? this.end_date.formatted : '',
      status: this.type,
      page: this.paginateConfig.currentPage,
      per_page: 15
    };
    formData.start == '' ? delete formData.start : null;
    formData.end == '' ? delete formData.end : null;
    this._store.dispatch(new noteActions.FilterReceiveNoteAction(formData));
  }

  viewNote(id: number) {
    this.openNoteViewModal.emit(this.notes.find(note => note.id == id));
  }

  updateNote(event, id: number) {
    event.stopPropagation();
    this.openNoteModal.emit(this.notes.find(note => note.id == id));
  }

  deleteNote(event, id: number) {
    event.stopPropagation();
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete receive note!'
    }).then((result) => {
      if (result.value) {
        this._store.dispatch(new noteActions.DeleteReceiveNoteAction(id))
      }
    });
  }

  getPage(page: number) {
    this.paginateConfig.currentPage = page;
    this.ngOnChanges();
  }
  
}
