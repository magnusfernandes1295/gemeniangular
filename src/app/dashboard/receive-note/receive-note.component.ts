import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromRoot from './../../shared/reducers';
import * as noteActions from './../../shared/actions/receive-note.actions';
import * as inventoryActions from './../../shared/actions/inventory.actions';
import { ReceiveNote, ReceiveNoteParticular } from '../../shared/models';
import { INgxMyDpOptions } from 'ngx-mydatepicker';

declare var $:any;

@Component({
  selector: 'app-receive-note',
  templateUrl: './receive-note.component.html',
  styleUrls: ['./receive-note.component.sass']
})

export class ReceiveNoteComponent implements OnInit {

  public addNote: boolean = false;
  public noteForm: FormGroup;
  public filterForm: FormGroup;
  public currentNote: ReceiveNote = new ReceiveNote({});
  public deletedItems: number[] = [];
  public type: string = 'can_modify';
  start_options: INgxMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    disableSince: {
      year: (new Date()).getFullYear(),
      month: (new Date()).getMonth() + 1,
      day: (new Date()).getDate() + 1
    }
  };

  constructor(
    private _store: Store<fromRoot.State>,
    private _fb: FormBuilder
  ) { }

  ngOnInit() {
    this.initializers();
    this.buildForm();
    this._store.dispatch(new inventoryActions.FetchAllInventoryAction);
    this.noteForm.valueChanges.subscribe(value => {
      let total : number = 0;
      this.rn_particulars.controls.map(group => total += Number(group.get('total').value));
      this.freight_total.patchValue(+(this.freight.value * (1 + (this.freight_gst.value * 0.01))).toFixed(2), {emitEvent: false});
      this.total.patchValue(Math.ceil(this.freight_total.value + this.expenses.value + Number(total.toFixed(2))), {emitEvent: false});
    });
  }

  buildForm() {
    this.noteForm = this._fb.group({
      freight: [null, [Validators.min(0)]],
      freight_gst: [null, [Validators.min(0), Validators.max(80)]],
      freight_total: [null],
      freight_gstn: [null, [Validators.minLength(15), Validators.maxLength(15), Validators.pattern("[a-zA-Z0-9]+")]],
      expenses: [null, [Validators.required, Validators.min(0)]],
      gstn: [null, [Validators.minLength(15), Validators.maxLength(15), Validators.pattern("[a-zA-Z0-9]+")]],
      total: [null, Validators.required],
      freight_switch: true,
      rn_particulars: this._fb.array([])
    });
    this.filterForm = this._fb.group({
      start_date: [''],
      end_date: ['']
    });
  }

  addParticular(data: ReceiveNoteParticular) {
    this.rn_particulars.push(this._fb.group({
      id: [data ? data.id : null],
      inventory_item_id: [data ? data.inventory_item.id : null, Validators.required],
      quantity: [data ? data.quantity : null, [Validators.required, Validators.min(0)]],
      unit_price: [data ? data.unit_price : null, [Validators.required, Validators.min(0)]],
      gst: [data ? data.gst : null, [Validators.required, Validators.min(0), Validators.max(80)]],
      total: [data ? data.total : null, Validators.required]
    }));
  }

  deleteParticular(index: number) {
    this.deletedItems.push(this.rn_particulars.at(index).get('id').value);
    this.rn_particulars.removeAt(index);
  }

  get start_date() {
    return this.filterForm.get('start_date').value;
  }

  get end_date() {
    return this.filterForm.get('end_date').value;
  }

  get freight(): FormControl {
    return this.noteForm.get('freight') as FormControl;
  }

  get freight_gst(): FormControl {
    return this.noteForm.get('freight_gst') as FormControl;
  }

  get freight_total(): FormControl {
    return this.noteForm.get('freight_total') as FormControl;
  }

  get freight_gstn(): FormControl {
    return this.noteForm.get('freight_gstn') as FormControl;
  }

  get expenses(): FormControl {
    return this.noteForm.get('expenses') as FormControl;
  }

  get gstn(): FormControl {
    return this.noteForm.get('gstn') as FormControl;
  }

  get total(): FormControl {
    return this.noteForm.get('total') as FormControl;
  }

  get freight_switch(): FormControl {
    return this.noteForm.get('freight_switch') as FormControl;
  }

  get rn_particulars(): FormArray {
    return this.noteForm.get('rn_particulars') as FormArray;
  }

  viewNote(note: ReceiveNote) {
    this.currentNote = note;
    this._store.dispatch(new noteActions.OpenReceiveNoteDetailModalAction);
  }

  openNoteModal(note: ReceiveNote) {
    this.currentNote = note;
    this.noteForm.reset();
    this.rn_particulars.controls = [];
    if (note) {
      this.deletedItems = [];
      this.addNote = false;
      this.noteForm.patchValue(note, {emitEvent: false});
      note.rn_particulars.map(particular => this.addParticular(particular));
    } else {
      this.addParticular(new ReceiveNoteParticular({}));
      this.addNote = true;
    }
    this._store.dispatch(new noteActions.OpenReceiveNoteModalAction);
  }

  openNoteViewModal(note: ReceiveNote) {
    this.currentNote = note;
    this._store.dispatch(new noteActions.OpenReceiveNoteDetailModalAction);
  }

  closeNoteModal() {
    this._store.dispatch(new noteActions.CloseReceiveNoteModalAction);
  }

  saveChanges() {
    let formData: any = this.noteForm.value;
    if (this.addNote) {
      this._store.dispatch(new noteActions.CreateNewReceiveNoteAction({
        receive_note: formData
      }));
    } else {
      formData["id"] = this.currentNote.id;
      this.deletedItems.map(id => {
        formData.rn_particulars.push({
          id: id,
          _destroy: 1
        })
      });
      this._store.dispatch(new noteActions.UpdateReceiveNoteAction({
        receive_note: formData
      }))
    }
  }

  confirmNote(id: number) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, confirm receive note!'
    }).then((result) => {
      if (result.value) {
        this._store.dispatch(new noteActions.ConfirmReceiveNoteAction(id))
      }
    });
  }

  initializers() {
    this._store.select(fromRoot.showNoteModal).subscribe(res => res ? $('#receiveNoteModal').modal("show") : $('#receiveNoteModal').modal("hide"))
    $('#receiveNoteModal').on('hidden.bs.modal', () => this.closeNoteModal());

    this._store.select(fromRoot.showNoteDetailModal).subscribe(res => res ? $('#noteModal').modal("show") : $('#noteModal').modal("hide"))
    $('#noteModal').on('hidden.bs.modal', () => this.closeNoteModal());
  }
}
