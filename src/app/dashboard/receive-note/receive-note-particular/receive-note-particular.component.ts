import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

import * as fromRoot from '../../../shared/reducers';
import * as rnActions from '../../../shared/actions/receive-note.actions';
import { Inventory } from "../../../shared/models";
import { Store } from "@ngrx/store";

declare var $: any;

@Component({
  selector: "[note-particular]",
  templateUrl: "./receive-note-particular.component.html",
  styleUrls: ["./receive-note-particular.component.sass"]
})
export class ReceiveNoteParticularComponent implements OnInit {
  
  @Input("group") public rnParticularForm: FormGroup;
  public inventories: Inventory[] = [];

  constructor(
    private _store: Store<fromRoot.State>
  ) {}

  ngOnInit() {
    this._store.select(fromRoot.getAllInventory).subscribe(inventory => this.inventories = inventory);
    this.rnParticularForm.valueChanges.subscribe(value => 
      this.total.patchValue(((this.unit_price.value * this.quantity.value) * (1 + (this.gst.value * .01))).toFixed(2), {emitEvent: false}));
  }

  get unit_price() {
    return this.rnParticularForm.controls["unit_price"] as FormControl;
  }

  get quantity() {
    return this.rnParticularForm.controls["quantity"] as FormControl;
  }

  get gst() {
    return this.rnParticularForm.controls["gst"] as FormControl;
  }

  get total() {
    return this.rnParticularForm.controls["total"] as FormControl;
  }
}
