import { ReactiveFormsModule } from '@angular/forms';
import { ReceiveNoteParticularComponent } from './receive-note-particular/receive-note-particular.component';
import { ReceiveNoteComponent } from './receive-note.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReceiveNoteTableComponent } from './receive-note-table/receive-note-table.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoadersCssModule } from 'angular2-loaders-css';

const routes: Routes = [
  {
    path: '',
    component: ReceiveNoteComponent
  }
];

@NgModule({
  declarations: [
    ReceiveNoteComponent,
    ReceiveNoteParticularComponent,
    ReceiveNoteTableComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxPaginationModule,
    LoadersCssModule,
    NgxMyDatePickerModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class ReceiveNoteModule { }
