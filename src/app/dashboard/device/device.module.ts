import { DeviceComponent } from './device.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SldComponent } from './sld/sld.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgArrayPipesModule } from 'ngx-pipes';
import { NgxPaginationModule } from 'ngx-pagination';
import { DeviceTableComponent } from './device-table/device-table.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: DeviceComponent
  }
];

@NgModule({
  declarations: [
    DeviceComponent,
    SldComponent,
    DeviceTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgArrayPipesModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxPaginationModule,
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class DeviceModule { }
