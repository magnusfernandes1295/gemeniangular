import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Store } from "@ngrx/store";

import * as fromRoot from "../../shared/reducers";
import * as deviceActions from "../../shared/actions/device.actions";
import { Device, User } from "../../shared/models";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";

declare var $:any;

@Component({
  selector: "app-device",
  templateUrl: "./device.component.html",
  styleUrls: ["./device.component.sass"]
})
export class DeviceComponent implements OnInit {

  public search: number;
  public addDevice: boolean = false;
  public deviceForm: FormGroup;
  public loggedUser: User = new User({});
  public type: string = 'unsold';

  constructor(
    private _store: Store<fromRoot.State>,
    private _fb: FormBuilder,
    private _cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.buildForm();
    this.initializers();
    this._cdr.detectChanges();
    this._store.dispatch(new deviceActions.FetchAllDevicesAction);
    this._store.select(fromRoot.loggedUser).subscribe(user => {
      this.loggedUser = user;
      if (this.loggedUser.role == 'distributor' || this.loggedUser.role == 'dealer') {
        this.type = 'sold';
      }
    });
    this.deviceForm.valueChanges.subscribe(value => {
      if (this.checkDuplicateInObject("sld_number", value.device)) {
        this.deviceForm.setErrors({"duplicates": true});
      } else {
        this.deviceForm.setErrors(null);
      }
    });
  }

  buildForm() {
    this.deviceForm = this._fb.group({
      device: this._fb.array([])
    });
  }

  get device(): FormArray {
    return this.deviceForm.get('device') as FormArray;
  }

  addSld() {
    this.device.push(this._fb.group({
      sld_number: ['', [Validators.required, Validators.minLength(4)]]
    }));
  }

  deleteSld(i: number) {
    this.device.removeAt(i);
  }

  enterPressed(id: number) {
    if (this.device.length-1 == id) {
      this.addSld();
    }
  }

  openDeviceModal(device: Device) {
    this.deviceForm.reset();
    while(this.device.length > 0) {
      this.device.removeAt(0);
    }
    if (device) {
      this.addDevice = false;
    } else {
      this.addSld();
      this.addDevice = true;
    }
    this._store.dispatch(new deviceActions.OpenDeviceModalAction);
  }

  closeDeviceModal() {
    this._store.dispatch(new deviceActions.CloseDeviceModalAction);
  }

  checkDuplicateInObject(propertyName: string, inputArray: any[]) {
    let seenDuplicate = false, testObject = {};
    if (inputArray != undefined)
      inputArray.map((item) => {
        let itemPropertyName = item[propertyName];
        if (itemPropertyName in testObject) {
          testObject[itemPropertyName].duplicate = true;
          item.duplicate = true;
          seenDuplicate = true;
        }
        else {
          testObject[itemPropertyName] = item;
          delete item.duplicate;
        }
      });
    return seenDuplicate;
  }

  saveChanges() {
    this._store.dispatch(new deviceActions.CreateDeviceAction(this.deviceForm.value));
  }

  initializers() {
    $('#deviceModal').on('hidden.bs.modal', () => this.closeDeviceModal());
    this._store.select(fromRoot.showDeviceModal).subscribe(res => res ? $('#deviceModal').modal('show') : $('#deviceModal').modal('hide'));
  }
}
