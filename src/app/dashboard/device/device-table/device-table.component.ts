import { Component, OnInit, Input, EventEmitter, ChangeDetectionStrategy, OnChanges } from '@angular/core';
import { Store } from "@ngrx/store";
import swal from 'sweetalert2';

import * as fromRoot from "../../../shared/reducers";
import * as deviceActions from "../../../shared/actions/device.actions";
import { Device } from '../../../shared/models';
import { PaginationInstance } from 'ngx-pagination';
import { filter } from 'rxjs/operator/filter';

@Component({
  selector: 'device-table',
  templateUrl: './device-table.component.html',
  styleUrls: ['./device-table.component.sass']
})
export class DeviceTableComponent implements OnInit, OnChanges {

  @Input('type') type: string;
  @Input('search') search: number;
  @Input('openDeviceModal') openDeviceModal: EventEmitter<any> = new EventEmitter();
  public devices: Device[] = [];
  public filteredDevices: Device[] = [];
  public config: PaginationInstance = {
    itemsPerPage: 20,
    currentPage: 1
  };

  constructor(
    private _store: Store<fromRoot.State>
  ) { }

  ngOnInit() {
    this._store.select(fromRoot.getAllDevices).subscribe(devices => {
      this.devices = devices;
      this.ngOnChanges();
    });
  }

  ngOnChanges() {
    this.filteredDevices = this.devices.filter(device => device.status == this.type);
  }

  updateDevice(id: number) {
    this.openDeviceModal.emit(this.devices.find(device => device.id == id));
  }

  deleteDevice(id: number) {
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete device!"
    }).then(result => {
      if (result.value) {
        this._store.dispatch(new deviceActions.DeleteDeviceAction(id));
      }
    });
  }
}
