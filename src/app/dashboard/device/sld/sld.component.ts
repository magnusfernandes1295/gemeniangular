import { Component, OnInit, Input, EventEmitter, Output, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../../shared/reducers';
import { DeviceService } from '../../../shared/services/device.service';
import * as deviceActions from '../../../shared/actions/device.actions';
import { Device } from '../../../shared/models/index';

declare var $:any;

@Component({
  selector: 'sld-device',
  templateUrl: './sld.component.html',
  styleUrls: ['./sld.component.sass']
})
export class SldComponent implements OnInit, AfterViewInit {

  @Input('group') sldForm: FormGroup;
  @Input('position') position: number;
  @Output() enterPressed: EventEmitter<any> = new EventEmitter();
  public flag: boolean = false;
  
  constructor(
    private _store: Store<fromRoot.State>,
    private _deviceService: DeviceService
  ) { }

  ngOnInit() {
    let timex;
    this.sldForm.valueChanges.subscribe(change => {
      clearTimeout(timex);
      timex = setTimeout(() => {
        this.checkSld(change.sld_number);
      }, 500);
    });
  }

  ngAfterViewInit() {
    $('#sld_number' + this.position).focus();
  }

  get sld_number(): FormControl {
    return this.sldForm.get('sld_number') as FormControl;
  }

  keyUp() {
    this.enterPressed.emit(this.position);
  }

  checkSld(sld_number: string) {
    if (sld_number) {
      this._deviceService.checkIfDeviceExists(sld_number).subscribe(res => {
        if (res) {
          this.sldForm.setErrors({"exists": true})
        } else {
          this.sldForm.setErrors(null);
        }
      });
    }
  }
}
