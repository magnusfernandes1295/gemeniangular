import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import {Store} from '@ngrx/store';
import swal from 'sweetalert2';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PaginationInstance } from 'ngx-pagination';

import * as fromRoot from '../../../shared/reducers';
import * as feedbackActions from '../../../shared/actions/feedback.actions';
import { Feedback, User, PageData } from '../../../shared/models';

@Component({
  selector: 'feedback-table',
  templateUrl: './feedback-table.component.html',
  styleUrls: ['./feedback-table.component.sass']
})
export class FeedbackTableComponent implements OnInit, OnChanges {

  @Input('type') type: string;
  @Input('category') category: string;
  @Input('user') user: string;
  @Input('start_date') start_date: any;
  @Input('end_date') end_date: any;
  @Output() openFeedDetail: EventEmitter<any> = new EventEmitter<any>();
  public feedBack: Feedback[] = [];
  public loggedUser: User = new User({});

  public pageData: BehaviorSubject<PageData> = new BehaviorSubject(new PageData({}));
  public paginateConfig: PaginationInstance = {
    id: 'transactionsPaginate',
    itemsPerPage: this.pageData.value.per_page,
    currentPage: 1,
    totalItems: this.pageData.value.total
  }
  public loading: boolean = false;

  constructor(
    private _store: Store<fromRoot.State>
  ) {
    this.pageData.subscribe(data => {
      this.paginateConfig.itemsPerPage = data.per_page;
      this.paginateConfig.totalItems = data.total;
    });
  }

  ngOnInit() {
    this._store.select(fromRoot.loggedUser).subscribe(user => this.loggedUser = user);
    this._store.select(fromRoot.getAllFeedbacks).subscribe(feedback => {
      this.loading = false;
      this.feedBack = (this.type == 'all' ? feedback : feedback.filter(feed => feed.status == this.type));
      this.feedBack = this.category ? this.feedBack.filter(feed => feed.category == this.category) : this.feedBack;
    });
    this._store.select(fromRoot.getFeedbackPageStatus).subscribe(pageData => this.pageData.next(pageData));
  }

  ngOnChanges() {
    this.loading = true;
    let formData = {
      start: this.start_date ? this.start_date.formatted : '',
      end: this.end_date ? this.end_date.formatted : '',
      status: this.type,
      category: this.category,
      creator_id: this.user,
      page: this.paginateConfig.currentPage,
      per_page: 15
    };
    formData.start == '' ? delete formData.start : null;
    formData.end == '' ? delete formData.end : null;
    formData.category ? null : delete formData.category;
    formData.creator_id ? null : delete formData.creator_id;
    formData.status == 'all' ? delete formData.status : null;
    this._store.dispatch(new feedbackActions.FilterFeedbackAction(formData));
  }

  deleteFeedback(event, id: number) {
    event.stopPropagation();
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete feedback!"
    }).then(result => {
      if (result.value) {
        this._store.dispatch(new feedbackActions.DeleteFeedbackAction(id));
      } 
    });
  }

  openFeed(id: number) {
    this.openFeedDetail.emit(this.feedBack.find(feed => feed.id == id));
  }

  getPage(page: number) {
    this.paginateConfig.currentPage = page;
    this.ngOnChanges();
  }

}
