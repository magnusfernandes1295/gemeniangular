import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {Store} from '@ngrx/store';

import * as fromRoot from '../../shared/reducers';
import * as feedbackActions from '../../shared/actions/feedback.actions';
import * as userActions from '../../shared/actions/user.actions';
import { User, Feedback } from '../../shared/models';
import { INgxMyDpOptions } from 'ngx-mydatepicker';

declare var $:any;

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.sass']
})
export class FeedbackComponent implements OnInit {

  public feedbackForm: FormGroup;
  public filterForm: FormGroup;
  public addFeed: boolean;
  public currentFeed: Feedback = new Feedback({});
  public loggedUser: User = new User({});
  public type: string = 'unread';
  public users: User[] = [];
  public categories: any[] = [
    {
      display: 'Enquiry',
      value: 'enquiry'
    },
    {
      display: 'Suggestion',
      value: 'suggestion'
    },
    {
      display: 'Grievance',
      value: 'grievance'
    },
    {
      display: 'Other',
      value: 'other'
    }
  ];
  start_options: INgxMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    disableSince: {
      year: (new Date()).getFullYear(),
      month: (new Date()).getMonth() + 1,
      day: (new Date()).getDate() + 1
    }
  };

  constructor(
    private _store: Store<fromRoot.State>,
    public _fb: FormBuilder
  ) {
    this._store.select(fromRoot.loggedUser).subscribe(user => this.loggedUser = user);
  }

  ngOnInit() {
    this.buildForm();
    this.initializers();
    this._store.dispatch(new userActions.FetchAllUsersAction);
    this._store.select(fromRoot.getUsers).subscribe(users => this.users = users.filter(user => user.role == 'distributor' || user.role == 'dealer'));
  }

  buildForm() {
    this.feedbackForm = this._fb.group({
      category: [null, Validators.required],
      subject: [null, [Validators.required, Validators.maxLength(40)]],
      body: [null, Validators.required]
    });
    this.filterForm = this._fb.group({
      start_date: [''],
      end_date: [''],
      category: [null],
      user: [null]
    });
  }

  get category(): FormControl {
    return this.feedbackForm.get('category') as FormControl;
  }

  get subject(): FormControl {
    return this.feedbackForm.get('subject') as FormControl;
  }

  get body(): FormControl {
    return this.feedbackForm.get('body') as FormControl;
  }

  get category_filter(): FormControl {
    return this.filterForm.get('category') as FormControl;
  }

  get user_filter(): FormControl {
    return this.filterForm.get('user') as FormControl;
  }

  get start_date() {
    return this.filterForm.get('start_date').value;
  }

  get end_date() {
    return this.filterForm.get('end_date').value;
  }

  saveChanges() {
    this._store.dispatch(new feedbackActions.CreateFeedbackAction(this.feedbackForm.value));
  }

  openFeedbackModal(feed: Feedback) {
    this.feedbackForm.reset();
    if (feed) {
      this.addFeed = false;
      this.currentFeed = feed;
      if ((this.loggedUser.role != 'distributor' && this.loggedUser.role != 'dealer') && feed.status == 'unread') {
        feed.status == 'unread' ? this._store.dispatch(new feedbackActions.MarkReadFeedbackAction(feed.id)) : null;
      }
    } else {
      this.addFeed = true;
    }
    this._store.dispatch(new feedbackActions.OpenFeedbackModal);
  }

  closeFeedbackModal() {
    this._store.dispatch(new feedbackActions.CloseFeedbackModal);
  }

  initializers() {
    $('#feedbackModal').on('hidden.bs.modal', () => this.closeFeedbackModal());
    this._store.select(fromRoot.showFeedbackModal).subscribe(res => res ? $('#feedbackModal').modal('show') : $('#feedbackModal').modal('hide'));
  }
}
