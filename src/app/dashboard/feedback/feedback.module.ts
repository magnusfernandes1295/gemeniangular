import { FeedbackComponent } from './feedback.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { FeedbackTableComponent } from './feedback-table/feedback-table.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoadersCssModule } from 'angular2-loaders-css';

const routes: Routes = [
  {
    path: '',
    component: FeedbackComponent
  }
];

@NgModule({
  declarations: [
    FeedbackComponent,
    FeedbackTableComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxPaginationModule,
    LoadersCssModule,
    NgxMyDatePickerModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class FeedbackModule { }
