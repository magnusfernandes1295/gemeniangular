import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferDevicesComponent } from './transfer-devices.component';

describe('TransferDevicesComponent', () => {
  let component: TransferDevicesComponent;
  let fixture: ComponentFixture<TransferDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
