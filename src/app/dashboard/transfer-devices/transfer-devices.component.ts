import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Store } from "@ngrx/store";
import swal from 'sweetalert2';

import * as fromRoot from "../../shared/reducers";
import * as userActions from "../../shared/actions/user.actions";
import * as deviceActions from "../../shared/actions/device.actions";
import { User, Device } from "../../shared/models";

declare var $:any;

@Component({
  selector: "app-transfer-devices",
  templateUrl: "./transfer-devices.component.html",
  styleUrls: ["./transfer-devices.component.sass"]
})
export class TransferDevicesComponent implements OnInit {
  public transferForm: FormGroup;
  public users: User[] = [];
  public devices: Device[] = [];

  constructor(
    private _fb: FormBuilder,
    private _store: Store<fromRoot.State>
  ) {}

  ngOnInit() {
    this.buildForm();
    this._store.dispatch(new userActions.FetchAllUsersAction);
    this._store.dispatch(new deviceActions.FetchAllDevicesAction);
    this._store.select(fromRoot.getAllDevices).subscribe(devices => this.devices = devices.filter(device => device.status != 'certified'));
    this._store.select(fromRoot.getDealersDistributors).subscribe(users => this.users = users);
  }

  buildForm() {
    this.transferForm = this._fb.group({
      user_id: [null, Validators.required],
      device_ids: [null, Validators.required]
    });
  }

  transferInit() {
    let formData: any = this.transferForm.value;
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, transfer devices!"
    }).then(result => {
      if (result.value) {
        this._store.dispatch(new deviceActions.TransferDeviceAction(formData));
        this.transferForm.reset();
      }
    });
  }
}
