import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromRoot from "../../shared/reducers";
import * as deviceActions from "../../shared/actions/device.actions";
import * as userActions from "../../shared/actions/user.actions";
import * as purchaseOrderActions from "../../shared/actions/purchase-order.actions";
import { Device, PurchaseOrder, User } from '../../shared/models';
import { CreatePurchaseOrderComponent } from './create-purchase-order/create-purchase-order.component';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { RtoService } from '../../shared/services/rto.service';

declare var $:any;

@Component({
  selector: 'app-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.sass']
})
export class PurchaseOrderComponent implements OnInit {

  public currentPurchaseOrder: PurchaseOrder = new PurchaseOrder({});
  public currentDispatchID: number = null;
  public confirmForm: FormGroup;
  public dispatchForm: FormGroup;
  public closeForm: FormGroup;
  public filterForm: FormGroup;
  public deviceList: Device[] = [];
  public loggedUser: User = new User({});
  public users: User[] = [];
  public type: string;
  public states: any[] = [];
  @ViewChild(CreatePurchaseOrderComponent) child:CreatePurchaseOrderComponent;
  start_options: INgxMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    disableSince: {
      year: (new Date()).getFullYear(),
      month: (new Date()).getMonth() + 1,
      day: (new Date()).getDate() + 1
    }
  };
  
  constructor(
    private _fb: FormBuilder,
    private _store: Store<fromRoot.State>,
    private _rtoService: RtoService
  ) {
    this._store.select(fromRoot.loggedUser).subscribe(user => {
      this.loggedUser = user;
      switch(user.role) {
        case 'store_purchases':
          this.type = 'can_modify';
          break;
        case 'accounts':
          this.type = 'opened';
          break;
        case 'store_dispatch':
          this.type = 'processing';
          this.fetchDevices();
          break;
        case 'store_logistics':
          this.type = 'dispatch_ready';
          break;
        default:
          this.type = 'can_modify';
          break;
      }
    });
  }

  ngOnInit() {
    this.buildForm();
    this.initializer();
    this.fetchUsers();
    this.states = this._rtoService.getStates();
    this._store.select(fromRoot.getAllDevices).subscribe(devices => this.deviceList = devices.filter(device => device.status == 'unsold'));
    this._store.select(fromRoot.getCurrentPurchaseOrder).subscribe(po => this.currentPurchaseOrder = po);
    this.confirmForm.valueChanges.subscribe(value => {
      this.amount_total.patchValue(Math.round(this.amount_paid.value + (this.amount_paid.value * this.amount_gst.value * 0.01)), {emitEvent: false});
    });
    this.closeForm.valueChanges.subscribe(value => {
      this.shipping_total.patchValue(Math.round(this.shipping_amt.value + (this.shipping_amt.value * this.shipping_gst.value * 0.01)), {emitEvent: false});
    });
  }

  buildForm() {
    this.confirmForm = this._fb.group({
      id: [null, Validators.required],
      amount_paid: [null, [Validators.required, Validators.min(0)]],
      amount_gst: [null, [Validators.required, Validators.min(0), Validators.max(80)]],
      amount_total: [null, [Validators.required, Validators.min(0)]]
    });
    this.dispatchForm = this._fb.group({
      device_ids: [[], Validators.required]
    });
    this.closeForm = this._fb.group({
      id: null,
      tracking_no: [null, Validators.required],
      shipping_date: [null, Validators.required],
      shipping_amt: [null, [Validators.required, Validators.min(0)]],
      shipping_gst: [null, [Validators.required, Validators.min(0), Validators.max(80)]],
      shipping_total: [null, Validators.required],
      shipping_gstn: [null, [Validators.minLength(15), Validators.maxLength(15), Validators.pattern("[a-zA-Z0-9]+")]]
    });
    this.filterForm = this._fb.group({
      start_date: [''],
      end_date: [''],
      state_code: [null],
      user: [null]
    });
  }

  fetchUsers() {
    this._store.dispatch(new userActions.FetchAllUsersAction);
    this._store.select(fromRoot.getUsers).subscribe(users => this.users = users);
  }

  fetchDevices() {
    this._store.dispatch(new deviceActions.FetchAllDevicesAction);
  }

  get amount_paid(): FormControl {
    return this.confirmForm.get('amount_paid') as FormControl;
  }
  
  get amount_gst(): FormControl {
    return this.confirmForm.get('amount_gst') as FormControl;
  }

  get amount_total(): FormControl {
    return this.confirmForm.get('amount_total') as FormControl;
  }

  get device_ids(): FormControl {
    return this.dispatchForm.get('device_ids') as FormControl;
  }

  get shipping_amt(): FormControl {
    return this.closeForm.get('shipping_amt') as FormControl;
  }
  get shipping_gst(): FormControl {
    return this.closeForm.get('shipping_gst') as FormControl;
  }
  get shipping_gstn(): FormControl {
    return this.closeForm.get('shipping_gstn') as FormControl;
  }
  get shipping_total(): FormControl {
    return this.closeForm.get('shipping_total') as FormControl;
  }

  get state_filter(): FormControl {
    return this.filterForm.get('state_code') as FormControl;
  }

  get user_filter(): FormControl {
    return this.filterForm.get('user') as FormControl;
  }

  get start_date() {
    return this.filterForm.get('start_date').value;
  }

  get end_date() {
    return this.filterForm.get('end_date').value;
  }

  showDetails(id: number) {
    this._store.dispatch(new purchaseOrderActions.OpenPurchaseOrderDetailModalAction(id));
  }
  
  openConfirmPO(po: PurchaseOrder) {
    this.confirmForm.reset();
    this._store.dispatch(new purchaseOrderActions.OpenPurchaseOrderConfirmModalAction);
    this.confirmForm.get('id').patchValue(po.id);
  }

  confirmPO() {
    let formData = {
      id: this.confirmForm.value.id,
      message: {
        purchase_order: this.confirmForm.value
      }
    }
    delete formData.message.purchase_order["id"];
    delete formData.message.purchase_order["amount_total"];
    this._store.dispatch(new purchaseOrderActions.ConfirmPurchaseOrderAction(formData));
  }

  openDispatchPO(id: number) {
    this.dispatchForm.reset();
    this._store.dispatch(new purchaseOrderActions.OpenPurchaseOrderDispatchModalAction(id));
    this.currentDispatchID = id;
  }

  dispatchPO() {
    this._store.dispatch(new purchaseOrderActions.DispatchPurchaseOrderAction({
      id: this.currentDispatchID,
      purchase_order: this.dispatchForm.value
    }));
    this.fetchDevices();
  }
  
  openClosePO(id: number) {
    this.closeForm.reset();
    this._store.dispatch(new purchaseOrderActions.OpenPurchaseOrderCloseModalAction);
    this.closeForm.get('id').patchValue(id);
  }

  closePO() {
    let formData = {
      id: this.closeForm.value.id,
      message: {
        purchase_order: this.closeForm.value
      }
    };
    delete formData.message.purchase_order["id"];
    delete formData.message.purchase_order["shipping_total"];
    formData.message.purchase_order["shipping_date"] = formData.message.purchase_order.shipping_date.jsdate;
    this._store.dispatch(new purchaseOrderActions.ClosePurchaseOrderAction(formData));
  }

  openUpdatePO(po: PurchaseOrder) {
    this.child.openPOUpdateModal(po);
  }

  closePOModal() {
    this._store.dispatch(new purchaseOrderActions.ClosePurchaseOrderModalAction);
  }

  initializer() {
    this._store.select(fromRoot.showDetailPO).subscribe(res => res ? $('#detailModal').modal('show') : $('#detailModal').modal('hide'));
    this._store.select(fromRoot.showConfirmPO).subscribe(res => res ? $('#confirmModal').modal('show') : $('#confirmModal').modal('hide'));
    this._store.select(fromRoot.showDispatchPO).subscribe(res => res ? $('#dispatchModal').modal('show') : $('#dispatchModal').modal('hide'));
    this._store.select(fromRoot.showClosePO).subscribe(res => res ? $('#closeModal').modal('show') : $('#closeModal').modal('hide'));
    $('#detailModal').on('hidden.bs.modal', () => this.closePOModal());
    $('#confirmModal').on('hidden.bs.modal', () => this.closePOModal());
    $('#dispatchModal').on('hidden.bs.modal', () => this.closePOModal());
    $('#closeModal').on('hidden.bs.modal', () => this.closePOModal());
  }
}
