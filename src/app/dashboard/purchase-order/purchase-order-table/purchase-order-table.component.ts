import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import swal from "sweetalert2";
import 'rxjs/add/operator/take';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PaginationInstance } from 'ngx-pagination';

import * as fromRoot from "../../../shared/reducers";
import * as purchaseOrderActions from "../../../shared/actions/purchase-order.actions";
import { PurchaseOrder, User, PageData } from '../../../shared/models';
import { PurchaseOrderService } from '../../../shared/services/purchase-order.service';


@Component({
  selector: 'purchase-order-table',
  templateUrl: './purchase-order-table.component.html',
  styleUrls: ['./purchase-order-table.component.sass']
})
export class PurchaseOrderTableComponent implements OnInit, OnChanges {

  @Input('type') type: string;
  @Input('start_date') start_date: any;
  @Input('end_date') end_date: any;
  @Input('state') state: string;
  @Input('user') user: string;
  @Output() openDispatchPO: EventEmitter<any> = new EventEmitter<any>();
  @Output() openClosePO: EventEmitter<any> = new EventEmitter<any>();
  @Output() openDetailPO: EventEmitter<any> = new EventEmitter<any>();
  @Output() openConfirmPO: EventEmitter<any> = new EventEmitter<any>();
  @Output() openUpdatePO: EventEmitter<any> = new EventEmitter<any>();
  public purchaseOrders: PurchaseOrder[] = [];
  public loggedUser: User;

  public pageData: BehaviorSubject<PageData> = new BehaviorSubject(new PageData({}));
  public paginateConfig: PaginationInstance = {
    id: 'transactionsPaginate',
    itemsPerPage: this.pageData.value.per_page,
    currentPage: 1,
    totalItems: this.pageData.value.total
  }
  public loading: boolean = false;

  constructor(
    private _store: Store<fromRoot.State>,
    private _poService: PurchaseOrderService
  ) {
    this.pageData.subscribe(data => {
      this.paginateConfig.itemsPerPage = data.per_page;
      this.paginateConfig.totalItems = data.total;
    });
  }

  ngOnInit() {
    this._store.select(fromRoot.getPurchaseOrders).subscribe(purchaseOrders => {
      this.loading = false;
      this.purchaseOrders = purchaseOrders;
    });
    this._store.select(fromRoot.loggedUser).subscribe(user => this.loggedUser = user);
    this._store.select(fromRoot.getPurchaseOrderPageStatus).subscribe(pageData => this.pageData.next(pageData));
  }

  ngOnChanges() {
    this.loading = true;
    let formData = {
      start: this.start_date ? this.start_date.formatted : '',
      end: this.end_date ? this.end_date.formatted : '',
      status: this.type,
      state_code: this.state,
      user_id: this.user,
      page: this.paginateConfig.currentPage,
      per_page: 15
    };
    formData.start == '' ? delete formData.start : null;
    formData.end == '' ? delete formData.end : null;
    formData.state_code ? null : delete formData.state_code;
    formData.user_id ? null : delete formData.user_id;
    this._store.dispatch(new purchaseOrderActions.FilterPurchaseOrderAction(formData));
  }

  confirm(event, type, id:number) {
    event.stopPropagation();
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, " + type + " purchase order!"
    }).then(result => {
      if (result.value) {
        switch(type) {
          case 'delete':
            this._store.dispatch(new purchaseOrderActions.DeletePurchaseOrderAction(id));
            break;
          case 'open':
            this._store.dispatch(new purchaseOrderActions.OpenPurchaseOrderAction(id));
            break;
          default:
            
        }
      }
    });
  }

  openConfirm(event, id:number) {
    event.stopPropagation();
    this.openConfirmPO.emit(this.purchaseOrders.find(po => po.id == id));
  }

  openDispatch(event, id:number) {
    event.stopPropagation();
    this.openDispatchPO.emit(id);
  }

  openClose(event, id:number) {
    event.stopPropagation();
    this.openClosePO.emit(id);
  }

  openDetail(id:number) {
    event.stopPropagation();
    this.openDetailPO.emit(id);
  }

  printPO(event, id: number) {
    event.stopPropagation();
    this._store.dispatch(new purchaseOrderActions.FetchPurchaseOrderAction(id));
    this._store.select(fromRoot.getCurrentPurchaseOrder).take(2).subscribe(po => (po != null ? this._poService.printPurchaseOrder(po) : null));
  }

  deletePO(event, id: number) {
    event.stopPropagation();
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete purchase order!"
    }).then(result => {
      if (result.value) {
        this._store.dispatch(
          new purchaseOrderActions.DeletePurchaseOrderAction(id)
        );
      }
    });
  }
  
  openUpdate(event, id:number) {
    event.stopPropagation();
    this.openUpdatePO.emit(this.purchaseOrders.find(po => po.id == id));
  }

  getPage(page: number) {
    this.paginateConfig.currentPage = page;
    this.ngOnChanges();
  }
}
