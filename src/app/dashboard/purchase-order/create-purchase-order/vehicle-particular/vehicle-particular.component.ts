import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../../../shared/reducers';
import * as vehicleActions from '../../../../shared/actions/vehicle.actions';
import { Vehicle } from '../../../../shared/models/index';

@Component({
  selector: '[vehicle]',
  templateUrl: './vehicle-particular.component.html',
  styleUrls: ['./vehicle-particular.component.sass']
})
export class VehicleParticularComponent implements OnInit {
  
  @Input('group') vehicleForm: FormGroup;
  public brands: any[] = [];
  public models: any[] = [];
  public variants: any[] = [];
  public vehicles: Vehicle[] = [];

  constructor(
    private _store: Store<fromRoot.State>
  ) { }

  ngOnInit() {
    this.getVehicles();
  }

  get make() {
    return this.vehicleForm.get('make') as FormControl;
  }

  get model() {
    return this.vehicleForm.get('model') as FormControl;
  }

  get variant() {
    return this.vehicleForm.get('variant') as FormControl;
  }

  getVehicles() {
    this._store.select(fromRoot.getVehicles).subscribe(vehicles => {
      this.vehicles = vehicles;
      this.brands = Array.from(new Set(vehicles.map(vehicle => vehicle.make)));
    });
  }

  getModels(make) {
    this.models = this.vehicles.filter(vehicle => vehicle.make == make).map(vehicle => vehicle.model)
  }

  getVariants(make, model) {
    this.variants = this.vehicles.filter(vehicle => vehicle.make == make).filter(vehicle => vehicle.model == model).map(vehicle => vehicle.variant)
  }
  
}
