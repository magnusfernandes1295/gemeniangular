import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import 'rxjs/add//operator/first';

import * as fromRoot from '../../../shared/reducers';
import * as purchaseOrderActions from '../../../shared/actions/purchase-order.actions';
import * as vehicleActions from '../../../shared/actions/vehicle.actions';
import { PurchaseOrder, User, PurchaseOrderParticulars, Vehicle } from '../../../shared/models/index';
import { RtoService } from '../../../shared/services/rto.service';

declare var $: any;

@Component({
  selector: 'create-purchase-order',
  templateUrl: './create-purchase-order.component.html',
  styleUrls: ['./create-purchase-order.component.sass']
})
export class CreatePurchaseOrderComponent implements OnInit {

  public purchaseOrderForm: FormGroup;
  public loggedUser: User = new User({});
  public vehicles: Vehicle[] = [];
  public currentPO: PurchaseOrder = null;
  public addForm: boolean = false;
  public savedAddress: boolean = true;
  public states: any[] = [];
  public deletedItems: PurchaseOrderParticulars[] = [];

  constructor(
    public _fb: FormBuilder,
    private _store: Store<fromRoot.State>,
    private _rtoService: RtoService
  ) { }

  ngOnInit() {
    this.buildForm();
    this.initializers();
    this.states = this._rtoService.getStates();
    this._store.dispatch(new vehicleActions.FetchAllVehiclesAction);
    this._store.select(fromRoot.loggedUser).subscribe(user => this.loggedUser = user);
    this._store.select(fromRoot.getCurrentPurchaseOrder).subscribe(po => po != null ? this.purchaseOrderForm.patchValue(po) : null);
    this._store.select(fromRoot.getVehicles).subscribe(vehicles => this.vehicles = vehicles);
  }

  buildForm() {
    this.purchaseOrderForm = this._fb.group({
      address: [null],
      address_l1: [null],
      address_l2: [null],
      locality: [null],
      city: [null],
      state: [null],
      pincode: [null, [Validators.minLength(6), Validators.maxLength(6)]],
      remarks: [null],
      particulars: this._fb.array([])
    })
  }

  get address(): FormControl {
    return this.purchaseOrderForm.get('address') as FormControl;
  }

  get address_l1(): FormControl {
    return this.purchaseOrderForm.get('address_l1') as FormControl;
  }

  get address_l2(): FormControl {
    return this.purchaseOrderForm.get('address_l2') as FormControl;
  }

  get locality(): FormControl {
    return this.purchaseOrderForm.get('locality') as FormControl;
  }

  get city(): FormControl {
    return this.purchaseOrderForm.get('city') as FormControl;
  }

  get state(): FormControl {
    return this.purchaseOrderForm.get('state') as FormControl;
  }

  get pincode(): FormControl {
    return this.purchaseOrderForm.get('pincode') as FormControl;
  }

  get remarks(): FormControl {
    return this.purchaseOrderForm.get('remarks') as FormControl;
  }

  get particulars(): FormArray {
    return this.purchaseOrderForm.get('particulars') as FormArray;
  }

  initVehicle(data?: PurchaseOrderParticulars) {
    return this._fb.group({
      id: [data ? data.id : null],
      make: [data ? data.make : null, Validators.required],
      model: [data ? data.model : null, Validators.required],
      variant: [data ? data.variant : null, Validators.required],
      quantity: [data ? data.quantity : null, Validators.required]
    });
  }

  addVehicle(data?: PurchaseOrderParticulars) {
    this.particulars.push(this.initVehicle(data));
  }

  removeVehicle(i: number) {
    this.particulars.at(i).get('id') ? this.deletedItems.push(this.particulars.at(i).value) : null
    this.particulars.removeAt(i);
  }

  useSavedAddress() {
    this.savedAddress = !this.savedAddress;
    if (this.savedAddress) {
      this.address.clearValidators();
      this.address_l1.clearValidators();
      this.locality.clearValidators();
      this.city.clearValidators();
      this.pincode.clearValidators();
      this.state.clearValidators();
    } else {
      this.address.setValidators([Validators.required])
      this.address_l1.setValidators([Validators.required])
      this.locality.setValidators([Validators.required])
      this.city.setValidators([Validators.required])
      this.pincode.setValidators([Validators.required, Validators.minLength(6), Validators.maxLength(6)])
      this.state.setValidators([Validators.required])
    }
    this.address.updateValueAndValidity();
    this.address_l1.updateValueAndValidity();
    this.locality.updateValueAndValidity();
    this.city.updateValueAndValidity();
    this.pincode.updateValueAndValidity();
    this.state.updateValueAndValidity();
  }

  savePO() {
    let formData = this.purchaseOrderForm.value;
    formData.particulars.map(particular => {
      particular["vehicle_id"] = this.vehicles
        .filter(vehicle => vehicle.make == particular.make)
        .filter(vehicle => vehicle.model == particular.model)
        .find(vehicle => vehicle.variant == particular.variant).id;
      delete particular.make;
      delete particular.model;
      delete particular.variant;
      return;
    });
    if (formData.state != null && !this.savedAddress) {
      formData["state_code"] = formData.state.code;
      formData["state"] = formData.state.name;
    } else {
      delete formData['state']
    }
    if (this.addForm) {
      if (!this.savedAddress){
        formData["address"] = formData.address_l1 + "\n" + (formData.address_l2 ? formData.address_l2 + "\n" : "") + formData.locality + "\n" + formData.city + "\n" + formData.pincode;
      } else {
        delete formData['address']
      }
    } else {
      formData["id"] = this.currentPO.id;
      delete formData["user"];
      delete formData['amount_paid'];
      delete formData['created_at'];
      delete formData['serial_no'];
      delete formData['shipping_date'];
      delete formData['shipping_amt'];
      delete formData['status'];
      delete formData['total_quantity'];
      delete formData['tracking_no'];
      this.deletedItems.map(item => {
        formData.particulars.push({
          id: item.id,
          _destroy: 1
        });
      });
    }
    delete formData['address_l1'];
    delete formData['address_l2'];
    delete formData['locality'];
    delete formData['city'];
    delete formData['pincode'];
    formData = {
      "purchase_order": Object.assign({}, this.currentPO, formData)
    };
    this.addForm ? this._store.dispatch(new purchaseOrderActions.CreatePurchaseOrderAction(formData))
      : this._store.dispatch(new purchaseOrderActions.UpdatePurchaseOrderAction(formData))
    this.purchaseOrderForm.reset();
  }

  openPOCreateModal() {
    this.particulars.controls = [];
    this._store.dispatch(new purchaseOrderActions.OpenPurchaseOrderModalAction);
    this.addForm = true;
    this.purchaseOrderForm.reset();
    this.addVehicle();
  }

  openPOUpdateModal(po: PurchaseOrder) {
    this.deletedItems = [];
    this._store.dispatch(new purchaseOrderActions.OpenPurchaseOrderModalAction);
    this.addForm = false;
    this.purchaseOrderForm.reset();
    this.currentPO = po;
    this.purchaseOrderForm.patchValue(po);
    this.particulars.controls = [];
    po.particulars.map(particular => this.addVehicle(particular));
    this.purchaseOrderForm.valueChanges.first().subscribe(value => {
      this.particulars.controls.map(group => group.valueChanges.first().subscribe(value => {
        if (group.get('make').touched) {
          this.deletedItems.push(group.value);
          group.reset();
        }
      }));
    });
  }

  closePOModal() {
    this._store.dispatch(new purchaseOrderActions.ClosePurchaseOrderModalAction);
  }

  initializers() {
    this._store.select(fromRoot.showPOModal).subscribe(res => res ? $("#poModal").modal("show") : $("#poModal").modal("hide"));
    $("#poModal").on("hidden.bs.modal", () => this.closePOModal());
  }
}
