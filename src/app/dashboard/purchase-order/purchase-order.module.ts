import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';

import { VehicleParticularComponent } from './create-purchase-order/vehicle-particular/vehicle-particular.component';
import { CreatePurchaseOrderComponent } from './create-purchase-order/create-purchase-order.component';
import { PurchaseOrderComponent } from './purchase-order.component';
import { PurchaseOrderTableComponent } from './purchase-order-table/purchase-order-table.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoadersCssModule } from 'angular2-loaders-css';

const routes: Routes = [
  {
    path: '',
    component: PurchaseOrderComponent
  }
];

@NgModule({
  declarations: [
    PurchaseOrderComponent,
    CreatePurchaseOrderComponent,
    VehicleParticularComponent,
    PurchaseOrderTableComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxPaginationModule,
    LoadersCssModule,
    NgxMyDatePickerModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class PurchaseOrderModule { }
