import { Component, OnInit } from '@angular/core';

import * as fromRoot from '../../shared/reducers';
import * as userActions from '../../shared/actions/user.actions';
import { Store } from '@ngrx/store';
import { User } from '../../shared/models/index';

@Component({
  selector: 'side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.sass']
})
export class SideNavComponent implements OnInit {
  public loggedUser: User;

  constructor(
    private _store: Store<fromRoot.State>
  ) { }

  ngOnInit() {
    this._store.select(fromRoot.loggedUser).subscribe(user => {
      this.loggedUser = user;
    });
  }

}
