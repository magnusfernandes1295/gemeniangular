import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../../shared/reducers';
import * as inventoryActions from '../../../shared/actions/inventory.actions';
import { Inventory } from '../../../shared/models/index';

@Component({
  selector: 'stock-chart',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.sass']
})
export class StockComponent implements OnInit {

  public inventoryLabels:any[] = [];
  public inventoryData:any[] = [];
  public pieChartType:string = 'bar';
  public barChartLegend:boolean = false;

  @Input('dashboardData') dashboardData: any;
  public index: number = 0;
  public types = [
    {
      type: 'automotive_connector',
      title: 'AUTOMOTIVE CONNECTOR'
    },
    {
      type: 'tool',
      title: 'TOOL'
    },
    {
      type: 'raw_material',
      title: 'RAW MATERIAL'
    },
    {
      type: 'finished_product',
      title: 'FINISHED PRODUCT'
    },
    {
      type: 'office_supply',
      title: 'OFFICE SUPPLY'
    },
    {
      type: 'other',
      title: 'OTHER'
    }
  ];

  constructor(
    private _store: Store<fromRoot.State>
  ) { }

  ngOnInit() {
    this.refreshData();
  }

  refreshData() {
    if (this.dashboardData != undefined) {
      this.inventoryLabels = Object.keys(this.dashboardData.inventory[this.types[this.index].type]);
      this.inventoryLabels.map(label => 
        this.inventoryData.push(this.dashboardData.inventory[this.types[this.index].type][label]));
    }
  }

  leftClick() {
    if (this.index == 0) {
      this.index = Object.keys(this.dashboardData.inventory).length;
    } else {
      this.index--;
      this.refreshData();
    }
  }

  rightClick() {
    if (Object.keys(this.dashboardData.inventory).length-1 == this.index) {
      this.index = 0;
    } else {
      this.index++;
      this.refreshData();
    }
  }

}
