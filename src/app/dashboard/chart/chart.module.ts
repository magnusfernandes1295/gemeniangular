import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StockComponent } from './stock/stock.component';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [
    StockComponent
  ],
  imports: [
    CommonModule,
    ChartsModule
  ],
  providers: [],
  exports: [
    StockComponent
  ]
})
export class ChartModule { }
