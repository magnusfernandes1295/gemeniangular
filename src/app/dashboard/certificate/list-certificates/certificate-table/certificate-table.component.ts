import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import swal from "sweetalert2";
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PaginationInstance } from 'ngx-pagination';

import * as fromRoot from "../../../../shared/reducers";
import { Certificate, User, PageData } from '../../../../shared/models';
import * as certificateActions from "../../../../shared/actions/certificate.actions";
import { CertificateService } from "../../../../shared/services/certificate.service";

@Component({
  selector: 'certificate-table',
  templateUrl: './certificate-table.component.html',
  styleUrls: ['./certificate-table.component.sass']
})
export class CertificateTableComponent implements OnInit, OnChanges {

  @Input('type') type: string;
  @Input('search') search: string;
  @Input('searchType') searchType: string;
  @Input('start_date') start_date: any;
  @Input('end_date') end_date: any;
  @Input('user') user: number;
  @Output() updateCertificate: EventEmitter<any> = new EventEmitter<any>();
  public certificates: Certificate[] = [];
  public loggedUser: User;

  public pageData: BehaviorSubject<PageData> = new BehaviorSubject(new PageData({}));
  public paginateConfig: PaginationInstance = {
    id: 'transactionsPaginate',
    itemsPerPage: this.pageData.value.per_page,
    currentPage: 1,
    totalItems: this.pageData.value.total
  }
  public loading: boolean = false;

  constructor(
    private _store: Store<fromRoot.State>,
    private _certificateService: CertificateService,
    private _router: Router
  ) {
    this.pageData.subscribe(data => {
      this.paginateConfig.itemsPerPage = data.per_page;
      this.paginateConfig.totalItems = data.total;
    });
  }

  ngOnInit() {
    this._store.select(fromRoot.getCertificates).subscribe(certificates => {
      this.loading = false;
      this.certificates = certificates;
    });
    this._store.select(fromRoot.loggedUser).subscribe(user => this.loggedUser = user);
    this._store.select(fromRoot.getCertificatePageStatus).subscribe(pageData => this.pageData.next(pageData));
  }

  ngOnChanges() {
    this.loading = true;
    let formData = {
      start: this.start_date ? this.start_date.formatted : '',
      end: this.end_date ? this.end_date.formatted : '',
      status: this.type,
      user_id: this.user,
      page: this.paginateConfig.currentPage,
      per_page: 15
    };
    if (this.search) {
      formData[this.searchType] = this.search;
    }
    formData.start == '' ? delete formData.start : null;
    formData.end == '' ? delete formData.end : null;
    formData.user_id ? null : delete formData.user_id;
    this._store.dispatch(new certificateActions.FilterCertificatesAction(formData));
  }

  filterCertificates() {
    return this.certificates.filter(certificate => certificate.status == this.type);
  }

  editCertificate(event, id: number) {
    event.stopPropagation();
    this.updateCertificate.emit(this.certificates.find(certificate => certificate.id == id));
  }

  renewCertificate(event, id: number) {
    event.stopPropagation();
    this._store.dispatch(new certificateActions.RenewCertificateAction(id));
  }

  issueCheck(event, certificate: Certificate, type: boolean) {
    event.stopPropagation();
    if (certificate.status != 'is_valid') {
      swal({
        title: "You need to issue this certificate first!",
        text: "Are you sure you want to issue?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, issue certificate!"
      }).then(result => {
        if (result.value) {
          this._store.dispatch(new certificateActions.IssueCertificateAction({
            id: certificate.id,
            type: type
          }));
        }
      });
    } else {
      type ? this._certificateService.certificateActions('Print', certificate.id) : this._certificateService.certificateActions('Download', certificate.id)
    }
  }

  deleteCertificate(event, id: number) {
    event.stopPropagation();
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete certificate!"
    }).then(result => {
      if (result.value) {
        this._store.dispatch(new certificateActions.DeleteCertificateAction(id));
      }
    });
  }

  viewCertificate(id: number) {
    this._router.navigate(['dashboard/certificate/issue-certificate/' + id]);
  }

  getPage(page: number) {
    this.paginateConfig.currentPage = page;
    this.ngOnChanges();
  }

}
