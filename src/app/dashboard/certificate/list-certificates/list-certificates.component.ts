import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { INgxMyDpOptions } from "ngx-mydatepicker";

import * as fromRoot from "../../../shared/reducers";
import * as certificateActions from "../../../shared/actions/certificate.actions";
import * as deviceActions from "../../../shared/actions/device.actions";
import * as userActions from "../../../shared/actions/user.actions";
import * as vehicleActions from "../../../shared/actions/vehicle.actions";
import { Device, Vehicle, Certificate, User } from "../../../shared/models";
import { RtoService } from "../../../shared/services/rto.service";

declare var $: any;

@Component({
  selector: "app-list-certificates",
  templateUrl: "./list-certificates.component.html",
  styleUrls: ["./list-certificates.component.sass"]
})
export class ListCertificatesComponent implements OnInit {

  public addForm: boolean = false;
  public certificateForm: FormGroup;
  public filterForm: FormGroup;
  public currentCertificate: Certificate = new Certificate({});
  public devices: Device[] = [];
  public vehicles: Vehicle[] = [];
  public makes: any[] = [];
  public models: any[] = [];
  public rto: any[] = [];
  public loggedUser: User = new User({});
  public users: User[] = [];
  public type: string = 'can_modify';
  start_options: INgxMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    disableSince: {
      year: (new Date()).getFullYear(),
      month: (new Date()).getMonth() + 1,
      day: (new Date()).getDate() + 1
    }
  };

  public mfgOptions: INgxMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    openSelectorTopOfInput: true,
    disableSince: {
      year: (new Date()).getFullYear(),
      month: (new Date()).getMonth() + 1,
      day: (new Date()).getDate()
    }
  };
  public regOptions: INgxMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    openSelectorTopOfInput: true,
    disableSince: {
      year: (new Date()).getFullYear(),
      month: (new Date()).getMonth() + 1,
      day: (new Date()).getDate()
    }
  };

  constructor(
    private _store: Store<fromRoot.State>,
    private _fb: FormBuilder,
    private _rtoService: RtoService
  ) { }

  ngOnInit() {
    this.initializers();
    this.buildForm();
    this.formListener();
    this._store.dispatch(new deviceActions.FetchAllDevicesAction);
    this._store.select(fromRoot.getAllDevices).subscribe(devices => this.devices = devices.filter(device => device.status != 'certified'));
    this._store.dispatch(new userActions.FetchAllUsersAction);
    this._store.select(fromRoot.getUsers).subscribe(users => this.users = users);    
    this._store.dispatch(new vehicleActions.FetchAllVehiclesAction);
    this._store.select(fromRoot.getVehicles).subscribe(vehicles => {
      this.vehicles = vehicles;
      this.getVehicles()
    });
    this._store.select(fromRoot.loggedUser).subscribe(user => {
      this.loggedUser = user;
      if (user.role == 'distributor' || user.role == 'dealer') {
        this.rto = this._rtoService.getRto(user.details.state);
      }
    });
  }

  buildForm() {
    this.certificateForm = this._fb.group({
      device_id: [null, Validators.required],
      invoice_no: [null, Validators.required],
      vehicle_make: [null, Validators.required],
      vehicle_model: [null, Validators.required],
      engine_number: ['', Validators.required],
      chassis_number: ['', Validators.required],
      cutoff_speed: ['', [Validators.required, Validators.min(40), Validators.max(120)]],
      car_reg_number: ['', Validators.required],
      customer_name: ['', Validators.required],
      customer_address: [''],
      address_l1: [''],
      address_l2: [''],
      locality: [''],
      city: [''],
      pincode: [''],
      seals: [''],
      customer_telephone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(13), Validators.pattern("[0-9]+")]],
      location_state: [null, Validators.required],
      location_rto: [null, Validators.required],
      mfg_month_year: [null, Validators.required],
      reg_month_year: [null, Validators.required],
    });
    this.filterForm = this._fb.group({
      search: [''],
      searchType: ['reg'],
      start_date: [''],
      end_date: [''],
      user: null
    });
  }

  get search() {
    return this.filterForm.get('search').value;
  }

  get searchType() {
    return this.filterForm.get('searchType').value;
  }

  get start_date() {
    return this.filterForm.get('start_date').value;
  }

  get end_date() {
    return this.filterForm.get('end_date').value;
  }

  get user_filter(): FormControl {
    return this.filterForm.get('user') as FormControl;
  }

  get device_id(): FormControl {
    return this.certificateForm.get('device_id') as FormControl;
  }

  get invoice_no(): FormControl {
    return this.certificateForm.get('invoice_no') as FormControl;
  }

  get vehicle_make(): FormControl {
    return this.certificateForm.get('vehicle_make') as FormControl;
  }

  get vehicle_model(): FormControl {
    return this.certificateForm.get('vehicle_model') as FormControl;
  }

  get engine_number(): FormControl {
    return this.certificateForm.get('engine_number') as FormControl;
  }

  get chassis_number(): FormControl {
    return this.certificateForm.get('chassis_number') as FormControl;
  }

  get cutoff_speed(): FormControl {
    return this.certificateForm.get('cutoff_speed') as FormControl;
  }

  get location_state(): FormControl {
    return this.certificateForm.get('location_state') as FormControl;
  }

  get location_rto(): FormControl {
    return this.certificateForm.get('location_rto') as FormControl;
  }

  get car_reg_number(): FormControl {
    return this.certificateForm.get('car_reg_number') as FormControl;
  }

  get customer_name(): FormControl {
    return this.certificateForm.get('customer_name') as FormControl;
  }

  get customer_telephone(): FormControl {
    return this.certificateForm.get('customer_telephone') as FormControl;
  }

  get customer_address(): FormControl {
    return this.certificateForm.get('customer_address') as FormControl;
  }

  get address_l1(): FormControl {
    return this.certificateForm.get('address_l1') as FormControl;
  }

  get address_l2(): FormControl {
    return this.certificateForm.get('address_l2') as FormControl;
  }

  get locality(): FormControl {
    return this.certificateForm.get('locality') as FormControl;
  }

  get city(): FormControl {
    return this.certificateForm.get('city') as FormControl;
  }

  get pincode(): FormControl {
    return this.certificateForm.get('pincode') as FormControl;
  }

  get seals(): FormControl {
    return this.certificateForm.get('seals') as FormControl;
  }

  get mfg_month_year(): FormControl {
    return this.certificateForm.get('mfg_month_year') as FormControl;
  }

  get reg_month_year(): FormControl {
    return this.certificateForm.get('reg_month_year') as FormControl;
  }

  getVehicles() {
    let data: any[] = [];
    this.makes = this.vehicles.map(vehicle => vehicle.make).filter((value, index, self) => self.indexOf(value) === index)
    this.models = this.vehicles
      .filter(vehicle => vehicle.make == this.vehicle_make.value)
      .map(vehicle => vehicle.model)
      .filter((value, index, self) => self.indexOf(value) === index)
  }

  formListener() {
    this.certificateForm.valueChanges.subscribe(value => {
      this.getVehicles();
    });
    this.vehicle_make.valueChanges.subscribe(() => {
      this.vehicle_model.reset();
    });
    this.mfg_month_year.valueChanges.subscribe(() => {
      this.reg_month_year.reset();
      let copy: INgxMyDpOptions = JSON.parse(JSON.stringify(this.regOptions));
      copy.disableUntil = this.mfg_month_year.value ? this.mfg_month_year.value.date : null
      this.regOptions = copy;
    });
  }

  saveChanges() {
    let formData = this.certificateForm.value;
    this.mfg_month_year.dirty ? formData.mfg_month_year = formData.mfg_month_year.formatted : null
    this.reg_month_year.dirty ? formData.reg_month_year = formData.reg_month_year.formatted : null
    formData["vehicle_id"] = this.vehicles.find(vehicle => vehicle.make == formData.vehicle_make && vehicle.model == formData.vehicle_model).id
    delete formData['vehicle_make'];
    delete formData['vehicle_model'];
    if (this.addForm) {
      formData["customer_address"] = formData.address_l1 + ", " + formData.address_l2 + ", " + formData.locality + ", " + formData.city + " - " + formData.pincode;
      delete formData['address_l1'];
      delete formData['address_l2'];
      delete formData['locality'];
      delete formData['city'];
      delete formData['pincode'];
      this._store.dispatch(new certificateActions.CreateCertificateAction(formData));
    } else {
      formData = Object.assign({}, this.currentCertificate, formData);
      delete formData.device_id;
      this._store.dispatch(new certificateActions.UpdateCertificateAction(formData));
    }
  }

  openCertificateModal(certificate: Certificate) {
    if (certificate) {
      this.currentCertificate = certificate;
      certificate.vehicle_make = certificate.vehicle.make;
      certificate.vehicle_model = certificate.vehicle.model;
      let data: any = certificate;
      data.device_id = data.device.sld_number
      this.addForm = false;
      this.certificateForm.patchValue(certificate, { emitEvent: false });
      let date = new Date(certificate.mfg_month_year);
      this.mfg_month_year.patchValue({
        date: {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate()
        }
      });
      date = new Date(certificate.reg_month_year);
      this.reg_month_year.patchValue({
        date: {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate()
        }
      });
    } else {
      this.location_state.patchValue(this.loggedUser.details.state, {emitEvent: false});
      this.addForm = true;
    }
    this.validation();
    this._store.dispatch(new certificateActions.OpenCertificateModalAction);
  }

  validation() {
    if (!this.addForm) {
      this.address_l1.clearValidators();
      this.address_l2.clearValidators();
      this.locality.clearValidators();
      this.city.clearValidators();
      this.pincode.clearValidators();
      this.customer_address.setValidators([Validators.required]);
    } else {
      this.address_l1.setValidators([Validators.required]);
      this.address_l2.setValidators([Validators.required]);
      this.locality.setValidators([Validators.required]);
      this.city.setValidators([Validators.required]);
      this.pincode.setValidators([Validators.minLength(6), Validators.maxLength(6)]);
      this.customer_address.clearValidators();
    }
    this.address_l1.updateValueAndValidity();
    this.address_l2.updateValueAndValidity();
    this.locality.updateValueAndValidity();
    this.city.updateValueAndValidity();
    this.pincode.updateValueAndValidity();
    this.customer_address.updateValueAndValidity();
  }

  closeCertificateModal() {
    this._store.dispatch(new certificateActions.CloseCertificateModalAction);
    this.certificateForm.reset();
  }

  initializers() {
    this._store.select(fromRoot.showCertificateModal).subscribe(res =>
      res ? $('#certificateModal').modal('show') : $('#certificateModal').modal('hide'));
    $("#certificateModal").on("hidden.bs.modal", () => this.closeCertificateModal());
  }
}
