import { CertificateComponent } from './certificate.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { NgSelectModule } from '@ng-select/ng-select';

import { ListCertificatesComponent } from './list-certificates/list-certificates.component';
import { IssueCertificateComponent } from './issue-certificate/issue-certificate.component';
import { CertificateTableComponent } from './list-certificates/certificate-table/certificate-table.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoadersCssModule } from 'angular2-loaders-css';

const routes: Routes = [
  {
    path: '',
    component: CertificateComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list-certificate'
      },
      {
        path: 'list-certificates',
        component: ListCertificatesComponent
      },
      {
        path: 'issue-certificate/:id',
        component: IssueCertificateComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    CertificateComponent,
    IssueCertificateComponent,
    ListCertificatesComponent,
    CertificateTableComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxMyDatePickerModule.forRoot(),
    NgSelectModule,
    NgxPaginationModule,
    LoadersCssModule,
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class CertificateModule { }
