import { ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import swal from "sweetalert2";

import * as fromRoot from "../../../shared/reducers";
import * as certificateActions from "../../../shared/actions/certificate.actions";
import { Certificate } from "./../../../shared/models/certificate.model";
import { CertificateService } from "../../../shared/services/certificate.service";

declare var $: any;

@Component({
  selector: "app-issue-certificate",
  templateUrl: "./issue-certificate.component.html",
  styleUrls: ["./issue-certificate.component.sass"]
})
export class IssueCertificateComponent implements OnInit {
  public certificate: Certificate;

  constructor(
    private _route: ActivatedRoute,
    private _store: Store<fromRoot.State>,
    private _certificateService: CertificateService
  ) {
    this._store.dispatch(new certificateActions.FetchCertificateAction(this._route.snapshot.params["id"]));
  }

  ngOnInit() {
    this._store.select(fromRoot.getCurrentCertificate).subscribe(certificate => this.certificate = certificate);
  }

  issueCheck(type: boolean) {
    if (this.certificate.status != 'is_valid') {
      swal({
        title: "You need to issue this certificate first!",
        text: "Are you sure you want to issue?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, issue certificate!"
      }).then(result => {
        if (result.value) {
          this._store.dispatch(new certificateActions.IssueCertificateAction({
            id: this.certificate.id,
            type: type
          }));
        }
      });
    } else {
      this.action(type);
    }
  }

  getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL;
  }

  action(type: boolean) {
    type ? this._certificateService.certificateActions('Print', this.certificate.id) : this._certificateService.certificateActions('Download', this.certificate.id)
  }
}
