import { Routes, RouterModule } from "@angular/router";
import { Angular2TokenService } from "angular2-token";
import { NgModule } from "@angular/core";

import { DashboardComponent } from "./dashboard.component";
import { TransferDevicesComponent } from './transfer-devices/transfer-devices.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [Angular2TokenService],
    children: [
      {
        path: 'home',
        loadChildren: './home/home.module#HomeModule'
      },
      {
        path: 'devices',
        loadChildren: './device/device.module#DeviceModule'
      },
      {
        path: 'transfer-devices',
        component: TransferDevicesComponent
      },
      {
        path: 'certificate',
        loadChildren: './certificate/certificate.module#CertificateModule'
      },
      {
        path: 'vehicle',
        loadChildren: './vehicle/vehicle.module#VehicleModule'
      },
      {
        path: 'purchase-order',
        loadChildren: './purchase-order/purchase-order.module#PurchaseOrderModule'
      },
      {
        path: 'user-management',
        loadChildren: './user-management/user-management.module#UserManagementModule'
      },
      {
        path: 'inventory',
        loadChildren: './inventory/inventory.module#InventoryModule'
      },
      {
        path: 'receive-note',
        loadChildren: './receive-note/receive-note.module#ReceiveNoteModule'
      },
      {
        path: 'requisition-order',
        loadChildren: './requisition-order/requisition-order.module#RequisitionOrderModule'
      },
      {
        path: 'salary-slip',
        loadChildren: './salary-slip/salary-slip.module#SalarySlipModule'
      },
      {
        path: 'expense',
        loadChildren: './expense/expense.module#ExpenseModule'
      },
      {
        path: 'income',
        loadChildren: './income/income.module#IncomeModule'
      },
      {
        path: 'transactions',
        loadChildren: './transactions/transactions.module#TransactionsModule'
      },
      {
        path: 'feedback',
        loadChildren: './feedback/feedback.module#FeedbackModule'
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'dashboard'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
