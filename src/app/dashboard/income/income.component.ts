import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../shared/reducers';
import * as incomeActions from '../../shared/actions/income.actions';
import { Cost } from '../../shared/models';

@Component({
  selector: 'app-income',
  templateUrl: './income.component.html',
  styleUrls: ['./income.component.sass']
})
export class IncomeComponent implements OnInit {
  public incomeForm: FormGroup;
  public categories: any[] = [
    {
      display: 'Indirect',
      value: 'indirect'
    },
    {
      display: 'Direct',
      value: 'direct'
    }
  ];

  constructor(
    private _store: Store<fromRoot.State>,
    private _fb: FormBuilder
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.incomeForm = this._fb.group({
      category: [null, Validators.required],
      description: [null, Validators.required],
      total: [null, [Validators.required, Validators.min(0)]]
    });
  }

  get category(): FormControl {
    return this.incomeForm.get('category') as FormControl;
  }

  get description(): FormControl {
    return this.incomeForm.get('description') as FormControl;
  }

  get total(): FormControl {
    return this.incomeForm.get('total') as FormControl;
  }

  saveIncome() {
    this._store.dispatch(new incomeActions.CreateIncomeAction({ income: this.incomeForm.value }));
    this.incomeForm.reset();
  }

}
