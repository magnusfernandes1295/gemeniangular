import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';

import { IncomeComponent } from './income.component';

const routes: Routes = [
  {
    path: '',
    component: IncomeComponent
  }
];

@NgModule({
  declarations: [
    IncomeComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgSelectModule,
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class IncomeModule { }
