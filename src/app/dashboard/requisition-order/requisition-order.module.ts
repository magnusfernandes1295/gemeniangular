import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { RouterModule, Routes } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { LoadersCssModule } from 'angular2-loaders-css';
import { NgxPaginationModule } from 'ngx-pagination';

import { RequisitionOrderParticularComponent } from './create-requisition-order/requisition-order-particular/requisition-order-particular.component';
import { CreateRequisitionOrderComponent } from './create-requisition-order/create-requisition-order.component';
import { RequisitionOrderComponent } from './requisition-order.component';
import { RequisitionTableComponent } from './requisition-table/requisition-table.component';

const routes: Routes = [
  {
    path: '',
    component: RequisitionOrderComponent
  }
];

@NgModule({
  declarations: [
    RequisitionOrderComponent,
    CreateRequisitionOrderComponent,
    RequisitionOrderParticularComponent,
    RequisitionTableComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxPaginationModule,
    LoadersCssModule,
    NgxMyDatePickerModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class RequisitionOrderModule { }
