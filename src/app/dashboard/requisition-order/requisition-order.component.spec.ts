import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisitionOrderComponent } from './requisition-order.component';

describe('RequisitionOrderComponent', () => {
  let component: RequisitionOrderComponent;
  let fixture: ComponentFixture<RequisitionOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequisitionOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisitionOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
