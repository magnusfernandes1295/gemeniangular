import { RequisitionOrderService } from "./../../shared/services/requisition-order.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Store } from "@ngrx/store";
import swal from 'sweetalert2';
import { FormBuilder, FormGroup } from "@angular/forms";
import { INgxMyDpOptions } from "ngx-mydatepicker";

import * as fromRoot from "../../shared/reducers";
import * as reqActions from "../../shared/actions/requisition-order.actions";
import * as inventoryActions from "../../shared/actions/inventory.actions";
import { RequisitionOrder, User } from "../../shared/models";
import { CreateRequisitionOrderComponent } from "./create-requisition-order/create-requisition-order.component";

declare var $: any;

@Component({
  selector: "app-requisition-order",
  templateUrl: "./requisition-order.component.html",
  styleUrls: ["./requisition-order.component.sass"]
})
export class RequisitionOrderComponent implements OnInit {

  public filterForm: FormGroup;
  public currentRequisition: RequisitionOrder = new RequisitionOrder({});
  public loggedUser: User = new User({});
  public type: string = 'can_modify';
  @ViewChild(CreateRequisitionOrderComponent) child:CreateRequisitionOrderComponent;
  start_options: INgxMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    disableSince: {
      year: (new Date()).getFullYear(),
      month: (new Date()).getMonth() + 1,
      day: (new Date()).getDate() + 1
    }
  };

  constructor(
    private _store: Store<fromRoot.State>,
    private _fb: FormBuilder
  ) {}

  ngOnInit() {
    this.buildForm();
    this.initializers();
    this._store.select(fromRoot.getCurrentRequisitionOrder).subscribe(req => this.currentRequisition = req);
    this._store.select(fromRoot.loggedUser).subscribe(user => this.loggedUser = user);
  }

  buildForm() {
    this.filterForm = this._fb.group({
      start_date: [''],
      end_date: ['']
    });
  }

  get start_date() {
    return this.filterForm.get('start_date').value;
  }

  get end_date() {
    return this.filterForm.get('end_date').value;
  }

  changeType(event, type: string) {
    event.preventDefault();
    this.type = type;
  }

  openReqModal(req: RequisitionOrder) {
    this._store.dispatch(new reqActions.OpenRequisitionOrderModal);
    this.child.openReqModal(req);
  }

  openReq(event, id: number) {
    event.stopPropagation();
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, open requisition order!"
    }).then(result => {
      if (result.value) {
        this._store.dispatch(new reqActions.OpenRequisitionOrderAction(id));
      }
    });
  }

  viewReq(id: number) {
    this._store.dispatch(new reqActions.OpenViewRequisitionOrderModal);
    this._store.dispatch(new reqActions.FetchRequisitionOrderAction(id));
  }

  closeViewReq() {
    this._store.dispatch(new reqActions.CloseViewRequisitionOrderModal());
  }

  canOpen() {
    let flag: boolean = true;
    this.currentRequisition.req_particulars.map(particular => flag = particular.quantity_available && flag);
    return flag;
  }

  initializers() {
    this._store
      .select(fromRoot.showViewReqModal)
      .subscribe(
        res =>
          res
            ? $("#detailsModal").modal("show")
            : $("#detailsModal").modal("hide")
      );

    $("#detailsModal").on("hidden.bs.modal", () => {
      this.closeViewReq();
    });
  }
}
