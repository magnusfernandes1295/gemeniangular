import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';

import * as fromRoot from "../../../shared/reducers";
import * as reqActions from "../../../shared/actions/requisition-order.actions";
import * as inventoryActions from "../../../shared/actions/inventory.actions";
import { Store } from '@ngrx/store';
import { RequisitionOrder, RequisitionOrderParticulars } from '../../../shared/models';

declare var $:any;

@Component({
  selector: 'create-requisition-order',
  templateUrl: './create-requisition-order.component.html',
  styleUrls: ['./create-requisition-order.component.sass']
})
export class CreateRequisitionOrderComponent implements OnInit {

  public requisitionOrderForm: FormGroup;
  public currentRequisitionOrder: RequisitionOrder;
  public addReq: boolean = false;
  public deletedItems: number[] = [];

  constructor(
    private _fb: FormBuilder,
    private _store: Store<fromRoot.State>
  ) { }

  ngOnInit() {
    this.buildForm();
    this.initializers();
    this._store.dispatch(new inventoryActions.FetchAllInventoryAction);
  }

  buildForm() {
    this.requisitionOrderForm = this._fb.group({
      id: null,
      req_particulars: this._fb.array([])
    });
  }

  get id(): FormControl {
    return this.requisitionOrderForm.get('id') as FormControl;
  }
  get reqParticulars(): FormArray {
    return this.requisitionOrderForm.get('req_particulars') as FormArray;
  }

  addInventory(data?: RequisitionOrderParticulars) {
    this.reqParticulars.push(this._fb.group({
      id: [data ? data.id : null],
      inventory_item_id: [data ? data.inventory_item.id : null, Validators.required],
      quantity: [data ? data.quantity : null, [Validators.required, Validators.min(0)]]
    }));
  }

  removeInventory(i: number) {
    this.deletedItems.push(this.reqParticulars.at(i).get('id').value);
    this.reqParticulars.removeAt(i);
  }

  openReqModal(req: RequisitionOrder) {
    this.requisitionOrderForm.reset();
    this.deletedItems = [];
    while(this.reqParticulars.length > 0) {
      this.reqParticulars.removeAt(0);
    }
    if (req) {
      this.addReq = false;
      let data = req;
      data.req_particulars.map(particular => this.addInventory(particular));
      this.id.patchValue(req.id, {emitEvent: false});
    } else {
      this.addReq = true;
      this.addInventory();
    }
    this._store.dispatch(new reqActions.OpenRequisitionOrderModal);
  }
  
  closeReqModal() {
    this._store.dispatch(new reqActions.CloseRequisitionOrderModal);
  }

  saveChanges() {
    let formData = this.requisitionOrderForm.value;
    if (this.addReq) {
      this._store.dispatch(new reqActions.CreateNewRequisitionOrderAction({
        req_order: formData
      }));
    } else {
      this.deletedItems.map(id => {
        formData.req_particulars.push({
          id: id,
          _destroy: 1
        })
      });
      this._store.dispatch(new reqActions.UpdateRequisitionOrderAction(formData))
    }
  }

  initializers() {
    this._store.select(fromRoot.showReqModal).subscribe(res =>
      res ? $("#requisitionOrderModal").modal("show") : $("#requisitionOrderModal").modal("hide"));

    $("#requisitionOrderModal").on("hidden.bs.modal", () => this.closeReqModal());
  }

}
