import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRequisitionOrderComponent } from './create-requisition-order.component';

describe('CreateRequisitionOrderComponent', () => {
  let component: CreateRequisitionOrderComponent;
  let fixture: ComponentFixture<CreateRequisitionOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRequisitionOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRequisitionOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
