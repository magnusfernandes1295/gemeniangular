import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import * as fromRoot from '../../../../shared/reducers';
import { Store } from '@ngrx/store';
import { Inventory } from '../../../../shared/models';

@Component({
  selector: 'req-particular',
  templateUrl: './requisition-order-particular.component.html',
  styleUrls: ['./requisition-order-particular.component.sass']
})
export class RequisitionOrderParticularComponent implements OnInit {

  @Input("group") public reqParticularForm: FormGroup;
  public inventory: Inventory[];
  
  constructor(
    private _store: Store<fromRoot.State>
  ) { }

  ngOnInit() {
    this._store.select(fromRoot.getAllInventory).subscribe(inventory => this.inventory = inventory);
  }

}
