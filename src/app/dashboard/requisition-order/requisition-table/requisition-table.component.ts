import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import swal from 'sweetalert2';

import * as fromRoot from "../../../shared/reducers";
import * as reqActions from "../../../shared/actions/requisition-order.actions";
import * as userActions from "../../../shared/actions/user.actions";
import { RequisitionOrder, User, PageData } from '../../../shared/models';
import { RequisitionOrderService } from '../../../shared/services/requisition-order.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PaginationInstance } from 'ngx-pagination';

declare var $: any;

@Component({
  selector: 'requisition-table',
  templateUrl: './requisition-table.component.html',
  styleUrls: ['./requisition-table.component.sass']
})
export class RequisitionTableComponent implements OnInit, OnChanges {

  @Input('type') type: string;
  @Input('start_date') start_date: any;
  @Input('end_date') end_date: any;
  @Output() openReqModal: EventEmitter<any> = new EventEmitter<any>();
  @Output() openReqViewModal: EventEmitter<any> = new EventEmitter<any>();
  public reqOrders: RequisitionOrder[] = [];
  public loggedUser: User;

  public pageData: BehaviorSubject<PageData> = new BehaviorSubject(new PageData({}));
  public paginateConfig: PaginationInstance = {
    id: 'transactionsPaginate',
    itemsPerPage: this.pageData.value.per_page,
    currentPage: 1,
    totalItems: this.pageData.value.total
  }
  public loading: boolean = false;

  constructor(
    private _store: Store<fromRoot.State>,
    private _requisitionService: RequisitionOrderService
  ) {
    this.pageData.subscribe(data => {
      this.paginateConfig.itemsPerPage = data.per_page;
      this.paginateConfig.totalItems = data.total;
    });
  }

  ngOnInit() {
    this._store.select(fromRoot.getRequisitionOrders).subscribe(reqOrders => {
      this.reqOrders = reqOrders;
      this.loading = false;
    });
    this._store.select(fromRoot.getRequisitionOrderPageStatus).subscribe(pageData => this.pageData.next(pageData));
    this._store.select(fromRoot.loggedUser).subscribe(user => this.loggedUser = user);
  }

  ngOnChanges() {
    this.loading = true;
    let formData = {
      start: this.start_date ? this.start_date.formatted : '',
      end: this.end_date ? this.end_date.formatted : '',
      status: this.type,
      page: this.paginateConfig.currentPage,
      per_page: 15
    };
    formData.start == '' ? delete formData.start : null;
    formData.end == '' ? delete formData.end : null;
    this._store.dispatch(new reqActions.FilterRequisitionOrderAction(formData));
  }

  reqOperation(event, type, id: number) {
    event.stopPropagation();
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, " + type + " requisition order!"
    }).then(result => {
      if (result.value) {
        type == 'delete' ?
        this._store.dispatch(new reqActions.DeleteRequisitionOrderAction(id)) :
        this._store.dispatch(new reqActions.CloseRequisitionOrderAction(id))
      }
    });
  }

  updateReq(event, id: number) {
    event.stopPropagation();
    this.openReqModal.emit(this.reqOrders.find(req => req.id == id));
  }

  printReq(event, id: number) {
    event.stopPropagation();
    this._requisitionService.printRequisition(this.reqOrders.find(req => req.id == id));
  }

  viewReq(id: number) {
    this.openReqViewModal.emit(id);
  }

  getPage(page: number) {
    this.paginateConfig.currentPage = page;
    this.ngOnChanges();
  }

}