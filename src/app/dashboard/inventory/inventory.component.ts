import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../shared/reducers';
import * as inventoryActions from '../../shared/actions/inventory.actions';
import { Inventory } from '../../shared/models';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

declare var $:any;

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.sass']
})
export class InventoryComponent implements OnInit {

  public addItem: boolean = false;
  public itemForm: FormGroup;
  public filterForm: FormGroup;
  public type: string = 'automotive_connector';
  public categories: any[] = [
    {
      display: 'Automotive connector',
      value: 'automotive_connector'
    },
    {
      display: 'Tool',
      value: 'tool'
    },
    {
      display: 'Raw material',
      value: 'raw_material'
    },
    {
      display: 'Finished product',
      value: 'finished_product'
    },
    {
      display: 'Other',
      value: 'other'
    }
  ];

  constructor(
    private _store: Store<fromRoot.State>,
    private _fb: FormBuilder
  ) { }

  ngOnInit() {
    this.buildForm();
    this.initializers();
  }

  buildForm() {
    this.itemForm = this._fb.group({
      id: null,
      category: [null, Validators.required],
      description: [null, Validators.required],
      item_code: [null, Validators.required]
    });
    this.filterForm = this._fb.group({
      search: null
    });
  }

  get search(): FormControl {
    return this.filterForm.get('search') as FormControl;
  }

  saveChanges() {
    let formData: any = {
      inventory_item: this.itemForm.value
    }
    if (this.addItem) {
      this._store.dispatch(new inventoryActions.CreateInventoryAction(formData));
    } else {
      formData = {
        id: formData.inventory_item.id,
        message: formData
      }
      delete formData.message.inventory_item["id"];
      this._store.dispatch(new inventoryActions.UpdateInventoryAction(formData));
    }
  }

  openInventoryModal(item: Inventory) {
    this.itemForm.reset();
    if (item) {
      this.addItem = false;
      this.itemForm.patchValue(item);
    } else {
      this.addItem = true;
    }
    this._store.dispatch(new inventoryActions.OpenInventoryModalAction);
  }

  closeInventoryModal() {
    this._store.dispatch(new inventoryActions.CloseInventoryModalAction);
  }

  initializers() {
    $('#inventoryModal').on('hidden.bs.modal', () => this.closeInventoryModal());
    this._store.select(fromRoot.showInventoryModal).subscribe(res => res ? $('#inventoryModal').modal('show') : $('#inventoryModal').modal('hide'));
  }

}
