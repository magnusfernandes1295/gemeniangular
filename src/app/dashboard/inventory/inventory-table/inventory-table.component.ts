import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import swal from 'sweetalert2';
import { PaginationInstance } from 'ngx-pagination';

import * as fromRoot from '../../../shared/reducers';
import * as inventoryActions from '../../../shared/actions/inventory.actions';
import { Inventory } from '../../../shared/models';

@Component({
  selector: 'inventory-table',
  templateUrl: './inventory-table.component.html',
  styleUrls: ['./inventory-table.component.sass']
})
export class InventoryTableComponent implements OnInit, OnChanges {

  @Input('type') type: string;
  @Input('search') search: string;
  @Output() openInventoryModal: EventEmitter<any> = new EventEmitter();
  public items: Inventory[] = [];
  public loading: boolean = false;
  public config: PaginationInstance = {
    itemsPerPage: 20,
    currentPage: 1,
    totalItems: this.items.length
  };

  constructor(
    private _store: Store<fromRoot.State>
  ) { }

  ngOnInit() {
    this._store.select(fromRoot.getAllInventory).subscribe(items => {
      this.loading = false;
      this.items = [...items.filter(item => item.category == this.type)];
    });
  }

  ngOnChanges() {
    if (this.type) {
      this.loading = true;
      this._store.dispatch(new inventoryActions.FilterInventoryAction(this.type));
    }
  }

  openUpdate(id: number) {
    this.openInventoryModal.emit(this.items.find(item => item.id == id));
  }

  deleteItem(id: number) {
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete inventory!"
    }).then(result => {
      if (result.value) {
        this._store.dispatch(new inventoryActions.DeleteInventoryAction(id));
      } 
    });
  }

}
