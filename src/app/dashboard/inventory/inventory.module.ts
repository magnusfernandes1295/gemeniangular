import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { InventoryComponent } from './inventory.component';
import { InventoryTableComponent } from './inventory-table/inventory-table.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoadersCssModule } from 'angular2-loaders-css';
import { NgArrayPipesModule } from 'ngx-pipes';

const routes: Routes = [
  {
    path: '',
    component: InventoryComponent
  }
];

@NgModule({
  declarations: [
    InventoryComponent,
    InventoryTableComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxPaginationModule,
    NgArrayPipesModule,
    LoadersCssModule,
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class InventoryModule { }
