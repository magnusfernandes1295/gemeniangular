import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { PaginationInstance } from 'ngx-pagination';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import * as fromRoot from "../../shared/reducers";
import * as transactionActions from "../../shared/actions/transaction.actions";
import { Transaction, PageData } from '../../shared/models';

declare var $: any;

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.sass']
})
export class TransactionsComponent implements OnInit, OnDestroy {

  public transactions: Transaction[] = [];
  public transaction: Transaction = new Transaction({});
  public transactionForm: FormGroup;
  public transactables: any[] = [
    {
      display: 'Salary slip',
      value: 'SalarySlip'
    },
    {
      display: 'Income',
      value: 'Income'
    },
    {
      display: 'Expense',
      value: 'Expense'
    },
    {
      display: 'Receive note',
      value: 'ReceiveNote'
    },
    {
      display: 'Purchase order',
      value: 'PurchaseOrder'
    }
  ];
  public categories: any[] = [
    {
      display: 'Expense',
      value: 'expense'
    },
    {
      display: 'Income',
      value: 'income'
    }
  ];
  public pageData: BehaviorSubject<PageData> = new BehaviorSubject(new PageData({}));
  public paginateConfig: PaginationInstance = {
    id: 'transactionsPaginate',
    itemsPerPage: this.pageData.value.per_page,
    currentPage: 1,
    totalItems: this.pageData.value.total
  }
  public loading: boolean = false;

  start_options: INgxMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    disableSince: {
      year: (new Date()).getFullYear(),
      month: (new Date()).getMonth() + 1,
      day: (new Date()).getDate() + 1
    }
  };

  constructor(
    private _store: Store<fromRoot.State>,
    private _fb: FormBuilder
  ) {
    this.pageData.subscribe(data => {
      this.paginateConfig.itemsPerPage = data.per_page;
      this.paginateConfig.totalItems = data.total;
    });
  }

  ngOnInit() {
    this.buildForm();
    this.initializers();
    this.formListener();
    this._store.select(fromRoot.getAllTransactions).subscribe(transactions => this.transactions = transactions);
    this._store.select(fromRoot.getCurrentTransaction).subscribe(transaction => this.transaction = transaction);
    this._store.dispatch(new transactionActions.FetchAllTransactionsAction({
      per_page: 15
    }));
    this._store.select(fromRoot.getTransactionPageStatus).subscribe(pageData => this.pageData.next(pageData));
  }

  ngOnDestroy() {
    this._store.dispatch(new transactionActions.ClearTransactionStateAction);
  }

  buildForm() {
    this.transactionForm = this._fb.group({
      start_date: ['', Validators.required],
      end_date: ['', Validators.required],
      transactable_type: ['', Validators.required],
      category: ['', Validators.required],
      page: 1,
      per_page: 15
    });
  }

  get start_date(): FormControl {
    return this.transactionForm.get('start_date') as FormControl;
  }

  get end_date(): FormControl {
    return this.transactionForm.get('end_date') as FormControl;
  }

  get transactable_type(): FormControl {
    return this.transactionForm.get('transactable_type') as FormControl;
  }

  get category(): FormControl {
    return this.transactionForm.get('category') as FormControl;
  }

  get page(): FormControl {
    return this.transactionForm.get('page') as FormControl;
  }

  formListener() {
    this.transactionForm.valueChanges.subscribe(value => {
      if (this.start_date.value) {
        let date = new Date();
        if (!this.end_date.value) {
          this.end_date.patchValue({ jsdate: date }, { emitEvent: false });
        }
        this._store.dispatch(new transactionActions.FetchAllTransactionsAction({
          start_date: this.start_date.value.formatted,
          end_date: this.end_date.value.formatted ? this.end_date.value.formatted : `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`,
          transactable_type: this.transactable_type.value ? this.transactable_type.value : null,
          category: this.category.value ? this.category.value : null,
          per_page: 15
        }));
      } else {
        this._store.dispatch(new transactionActions.FetchAllTransactionsAction({
          per_page: 15,
          page: this.page.value
        }));
      }
    });
  }

  getPage(page: number) {
    this.paginateConfig.currentPage = page;
    this.page.patchValue(page);
  }

  openTransactionModal(id: number) {
    this._store.dispatch(new transactionActions.OpenTransactionModalAction);
    this._store.dispatch(new transactionActions.FetchTransactionAction(id));
  }

  closeTransactionModal() {
    this._store.dispatch(new transactionActions.CloseTransactionModalAction);
  }

  initializers() {
    $('#transactionModal').on('hidden.bs.modal', () => this.closeTransactionModal());
    this._store.select(fromRoot.showTransactionModal).subscribe(res => res ? $('#transactionModal').modal('show') : $('#transactionModal').modal('hide'));
  }
}
