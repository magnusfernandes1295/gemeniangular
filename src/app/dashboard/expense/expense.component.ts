import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../shared/reducers';
import * as expenseActions from '../../shared/actions/expense.actions';
import { Cost } from '../../shared/models';

declare var $:any;

@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.sass']
})
export class ExpenseComponent implements OnInit {

  public categories: any[] = [
    {
      display: 'Indirect',
      value: 'indirect'
    },
    {
      display: 'Direct',
      value: 'direct'
    }
  ];
  public addExpense: boolean = false;
  public expenseForm: FormGroup;

  constructor(
    private _store: Store<fromRoot.State>,
    private _fb: FormBuilder
  ) { }

  ngOnInit() {
    this.buildForm();
    this.initializers();
    this._store.dispatch(new expenseActions.FetchAllExpensesAction);
    this.expenseForm.valueChanges.subscribe(value =>
      this.total.patchValue(Math.ceil(this.amount.value + (this.amount.value * this.gst.value * 0.01)), {emitEvent: false}));
  }

  buildForm() {
    this.expenseForm = this._fb.group({
      id: null,
      category: ['', Validators.required],
      description: ['', Validators.required],
      amount: ['', [Validators.required, Validators.min(0)]],
      gst: ['', [Validators.required, Validators.min(0), Validators.max(80)]],
      gstn: ['', [Validators.minLength(15), Validators.maxLength(15), Validators.pattern("[a-zA-Z0-9]+")]],
      total: ['', Validators.required]
    });
  }

  get category(): FormControl {
    return this.expenseForm.get('category') as FormControl;
  }

  get description(): FormControl {
    return this.expenseForm.get('description') as FormControl;
  }

  get amount(): FormControl {
    return this.expenseForm.get('amount') as FormControl;
  }

  get gst(): FormControl {
    return this.expenseForm.get('gst') as FormControl;
  }

  get gstn(): FormControl {
    return this.expenseForm.get('gstn') as FormControl;
  }

  get total(): FormControl {
    return this.expenseForm.get('total') as FormControl;
  }

  openExpenseModal(expense?: Cost) {
    this.expenseForm.reset();
    if (expense) {
      this.addExpense = false;
      this.expenseForm.patchValue(expense);
    } else {
      this.addExpense = true;
    }
    this._store.dispatch(new expenseActions.OpenExpenseModalAction);
  }

  closeExpenseModal() {
    this._store.dispatch(new expenseActions.CloseExpenseModalAction);
  }

  saveExpense() {
    if (this.addExpense) {
      this._store.dispatch(new expenseActions.CreateExpenseAction({
        expense: this.expenseForm.value
      }));
    } else {
      let formData: any = {
        id: this.expenseForm.value.id,
        message: {
          expense: this.expenseForm.value
        }
      };
      delete formData.message.expense["id"];
      this._store.dispatch(new expenseActions.UpdateExpenseAction(formData));
    }
  }

  initializers() {
    this._store.select(fromRoot.showExpenseModal).subscribe(res => res ? $('#expenseModal').modal('show') : $('#expenseModal').modal('hide'));
    $('#expenseModal').on('hidden.bs.modal', () => this.closeExpenseModal());
  }

}
