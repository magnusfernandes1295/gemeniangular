import { ReactiveFormsModule } from '@angular/forms';
import { ExpenseComponent } from './expense.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ExpenseTableComponent } from './expense-table/expense-table.component';

const routes: Routes = [
  {
    path: '',
    component: ExpenseComponent
  }
];

@NgModule({
  declarations: [
    ExpenseComponent,
    ExpenseTableComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgSelectModule,
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class ExpenseModule { }
