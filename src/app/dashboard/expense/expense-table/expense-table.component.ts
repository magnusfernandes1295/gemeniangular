import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import swal from 'sweetalert2';

import * as fromRoot from '../../../shared/reducers';
import * as expenseActions from '../../../shared/actions/expense.actions';
import { Cost } from '../../../shared/models';

@Component({
  selector: 'expense-table',
  templateUrl: './expense-table.component.html',
  styleUrls: ['./expense-table.component.sass']
})
export class ExpenseTableComponent implements OnInit {

  @Input('type') type: string;
  @Output() openExpenseModal: EventEmitter<any> = new EventEmitter();
  public expenses: Cost[] = [];

  constructor(
    private _store: Store<fromRoot.State>
  ) { }

  ngOnInit() {
    this._store.select(fromRoot.getAllExpenses).subscribe(expenses => this.expenses = expenses.filter(expense => expense.category == this.type));
  }

  openExpense(event, id: number) {
    event.stopPropagation();
    this.openExpenseModal.emit(this.expenses.find(expense => expense.id == id));
  }

  deleteExpense(event, id: number) {
    event.stopPropagation();
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete expense!"
    }).then(result => {
      if (result.value) {
        this._store.dispatch(new expenseActions.DeleteExpenseAction(id));
      } 
    });
  }

}
