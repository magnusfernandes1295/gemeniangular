import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Angular2TokenService } from 'angular2-token';

import * as moment from 'moment';

import * as fromRoot from '../../shared/reducers';
import * as userActions from '../../shared/actions/user.actions';

@Component({
  selector: 'top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.sass']
})
export class TopNavbarComponent implements OnInit, OnDestroy {
  public dateTime: string;
  public interval: any;

  constructor(
    private _tokenService: Angular2TokenService,
    private _store: Store<fromRoot.State>
  ) { }

  ngOnInit() {
    this.startTimer();
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  logOut() {
    this._store.dispatch(new userActions.SignoutUserAction);
  }

  startTimer() {
    this.interval = setInterval(() => {
      this.dateTime = moment().format('Do MMMM YYYY, hh:mm:ss A');
    }, 1000);
  }

}
