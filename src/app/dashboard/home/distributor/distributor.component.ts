import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'distributor-dashboard',
  templateUrl: './distributor.component.html',
  styleUrls: ['./distributor.component.sass']
})
export class DistributorComponent implements OnInit {

  @Input('dashboard') dashboard: any;
  @Input('dealer') dealer: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
