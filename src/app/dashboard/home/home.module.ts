import { DashboardService } from './../../shared/services/dashboard.service';
import { ChartModule } from './../chart/chart.module';
import { HomeComponent } from './home.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { DistributorComponent } from './distributor/distributor.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  declarations: [
    HomeComponent,
    DistributorComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ChartModule,
    RouterModule.forChild(routes)
  ],
  exports: [],
  providers: [DashboardService]
})
export class HomeModule { }
