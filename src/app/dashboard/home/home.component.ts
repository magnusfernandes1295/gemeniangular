import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../shared/reducers';
import * as deviceActions from '../../shared/actions/device.actions';
import { DashboardService } from '../../shared/services/dashboard.service';
import { User } from '../../shared/models/index';
import { RtoService } from '../../shared/services/rto.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  public loggedUser: User = null;
  public dashboardData: any;
  public states: any[] = [];
  public salesForm: FormGroup;
  public sales: any;

  constructor(
    private _fb: FormBuilder,
    private _store: Store<fromRoot.State>,
    private _dashboardService: DashboardService,
    private _rtoService: RtoService
  ) { }

  ngOnInit() {
    this.buildForm();
    this._store.select(fromRoot.loggedUser).subscribe(user => {
      user ? this.loggedUser = user : null;
      if (this.loggedUser.role == 'manufacturer') {
        this.state.patchValue('all', {emitEvent: false});
      }
    });
    this.states = this._rtoService.getStates();
    this.salesForm.valueChanges.subscribe(value => this.sales = this.dashboardData.sales[value.state]);
    this._dashboardService.getDashboardData().subscribe(res => {
      this.dashboardData = res;
      if (this.loggedUser.role == 'manufacturer') {
        this.sales = this.dashboardData.sales[this.state.value]
      }
    });
  }

  buildForm() {
    this.salesForm = this._fb.group({
      state: null
    });
  }

  get state(): FormControl {
    return this.salesForm.controls['state'] as FormControl;
  }

  getStates() {
    let states: any[] = [];
    if (this.dashboardData) {
      this.states.filter(state => 
        Object.keys(this.dashboardData.sales).map(code => code == state.code ? states.push(state) : null));
    }
    return states;
  }

  getFinishedProduct(): number {
    let sum: number = 0;
    if (this.dashboardData != undefined) {
      Object.keys(this.dashboardData.inventory['finished_product']).map(key =>
        sum += this.dashboardData.inventory['finished_product'][key]);
    }
    return sum;
  }
}
