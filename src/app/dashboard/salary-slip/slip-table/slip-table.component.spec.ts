import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlipTableComponent } from './slip-table.component';

describe('SlipTableComponent', () => {
  let component: SlipTableComponent;
  let fixture: ComponentFixture<SlipTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlipTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlipTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
