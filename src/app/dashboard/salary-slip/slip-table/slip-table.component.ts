import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import swal from 'sweetalert2';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PaginationInstance } from 'ngx-pagination';

import * as fromRoot from "../../../shared/reducers";
import * as slipActions from "../../../shared/actions/salary-slip.actions";
import { SalarySlip, User, PageData } from '../../../shared/models';

@Component({
  selector: 'slip-table',
  templateUrl: './slip-table.component.html',
  styleUrls: ['./slip-table.component.sass']
})
export class SlipTableComponent implements OnInit, OnChanges {

  @Input('type') type: string;
  @Input('start_date') start_date: any;
  @Input('end_date') end_date: any;
  @Input('employee') employee: string;
  @Output() openSlipModal: EventEmitter<any> = new EventEmitter();
  public slips: SalarySlip[] = [];
  public loggedUser: User = new User({});

  public pageData: BehaviorSubject<PageData> = new BehaviorSubject(new PageData({}));
  public paginateConfig: PaginationInstance = {
    id: 'transactionsPaginate',
    itemsPerPage: this.pageData.value.per_page,
    currentPage: 1,
    totalItems: this.pageData.value.total
  }
  public loading: boolean = false;

  constructor(
    private _store: Store<fromRoot.State>
  ) {
    this.pageData.subscribe(data => {
      this.paginateConfig.itemsPerPage = data.per_page;
      this.paginateConfig.totalItems = data.total;
    });
  }

  ngOnInit() {
    this._store.select(fromRoot.getSalarySlips).subscribe(slips => {
      this.slips = slips
      this.loading = false;
    });
    this._store.select(fromRoot.loggedUser).subscribe(user => this.loggedUser = user);
    this._store.select(fromRoot.getSalarySlipPageStatus).subscribe(pageData => this.pageData.next(pageData));
  }

  ngOnChanges() {
    this.loading = true;
    let formData: any = {
      start: this.start_date ? this.start_date.formatted : '',
      end: this.end_date ? this.end_date.formatted : '',
      status: this.type,
      user_id: this.employee,
      page: this.paginateConfig.currentPage,
      per_page: 15
    };
    formData.start == '' ? delete formData.start : null;
    formData.end == '' ? delete formData.end : null;
    formData.user_id == '' ? delete formData.user_id : null;
    this._store.dispatch(new slipActions.FilterSalarySlipsAction(formData));
  }

  updateSlip(event, id: number) {
    event.stopPropagation();
    this.openSlipModal.emit(this.slips.find(slip => slip.id == id));
  }

  slipAction(event, id: number, type: string) {
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, " + type + " slip!"
    }).then(result => {
      if (result.value) {
        if (type == 'delete') {
          this._store.dispatch(new slipActions.DeleteSalarySlipAction(id));
        } else if (type == 'confirm') {
          this._store.dispatch(new slipActions.ConfirmSalarySlipAction(id));
        } else {
          this._store.dispatch(new slipActions.PaySalarySlipAction(id));
        }
      }
    });
  }

  getPage(page: number) {
    this.paginateConfig.currentPage = page;
    this.ngOnChanges();
  }
}
