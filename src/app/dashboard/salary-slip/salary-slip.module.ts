import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { LoadersCssModule } from 'angular2-loaders-css';
import { NgxPaginationModule } from 'ngx-pagination';

import { SalarySlipComponent } from './salary-slip.component';
import { SlipTableComponent } from './slip-table/slip-table.component';

const routes: Routes = [
  {
    path: '',
    component: SalarySlipComponent
  }
];

@NgModule({
  declarations: [
    SalarySlipComponent,
    SlipTableComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxPaginationModule,
    LoadersCssModule,
    NgxMyDatePickerModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class SalarySlipModule { }
