import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import swal from 'sweetalert2';
import { Store } from '@ngrx/store';
import { INgxMyDpOptions } from 'ngx-mydatepicker';

import * as fromRoot from "../../shared/reducers";
import * as slipActions from "../../shared/actions/salary-slip.actions";
import * as userActions from "../../shared/actions/user.actions";
import { SalarySlip, User } from '../../shared/models';

declare var $:any;

@Component({
  selector: 'app-salary-slip',
  templateUrl: './salary-slip.component.html',
  styleUrls: ['./salary-slip.component.sass']
})
export class SalarySlipComponent implements OnInit {

  public loggedUser: User = new User({});
  public addSlip: boolean = false;
  public slipForm: FormGroup;
  public filterForm: FormGroup;
  public employees: User[] = [];
  public status: string = 'can_modify';
  start_options: INgxMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    disableSince: {
      year: (new Date()).getFullYear(),
      month: (new Date()).getMonth() + 1,
      day: (new Date()).getDate() + 1
    }
  };

  constructor(
    private _store: Store<fromRoot.State>,
    private _fb: FormBuilder
  ) { }

  ngOnInit() {
    this.buildForm();
    this.initializers();
    this._store.dispatch(new userActions.FetchAllUsersAction);
    this._store.select(fromRoot.getEmployees).subscribe(users => this.employees = users);
    this._store.select(fromRoot.loggedUser).subscribe(user => {
      this.loggedUser = user;
      user.role == 'accounts' ? this.status = 'confirmed' : 'can_modify'
    });
    this.slipForm.valueChanges.subscribe(value => {
      let user: User = this.employees.find(user => user.id == this.employee_id.value);
      if (!user) { return }
      let amount = user.details.base_salary + user.details.hra + user.details.transport_allowance;
      user.details.esic ? amount -= amount * (user.details.esic/100) : null
      amount -= amount * (user.details.gpf/100);
      amount = amount - ((amount/31) * this.leave_days.value);
      this.amount.patchValue(Math.round(amount + this.bonus.value), {emitEvent: false});
    });
  }
  
  buildForm() {
    this.slipForm = this._fb.group({
      id: [null],
      employee_id: [null, Validators.required],
      bonus: [null, [Validators.required, Validators.min(0)]],
      leave_days: [null, [Validators.required, Validators.min(0), Validators.max(31)]],
      amount: [null, [Validators.required, Validators.min(0)]]
    });
    this.filterForm = this._fb.group({
      start_date: [''],
      end_date: [''],
      employee: ['']
    });
  }

  get start_date() {
    return this.filterForm.get('start_date').value;
  }

  get end_date() {
    return this.filterForm.get('end_date').value;
  }

  get employee() {
    return this.filterForm.get('employee').value;
  }

  get employee_id():FormControl {
    return this.slipForm.get('employee_id') as FormControl;
  }

  get bonus():FormControl {
    return this.slipForm.get('bonus') as FormControl;
  }

  get leave_days():FormControl {
    return this.slipForm.get('leave_days') as FormControl;
  }

  get amount():FormControl {
    return this.slipForm.get('amount') as FormControl;
  }

  changeStatus(type: string) {
    this.status = type;
  }

  openSlipModal(slip: SalarySlip) {
    this.slipForm.reset();
    if (slip) {
      this.addSlip = false;
      this.slipForm.patchValue(slip, {emitEvent: false});
      this.employee_id.patchValue(slip.employee.id, {emitEvent: false})
    } else {
      this.addSlip = true;
    }
    this._store.dispatch(new slipActions.OpenSalarySlipModalAction);
  }

  closeSlipModal() {
    this._store.dispatch(new slipActions.CloseSalarySlipModalAction);
  }

  saveChanges() {
    if (this.addSlip) {
      this._store.dispatch(new slipActions.CreateSalarySlipAction(this.slipForm.value));
    } else {
      this._store.dispatch(new slipActions.UpdateSalarySlipAction(this.slipForm.value));
    }
  }

  initializers() {
    this._store.select(fromRoot.showSlipModal).subscribe(res => res ? $('#slipModal').modal('show') : $('#slipModal').modal('hide'));
    $('#slipModal').on('hidden.bs.modal', () => this.closeSlipModal());
  }
}
