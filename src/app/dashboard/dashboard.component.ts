import { loggedUser } from './../shared/reducers/index';
import { User } from './../shared/models/user.model';
import { Store } from '@ngrx/store';
import { Angular2TokenService } from 'angular2-token';
import { Component, OnInit } from '@angular/core';

import * as fromRoot from '../shared/reducers';
import * as userActions from '../shared/actions/user.actions';

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  public loggedUser: User;

  constructor(
    private _tokenService: Angular2TokenService,
    private _store: Store<fromRoot.State>
  ) {
    this._store.dispatch(new userActions.ValidateTokenAction);
  }

  ngOnInit() {
    this._store.select(fromRoot.loggedUser).subscribe(user =>this.loggedUser = user);
    this.adjustContent();
  }

  adjustContent() {
    $(window).ready(() => {
      $('.overflow-container').css('height', $(window).height() - $('.navbar').outerHeight());
    });
  }

}
