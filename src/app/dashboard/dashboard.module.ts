import { FeedbackModule } from './feedback/feedback.module';
import { TransactionsModule } from './transactions/transactions.module';
import { IncomeModule } from './income/income.module';
import { ExpenseModule } from './expense/expense.module';
import { SalarySlipModule } from './salary-slip/salary-slip.module';
import { RequisitionOrderModule } from './requisition-order/requisition-order.module';
import { ReceiveNoteModule } from './receive-note/receive-note.module';
import { InventoryModule } from './inventory/inventory.module';
import { UserManagementModule } from './user-management/user-management.module';
import { PurchaseOrderModule } from './purchase-order/purchase-order.module';
import { DeviceModule } from './device/device.module';
import { CertificateModule } from './certificate/certificate.module';
import { VehicleModule } from './vehicle/vehicle.module';
import { HomeModule } from './home/home.module';

import { RequisitionOrderService } from './../shared/services/requisition-order.service';
import { CertificateService } from './../shared/services/certificate.service';
import { DealerGuardService } from './../shared/guards';
import { DeviceService } from './../shared/services/device.service';
import { PurchaseOrderService } from '../shared/services/purchase-order.service';
import { RtoService } from './../shared/services/rto.service';

import { TopNavbarComponent } from './top-navbar/top-navbar.component';
import { DashboardComponent } from './dashboard.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TransferDevicesComponent } from './transfer-devices/transfer-devices.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
    DashboardComponent,
    TopNavbarComponent,
    SideNavComponent,
    TransferDevicesComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    DashboardRoutingModule,
    HomeModule,
    CertificateModule,
    VehicleModule,
    DeviceModule,
    PurchaseOrderModule,
    UserManagementModule,
    InventoryModule,
    ReceiveNoteModule,
    RequisitionOrderModule,
    SalarySlipModule,
    ExpenseModule,
    IncomeModule,
    TransactionsModule,
    FeedbackModule,
    ReactiveFormsModule,
    NgSelectModule
  ],
  providers: [RtoService, DeviceService, CertificateService, RequisitionOrderService, DealerGuardService, PurchaseOrderService]
})
export class DashboardModule { }
