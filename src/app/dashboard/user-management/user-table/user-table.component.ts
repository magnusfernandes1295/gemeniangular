import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Store } from "@ngrx/store";
import swal from 'sweetalert2';

import * as fromRoot from "../../../shared/reducers";
import * as userActions from "../../../shared/actions/user.actions";
import { User } from "../../../shared/models/index";

@Component({
  selector: 'user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.sass']
})
export class UserTableComponent implements OnInit {

  @Input("type") type: string;
  @Output() openUserModal: EventEmitter<any> = new EventEmitter<any>();
  @Output() password: EventEmitter<any> = new EventEmitter<any>();
  public users: User[] = [];

  constructor(
    public _store: Store<fromRoot.State>
  ) { }

  ngOnInit() {
    this._store.select(fromRoot.getUsers).subscribe(users => {
      switch(this.type) {
        case "customers":
          this.users = users.filter(user => (user.role == 'distributor' || user.role == 'dealer'));
          break;
        case "employees":
          this.users = users.filter(user => (user.role != 'distributor' && user.role != 'dealer' && user.role != 'manufacturer'));
          break;
        default:
          this.users = users.filter(user => user.role == this.type);
          break;
      }
    });
  }

  deleteUser(id: number, email: string) {
    swal({
      title: 'Are you sure?',
      text: 'Delete ' + email,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete!'
    }).then((result) => {
      if (result.value) {
        this._store.dispatch(new userActions.DeleteUserAction(id));
      }
    });
  }

  openEditUserModal(id: number) {
    this.openUserModal.emit(this.users.find(user => user.id == id));
  }

  changePassword(id: number) {
    this.password.emit(id);
  }
}
