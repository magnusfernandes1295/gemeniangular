import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormBuilder, FormControl, FormArray } from "@angular/forms";
import { Store } from "@ngrx/store";
import swal from 'sweetalert2';
import { PaginationInstance } from "ngx-pagination";

import * as fromRoot from "../../shared/reducers";
import * as vehicleActions from "../../shared/actions/vehicle.actions";
import * as inventoryActions from "../../shared/actions/inventory.actions";
import { Vehicle, Inventory, Icat } from "./../../shared/models";

declare var $: any;

@Component({
  selector: "app-vehicle",
  templateUrl: "./vehicle.component.html",
  styleUrls: ["./vehicle.component.sass"]
})

export class VehicleComponent implements OnInit {

  public addForm: boolean = false;
  public vehicles: Vehicle[] = [];
  public connectors: Inventory[] = []
  public vehicleForm: FormGroup;
  public filterForm: FormGroup;
  public icat_pages: Icat[] = [];
  public config: PaginationInstance = {
    itemsPerPage: 20,
    currentPage: 1,
    totalItems: this.vehicles.length
  };

  constructor(
    public _store: Store<fromRoot.State>,
    private _fb: FormBuilder
  ) {
    this._store.dispatch(new vehicleActions.FetchAllVehiclesAction);
    this._store.dispatch(new inventoryActions.FetchAllInventoryAction);
  }

  ngOnInit() {
    this.buildForm();
    this.initializers();
    this._store.select(fromRoot.getVehicles).subscribe(vehicles => this.vehicles = vehicles);
    this._store.select(fromRoot.getAllInventory).subscribe(inventory => this.connectors = inventory.filter(item => item.category == 'automotive_connector'));
  }

  buildForm() {
    this.vehicleForm = this._fb.group({
      id: [null],
      make: [null, Validators.required],
      model: [null, Validators.required],
      variant: [null, Validators.required],
      tac_number: [null, Validators.required],
      category: [null, Validators.required],
      connector_id: [null, Validators.required],
      icats: this._fb.array([])
    });
    this.filterForm = this._fb.group({
      search: null
    });
  }

  get vehicle_id(): FormControl {
    return this.vehicleForm.controls["id"] as FormControl;
  }

  get make(): FormControl {
    return this.vehicleForm.controls["make"] as FormControl;
  }

  get model(): FormControl {
    return this.vehicleForm.controls["model"] as FormControl;
  }

  get variant(): FormControl {
    return this.vehicleForm.controls["variant"] as FormControl;
  }

  get tac_number(): FormControl {
    return this.vehicleForm.controls["tac_number"] as FormControl;
  }

  get category(): FormControl {
    return this.vehicleForm.controls["category"] as FormControl;
  }

  get connector_id(): FormControl {
    return this.vehicleForm.controls["connector_id"] as FormControl;
  }

  get icats(): FormArray {
    return this.vehicleForm.controls["icats"] as FormArray;
  }

  get search(): FormControl {
    return this.filterForm.get('search') as FormControl;
  }

  addIcat() {
    this.icats.push(this._fb.control(null, Validators.required));
  }

  removeIcat(index: number) {
    if (this.addForm) {
      this.icats.removeAt(index);
    }
  }

  deleteIcat(id: number) {
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete document!"
    }).then(result => {
      if (result.value) {
        this._store.dispatch(new vehicleActions.DeleteVehicleIcatAction({
          vehicle_id: this.vehicle_id.value,
          icat_id: id
        }));
      }
    });
  }

  onFileChange($event, index: number) {
    let reader = new FileReader();
    let file = $event.target.files[0];
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.icats.at(index).patchValue(reader.result.split(',')[1], {emitEvent: false})
      this.icats.at(index).markAsDirty();
    }
  }

  openModal(id: number) {
    this.vehicleForm.reset();
    while(this.icats.length != 0) {
      this.icats.removeAt(0);
    }
    this.icat_pages = [];
    if (id) {
      this.addForm = false;
      let vehicle: Vehicle = this.vehicles.find(vehicle => vehicle.id == id);
      this.vehicleForm.patchValue(vehicle, { emitEvent: false });
      this.connector_id.patchValue(vehicle.connector.id, { emitEvent: false });
      this.icat_pages = vehicle.icats;
    } else {
      this.addForm = true;
    }
    this._store.dispatch(new vehicleActions.OpenVehicleModal);
  }

  closeModal() {
    this._store.dispatch(new vehicleActions.CloseVehicleModal);
  }

  saveChanges() {
    let formData: any = this.vehicleForm.value;
    if (this.icats.pristine) {
      delete formData.icats;
    }
    if (this.addForm) {
      this._store.dispatch(new vehicleActions.CreateVehicleAction({
        vehicle: formData
      }));
    } else {
      this._store.dispatch(new vehicleActions.UpdateVehicleAction({
        vehicle: formData
      }));
      if (formData.icats.length > 0) {
        this._store.dispatch(new vehicleActions.UpdateVehicleIcatAction({
          id: formData.id,
          icats: formData.icats
        }))
      }
    }
  }

  deleteVehicle(id: number) {
    swal({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete vehicle!"
    }).then(result => {
      if (result.value) {
        this._store.dispatch(new vehicleActions.DeleteVehicleAction(id));
      }
    });
  }

  initializers() {
    this._store.select(fromRoot.showVehicleModal).subscribe(res => res ? $('#vehicleModal').modal('show') : $('#vehicleModal').modal('hide'));
    $('#vehicleModal').on('hidden.bs.modal', () => this.closeModal());
  }
}
