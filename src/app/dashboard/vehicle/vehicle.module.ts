import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';

import { VehicleComponent } from './vehicle.component';
import { NgArrayPipesModule } from 'ngx-pipes';

const routes: Routes = [
  {
    path: '',
    component: VehicleComponent
  }
];

@NgModule({
  declarations: [
    VehicleComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgArrayPipesModule,
    NgxPaginationModule,
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class VehicleModule { }
