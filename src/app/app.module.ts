import { HttpModule } from '@angular/http';
import { LoginModule } from './login/login.module';
import { Angular2TokenService } from 'angular2-token';
import { DashboardModule } from './dashboard/dashboard.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ErrorComponent } from './error/error.component';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { FeedbackEffects } from './shared/effects/feedback.effects';
import { TransactionEffects } from './shared/effects/transaction.effects';
import { IncomeEffects } from './shared/effects/income.effects';
import { ExpenseEffects } from './shared/effects/expense.effects';
import { SalarySlipEffects } from './shared/effects/salary-slip.effects';
import { RequisitionOrderEffects } from './shared/effects/requisition-order.effects';
import { ReceiveNoteEffects } from './shared/effects/receive-note.effects';
import { InventoryEffects } from './shared/effects/inventory.effects';
import { PurchaseOrderEffects } from './shared/effects/purchase-order.effects';
import { CertificateEffects } from './shared/effects/certificate.effects';
import { VehicleEffects } from './shared/effects/vehicle.effects';
import { DeviceEffects } from './shared/effects/device.effects';
import { UserEffects } from './shared/effects/user.effects';

import { reducers } from './shared/reducers';

@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    DashboardModule,
    AppRoutingModule,
    ChartsModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([
      UserEffects,
      DeviceEffects,
      VehicleEffects,
      CertificateEffects,
      InventoryEffects,
      PurchaseOrderEffects,
      ReceiveNoteEffects,
      RequisitionOrderEffects,
      SalarySlipEffects,
      ExpenseEffects,
      IncomeEffects,
      TransactionEffects,
      FeedbackEffects
    ]),
    StoreDevtoolsModule.instrument({
      maxAge: 25 //  Retains last 25 states
    }),
    LoginModule
  ],
  providers: [Angular2TokenService],
  bootstrap: [AppComponent]
})
export class AppModule { }
