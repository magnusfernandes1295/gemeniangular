import { environment } from './../environments/environment';
import { Angular2TokenService } from "angular2-token";
import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.sass"]
})
export class AppComponent {
  constructor(
    private _tokenService: Angular2TokenService,
    private _router: Router
  ) {
    this._tokenService.init(environment.token_auth_config);
  }
}
